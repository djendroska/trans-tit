package com.fireup.trans.tit.interfaces.rest.atlas;

import javax.xml.bind.ValidationException;

import org.junit.Test;

import com.fireup.trans.tit.interfaces.rest.atlas.dto.Coordinate;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.DateTime;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.Device;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.Position;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.Version;

import static com.fireup.trans.tit.interfaces.rest.atlas.AtlasDtoValidation.validateDateTime;
import static com.fireup.trans.tit.interfaces.rest.atlas.AtlasDtoValidation.validateDevice;
import static com.fireup.trans.tit.interfaces.rest.atlas.AtlasDtoValidation.validatePosition;
import static com.fireup.trans.tit.interfaces.rest.atlas.AtlasDtoValidation.validateVersion;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AtlasDtoValidationTests {

    @Test
    public void version() throws ValidationException {
        Version version = new Version();
        assertThat(version.getVersion()).isEqualTo(Version.VERSION_IMPLEMENTED);

        version.setVersion("");
        assertThat(version.getVersion()).isEqualTo("");

        version.setVersion("test");
        assertThat(version.getVersion()).isEqualTo("test");

        version.setVersion(null);
        assertThatThrownBy(() -> validateVersion(version)).isInstanceOf(ValidationException.class);

        version.setVersion("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
        assertThatThrownBy(() -> validateVersion(version)).isInstanceOf(ValidationException.class);
    }

    @Test
    public void dateTime() throws ValidationException {
        DateTime dateTime = new DateTime(2017, 4, 5, 10, 29, 33, "utc");
        assertThat(dateTime.getYear()).isEqualTo(2017);
        assertThat(dateTime.getMonth()).isEqualTo(4);
        assertThat(dateTime.getDay()).isEqualTo(5);
        assertThat(dateTime.getHour()).isEqualTo(10);
        assertThat(dateTime.getMinute()).isEqualTo(29);
        assertThat(dateTime.getSeconds()).isEqualTo(33);
        assertThat(dateTime.getTimezone()).isEqualTo("utc");

        assertThatThrownBy(() -> validateDateTime(new DateTime(2000, 4, 5, 10, 29, 33, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(10000, 4 , 5, 10, 29, 33, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 0, 5, 10, 29, 33, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 13, 5, 10, 29, 33, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, -1, 10, 29, 33, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, 32, 10, 29, 33, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, 5, -8, 29, 33, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, 5, 24, 29, 33, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, 5, 10, -1, 33, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, 5, 10, 60, 33, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, 5, 10, 29, -5, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, 5, 10, 29, 60, "UTC"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, 5, 10, 29, 33, "UTc"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, 5, 10, 29, 33, "UTCs"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDateTime(new DateTime(2017, 4, 5, 10, 29, 33, null))).isInstanceOf(ValidationException.class);
    }

    @Test
    public void device() throws ValidationException {
        Device device = new Device("id", "name");
        assertThat(device.getDeviceId()).isEqualTo("id");
        assertThat(device.getDeviceName()).isEqualTo("name");

        assertThatThrownBy(() -> validateDevice(new Device(null, "name"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDevice(new Device("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", "name"))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validateDevice(new Device("id", "iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii"))).isInstanceOf(ValidationException.class);
    }

    @Test
    public void position() throws ValidationException {
        Coordinate coordinate = new Coordinate(50.332d, 18.232d);
        DateTime dateTime = new DateTime(2017, 4, 5, 10, 29, 33, "UTC");

        Position position = new Position("id", coordinate, null, null, "On", dateTime);
        assertThat(position.getDeviceId()).isEqualTo("id");
        assertThat(position.getCoordinate().getLatitude()).isEqualTo(50.332d);
        assertThat(position.getCoordinate().getLongitude()).isEqualTo(18.232d);
        assertThat(position.getHeading()).isEqualTo(null);
        assertThat(position.getSpeed()).isEqualTo(null);
        assertThat(position.getIgnitionState()).isEqualTo("On");
        assertThat(position.getDateTime().getYear()).isEqualTo(2017);
        assertThat(position.getDateTime().getMonth()).isEqualTo(4);
        assertThat(position.getDateTime().getDay()).isEqualTo(5);
        assertThat(position.getDateTime().getHour()).isEqualTo(10);
        assertThat(position.getDateTime().getMinute()).isEqualTo(29);
        assertThat(position.getDateTime().getSeconds()).isEqualTo(33);
        assertThat(position.getDateTime().getTimezone()).isEqualTo("UTC");

        boolean exceptionThrown = false;

        assertThatThrownBy(() -> validatePosition(new Position("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", coordinate, null, null, "On", dateTime))).isInstanceOf(ValidationException.class);

        validatePosition(new Position("id", coordinate, 15, null, "ON", dateTime));

        assertThatThrownBy(() -> validatePosition(new Position("id", coordinate, 15, null, "On", dateTime))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validatePosition(new Position("id", coordinate, -1, null, "ON", dateTime))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validatePosition(new Position("id", coordinate, 360, null, "ON", dateTime))).isInstanceOf(ValidationException.class);

        validatePosition(position = new Position("id", coordinate, null, 9551513, "ON", dateTime));
        assertThat(position.getSpeed()).isEqualTo(9551513);

        assertThatThrownBy(() -> validatePosition(new Position("id", coordinate, null, null, "dss", dateTime))).isInstanceOf(ValidationException.class);
        assertThatThrownBy(() -> validatePosition(new Position("id", coordinate, null, null, "off", dateTime))).isInstanceOf(ValidationException.class);

        validatePosition(new Position("id", coordinate, null, null, "UNKNOWN", dateTime));
    }
}
