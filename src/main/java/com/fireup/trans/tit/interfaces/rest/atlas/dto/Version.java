package com.fireup.trans.tit.interfaces.rest.atlas.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Version {
    public static final int VERSION_MAX_LEN = 150;
    public static final String VERSION_IMPLEMENTED = "1.0";

    private String version;

    public Version() {
        this.version = VERSION_IMPLEMENTED;
    }
}
