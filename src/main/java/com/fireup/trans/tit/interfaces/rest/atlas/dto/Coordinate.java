package com.fireup.trans.tit.interfaces.rest.atlas.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Coordinate {
    private Double latitude;
    private Double longitude;
}
