package com.fireup.trans.tit.interfaces.rest.atlas.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DateTime {

    public static final int YEAR_MIN = 2013;
    public static final int YEAR_MAX = 9999;
    public static final int MONTH_MIN = 1;
    public static final int MONTH_MAX = 12;
    public static final int DAY_MIN = 1;
    public static final int DAY_MAX = 31;
    public static final int HOUR_MIN = 0;
    public static final int HOUR_MAX = 23;
    public static final int MINUTE_MIN = 0;
    public static final int MINUTE_MAX = 59;
    public static final int SECONDS_MIN = 0;
    public static final int SECONDS_MAX = 59;

    public static final int TIMEZONE_MAX_LEN = 3;
    public static final String TIMEZONE_REQUIRED = "UTC";


    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int seconds;
    private String timezone;
}
