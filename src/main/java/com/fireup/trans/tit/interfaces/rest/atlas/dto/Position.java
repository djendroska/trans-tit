package com.fireup.trans.tit.interfaces.rest.atlas.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Position {

    public static final int HEADING_MIN = 0;
    public static final int HEADING_MAX = 359;

    private String deviceId;
    private Coordinate coordinate;
    private Integer heading; //optional
    private Integer speed; //optional
    private String ignitionState;
    private DateTime dateTime;
}
