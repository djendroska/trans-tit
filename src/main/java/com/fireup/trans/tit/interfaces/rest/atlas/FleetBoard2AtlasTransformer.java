package com.fireup.trans.tit.interfaces.rest.atlas;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.xml.bind.ValidationException;

import com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice.VEHICLE;
import com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice.GetLastPositionResponseType;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.Coordinate;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.DateTime;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.Device;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.DeviceList;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.IgnitionState;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.Position;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.PositionList;

import static com.fireup.trans.tit.interfaces.rest.atlas.dto.DateTime.TIMEZONE_REQUIRED;


public class FleetBoard2AtlasTransformer {

    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static DeviceList transformDevices(List<VEHICLE> fleetBoardVehicles) throws ValidationException {
        DeviceList atlasDevices = new DeviceList();
        for (VEHICLE vehicle : fleetBoardVehicles) {
            atlasDevices.getDeviceList().add(new Device(vehicle.getInoid(), vehicle.getFLEETVEHICLE().getVEHICLENUMBER()));
        }
        return atlasDevices;
    }

    public static PositionList transformPositions(List<GetLastPositionResponseType.Positions> fleetBoardPositions)
            throws ValidationException {
        PositionList atlasPositions = new PositionList();

        for (GetLastPositionResponseType.Positions position : fleetBoardPositions) {

            String deviceId = transformVehicleId(position.getVehicleID());
            Coordinate coordinate = transformCoordinate(position.getPosition().getLat(), position.getPosition().getLong());
            Integer heading = transformFleetBoardStringToInteger(position.getPosition().getCourse());
            Integer speed = transformFleetBoardStringToInteger(position.getPosition().getSpeed());
            String ignitionState = transformIgnitionStatus(position.getIgnitionStatus());
            DateTime dateTime = transformTimeStamp(position.getPosition().getTimestamp());

            atlasPositions.getPositionList().add(new Position(deviceId, coordinate, heading, speed, ignitionState, dateTime));
        }

        return atlasPositions;
    }

    private static String transformVehicleId(Long fleetBoardVehicleId) {
        return Long.toString(fleetBoardVehicleId);
    }

    private static Coordinate transformCoordinate(Float fleetBoardLatitude, Float fleetBoardLongitude) {
        Double latitude = Double.valueOf(fleetBoardLatitude);
        Double longitude = Double.valueOf(fleetBoardLongitude);
        return new Coordinate(latitude, longitude);
    }

    private static Integer transformFleetBoardStringToInteger(String fleetBoardSpeed) {
        try {
            return Integer.valueOf(fleetBoardSpeed);
        } catch (Exception e) {
            return null;
        }
    }

    private static String transformIgnitionStatus(Short fleetBoardIgnitionState) {

        if (fleetBoardIgnitionState == null) {
            fleetBoardIgnitionState = -1;
        }
        switch (fleetBoardIgnitionState) {
        case 0:
            return IgnitionState.OFF.name();
        case 1:
            return IgnitionState.ON.name();
        default:
            return IgnitionState.UNKNOWN.name();
        }
    }

    private static DateTime transformTimeStamp(String fleetBoardTimestamp) throws ValidationException {

        LocalDateTime fleetBoardDateTime = LocalDateTime.parse(fleetBoardTimestamp, formatter);

        int year = fleetBoardDateTime.getYear();
        int month = fleetBoardDateTime.getMonthValue();
        int day = fleetBoardDateTime.getDayOfMonth();
        int hour = fleetBoardDateTime.getHour();
        int minute = fleetBoardDateTime.getMinute();
        int seconds = fleetBoardDateTime.getSecond();
        String timezone = TIMEZONE_REQUIRED; //not explicitly stated in the FleetBoard documentation

        return new DateTime(year, month, day, hour, minute, seconds, timezone);
    }

}
