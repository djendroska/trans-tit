package com.fireup.trans.tit.interfaces.rest.atlas.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class PositionList {

    private List<Position> positionList = new ArrayList<Position>();
}
