package com.fireup.trans.tit.interfaces.rest.atlas;

import javax.xml.bind.ValidationException;

import com.fireup.trans.tit.interfaces.rest.atlas.dto.DateTime;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.Device;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.DeviceList;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.IgnitionState;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.Position;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.PositionList;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.Version;

public class AtlasDtoValidation {
    private static boolean validateInput(int input, int min, int max) throws ValidationException {
        if (min <= input && input <= max) {
            return true;
        } else {
            throw new ValidationException("Value " + input + " out of bounds: " + min + ", " + max + ".");
        }
    }

    private static boolean validateInput(String input, int maxLength) throws ValidationException {
        String errMessage;
        if (input == null) {
            errMessage = "Null value not allowed.";
        } else if (input.length() > maxLength) {
            errMessage = "Value " + input + " exceeded maximum length: " + maxLength + ".";
        } else {
            return true;
        }
        throw new ValidationException(errMessage);
    }

    private static boolean validateInput(String input, Class<?> enumeration)
            throws IllegalArgumentException, ValidationException {
        if (enumeration == null || !enumeration.isEnum()) {
            throw new IllegalArgumentException("Expected enum type.");
        }
        Object[] possibleValues = enumeration.getEnumConstants();
        for (Object value : possibleValues) {
            if (value.toString().equals(input)){
                return true;
            }
        }
        throw new ValidationException(input + " not found in provided enum " + enumeration.getName());
    }

    private static void validateTimezone(String timezone) throws ValidationException {
        if (!(validateInput(timezone, DateTime.TIMEZONE_MAX_LEN) && timezone.equals(DateTime.TIMEZONE_REQUIRED))) {
            throw new ValidationException("Timezone has to be " + DateTime.TIMEZONE_REQUIRED + ", instead " + timezone + " was given.");
        }
    }

    //package for tests
    static void validateDateTime(DateTime dateTime) throws ValidationException {
        validateInput(dateTime.getYear(), DateTime.YEAR_MIN, DateTime.YEAR_MAX);
        validateInput(dateTime.getMonth(), DateTime.MONTH_MIN, DateTime.MONTH_MAX);
        validateInput(dateTime.getDay(), DateTime.DAY_MIN, DateTime.DAY_MAX);
        validateInput(dateTime.getHour(), DateTime.HOUR_MIN, DateTime.HOUR_MAX);
        validateInput(dateTime.getMinute(), DateTime.MINUTE_MIN, DateTime.MINUTE_MAX);
        validateInput(dateTime.getSeconds(), DateTime.SECONDS_MIN, DateTime.SECONDS_MAX);
        validateTimezone(dateTime.getTimezone());
    }

    //package for tests
    static void validatePosition(Position position) throws ValidationException {
        validateDateTime(position.getDateTime());
        validateInput(position.getDeviceId(), Device.DEVICE_ID_MAX_LEN);
        validateInput(position.getIgnitionState(), IgnitionState.class);
        if(position.getHeading() != null) {
            validateInput(position.getHeading(), Position.HEADING_MIN, Position.HEADING_MAX);
        }
    }

    //package for tests
    static void validateDevice(Device device) throws ValidationException {
        validateInput(device.getDeviceId(), Device.DEVICE_ID_MAX_LEN);
        validateInput(device.getDeviceName(), Device.DEVICE_NAME_MAX_LEN);
    }

    public static void validatePositionList(PositionList positionList) throws ValidationException {
        for (Position position : positionList.getPositionList()) {
            validatePosition(position);
        }
    }

    public static void validateDeviceList(DeviceList deviceList) throws ValidationException {
        for (Device device : deviceList.getDeviceList()) {
            validateDevice(device);
        }
    }

    public static void validateVersion(Version version) throws ValidationException {
        validateInput(version.getVersion(), Version.VERSION_MAX_LEN);
    }

}
