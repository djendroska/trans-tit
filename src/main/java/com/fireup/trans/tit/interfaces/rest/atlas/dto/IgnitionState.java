package com.fireup.trans.tit.interfaces.rest.atlas.dto;

public enum IgnitionState {
    ON,
    OFF,
    UNKNOWN
}
