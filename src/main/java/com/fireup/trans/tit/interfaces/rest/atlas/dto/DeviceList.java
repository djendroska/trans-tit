package com.fireup.trans.tit.interfaces.rest.atlas.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class DeviceList {

    private List<Device> deviceList = new ArrayList<Device>();
}
