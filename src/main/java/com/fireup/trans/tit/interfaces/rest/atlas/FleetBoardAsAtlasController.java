package com.fireup.trans.tit.interfaces.rest.atlas;

import java.util.List;

import javax.xml.bind.ValidationException;

import org.apache.http.auth.InvalidCredentialsException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice.VEHICLE;
import com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice.GetLastPositionResponseType;
import com.fireup.trans.tit.infrastructure.telematics.fleetboard.soap.BasicServiceClient;
import com.fireup.trans.tit.infrastructure.telematics.fleetboard.soap.PosServiceClient;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.DeviceList;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.PositionList;
import com.fireup.trans.tit.interfaces.rest.atlas.dto.Version;
import com.google.gwt.core.ext.typeinfo.NotFoundException;

import lombok.extern.slf4j.Slf4j;

import static com.fireup.trans.tit.interfaces.rest.atlas.AtlasDtoValidation.validateDeviceList;
import static com.fireup.trans.tit.interfaces.rest.atlas.AtlasDtoValidation.validatePositionList;
import static com.fireup.trans.tit.interfaces.rest.atlas.AtlasDtoValidation.validateVersion;

@Slf4j
@RestController
@RequestMapping("/fleetboard")
public class FleetBoardAsAtlasController {

    BasicServiceClient fleetBoardBasicServiceClient;
    PosServiceClient fleetBoardPosServiceClient;

    public FleetBoardAsAtlasController(BasicServiceClient fleetBoardBasicServiceClient, PosServiceClient fleetBoardPosServiceClient) {
        this.fleetBoardBasicServiceClient = fleetBoardBasicServiceClient;
        this.fleetBoardPosServiceClient = fleetBoardPosServiceClient;
    }

    @RequestMapping("/**")
    public void notMapped() throws NotFoundException {
        throw new NotFoundException("Unable to map the request");
    }

    @RequestMapping(path = {"/{fleetName}/atlas/version", "/atlas/version"})
    public Version atlasVersion() throws ValidationException {
        Version atlasVersion = new Version();
        validateVersion(atlasVersion);
        return atlasVersion;
    }

    @RequestMapping(path = {"/{fleetName}/atlas/{username}/devices", "/atlas/{fleetName}/{username}/devices"})
    public DeviceList atlasDevices(
            @PathVariable("fleetName") String fleetName,
            @PathVariable("username") String username,
            @RequestParam("password") String password)
            throws InvalidCredentialsException, ValidationException {

        List<VEHICLE> fleetBoardVehicles = fleetBoardBasicServiceClient.getVehicles(username, fleetName, password);
        DeviceList atlasDeviceList = FleetBoard2AtlasTransformer.transformDevices(fleetBoardVehicles);
        validateDeviceList(atlasDeviceList);
        return atlasDeviceList;
    }

    @RequestMapping(path = {"/{fleetName}/atlas/{username}/positions", "/atlas/{fleetName}/{username}/positions"})
    public PositionList atlasPositions(
            @PathVariable("fleetName") String fleetName,
            @PathVariable("username") String username,
            @RequestParam("password") String password)
            throws ValidationException, InvalidCredentialsException {

        List<GetLastPositionResponseType.Positions> fleetBoardPositions = fleetBoardPosServiceClient.getPositions(username, fleetName, password);
        PositionList atlasPositionList = FleetBoard2AtlasTransformer.transformPositions(fleetBoardPositions);
        validatePositionList(atlasPositionList);
        return atlasPositionList;
    }

    @ExceptionHandler
    @ResponseStatus(value=HttpStatus.NOT_FOUND)
    public @ResponseBody String notFoundException(NotFoundException e) {
        return exceptionHandler(e, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String unauthorizedException(InvalidCredentialsException e) {
        return exceptionHandler(e, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public String genericException(Exception e) {
        return exceptionHandler(e, HttpStatus.SERVICE_UNAVAILABLE);
    }

    private String exceptionHandler(Exception e, HttpStatus httpStatus) {
        log.error("About to return http status {} because of exception {}: {}", httpStatus.value(), e.getClass(), e.getMessage());
        return e.getClass() + ": " + e.getMessage();
    }
}
