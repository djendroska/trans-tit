package com.fireup.trans.tit.interfaces.rest.atlas.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Device {

    public static final int DEVICE_ID_MAX_LEN = 50;
    public static final int DEVICE_NAME_MAX_LEN = 50;

    private String deviceId;
    private String deviceName;
}
