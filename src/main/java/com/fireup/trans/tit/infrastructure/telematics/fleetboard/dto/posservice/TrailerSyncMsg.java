
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nationality" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="licencePlateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="customerMatchCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fleetId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="trailerGroupIds" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="trailerState" type="{http://www.fleetboard.com/data}TrailerState"/>
 *         &lt;element name="updateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="trailerServiceProviderAccountId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trailerServiceProviderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "name",
    "vin",
    "nationality",
    "licencePlateNumber",
    "customerMatchCode",
    "fleetId",
    "trailerGroupIds",
    "trailerState",
    "updateTime",
    "trailerServiceProviderAccountId",
    "trailerServiceProviderId"
})
@XmlRootElement(name = "TrailerSyncMsg")
public class TrailerSyncMsg {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected String name;
    protected String vin;
    @XmlElement(required = true)
    protected String nationality;
    @XmlElement(required = true)
    protected String licencePlateNumber;
    @XmlElement(required = true)
    protected String customerMatchCode;
    protected long fleetId;
    protected List<String> trailerGroupIds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TrailerState trailerState;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updateTime;
    protected String trailerServiceProviderAccountId;
    protected String trailerServiceProviderId;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the vin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVin() {
        return vin;
    }

    /**
     * Sets the value of the vin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVin(String value) {
        this.vin = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the licencePlateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicencePlateNumber() {
        return licencePlateNumber;
    }

    /**
     * Sets the value of the licencePlateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicencePlateNumber(String value) {
        this.licencePlateNumber = value;
    }

    /**
     * Gets the value of the customerMatchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerMatchCode() {
        return customerMatchCode;
    }

    /**
     * Sets the value of the customerMatchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerMatchCode(String value) {
        this.customerMatchCode = value;
    }

    /**
     * Gets the value of the fleetId property.
     * 
     */
    public long getFleetId() {
        return fleetId;
    }

    /**
     * Sets the value of the fleetId property.
     * 
     */
    public void setFleetId(long value) {
        this.fleetId = value;
    }

    /**
     * Gets the value of the trailerGroupIds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the trailerGroupIds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrailerGroupIds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTrailerGroupIds() {
        if (trailerGroupIds == null) {
            trailerGroupIds = new ArrayList<String>();
        }
        return this.trailerGroupIds;
    }

    /**
     * Gets the value of the trailerState property.
     * 
     * @return
     *     possible object is
     *     {@link TrailerState }
     *     
     */
    public TrailerState getTrailerState() {
        return trailerState;
    }

    /**
     * Sets the value of the trailerState property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrailerState }
     *     
     */
    public void setTrailerState(TrailerState value) {
        this.trailerState = value;
    }

    /**
     * Gets the value of the updateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUpdateTime() {
        return updateTime;
    }

    /**
     * Sets the value of the updateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUpdateTime(XMLGregorianCalendar value) {
        this.updateTime = value;
    }

    /**
     * Gets the value of the trailerServiceProviderAccountId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrailerServiceProviderAccountId() {
        return trailerServiceProviderAccountId;
    }

    /**
     * Sets the value of the trailerServiceProviderAccountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrailerServiceProviderAccountId(String value) {
        this.trailerServiceProviderAccountId = value;
    }

    /**
     * Gets the value of the trailerServiceProviderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrailerServiceProviderId() {
        return trailerServiceProviderId;
    }

    /**
     * Sets the value of the trailerServiceProviderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrailerServiceProviderId(String value) {
        this.trailerServiceProviderId = value;
    }

}
