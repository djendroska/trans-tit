
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetVehicleGroupResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getVehicleGroupResponse"
})
@XmlRootElement(name = "getVehicleGroupResponse")
public class GetVehicleGroupResponse {

    @XmlElement(name = "GetVehicleGroupResponse", required = true)
    protected GetVehicleGroupResponse2 getVehicleGroupResponse;

    /**
     * Gets the value of the getVehicleGroupResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetVehicleGroupResponse2 }
     *     
     */
    public GetVehicleGroupResponse2 getGetVehicleGroupResponse() {
        return getVehicleGroupResponse;
    }

    /**
     * Sets the value of the getVehicleGroupResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetVehicleGroupResponse2 }
     *     
     */
    public void setGetVehicleGroupResponse(GetVehicleGroupResponse2 value) {
        this.getVehicleGroupResponse = value;
    }

}
