
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SetpointSensorDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SetpointSensorDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Setpoint1"/>
 *     &lt;enumeration value="Setpoint2"/>
 *     &lt;enumeration value="Setpoint3"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SetpointSensorDTO")
@XmlEnum
public enum SetpointSensorDTO {

    @XmlEnumValue("Setpoint1")
    SETPOINT_1("Setpoint1"),
    @XmlEnumValue("Setpoint2")
    SETPOINT_2("Setpoint2"),
    @XmlEnumValue("Setpoint3")
    SETPOINT_3("Setpoint3");
    private final String value;

    SetpointSensorDTO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SetpointSensorDTO fromValue(String v) {
        for (SetpointSensorDTO c: SetpointSensorDTO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
