
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetLocationDescriptionRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLocationDescriptionRequest"
})
@XmlRootElement(name = "getLocationDescription")
public class GetLocationDescription {

    @XmlElement(name = "GetLocationDescriptionRequest", required = true)
    protected GetLocationDescriptionRequestType getLocationDescriptionRequest;

    /**
     * Gets the value of the getLocationDescriptionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetLocationDescriptionRequestType }
     *     
     */
    public GetLocationDescriptionRequestType getGetLocationDescriptionRequest() {
        return getLocationDescriptionRequest;
    }

    /**
     * Sets the value of the getLocationDescriptionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetLocationDescriptionRequestType }
     *     
     */
    public void setGetLocationDescriptionRequest(GetLocationDescriptionRequestType value) {
        this.getLocationDescriptionRequest = value;
    }

}
