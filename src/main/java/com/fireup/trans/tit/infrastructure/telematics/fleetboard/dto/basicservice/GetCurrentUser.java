
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetCurrentUserRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCurrentUserRequest"
})
@XmlRootElement(name = "getCurrentUser")
public class GetCurrentUser {

    @XmlElement(name = "GetCurrentUserRequest", required = true)
    protected GetCurrentUserRequest getCurrentUserRequest;

    /**
     * Gets the value of the getCurrentUserRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetCurrentUserRequest }
     *     
     */
    public GetCurrentUserRequest getGetCurrentUserRequest() {
        return getCurrentUserRequest;
    }

    /**
     * Sets the value of the getCurrentUserRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCurrentUserRequest }
     *     
     */
    public void setGetCurrentUserRequest(GetCurrentUserRequest value) {
        this.getCurrentUserRequest = value;
    }

}
