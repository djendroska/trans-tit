
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Long" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="Lat" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="PosText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Course" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Speed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="KM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GPSStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TracePositions" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TracePosition" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="GpsTime" type="{http://www.fleetboard.com/data}timestampType"/>
 *                             &lt;element name="Long" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                             &lt;element name="Lat" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="timestamp" type="{http://www.fleetboard.com/data}timestampType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_long",
    "lat",
    "posText",
    "course",
    "speed",
    "km",
    "gpsStatus",
    "tracePositions"
})
@XmlRootElement(name = "Position")
public class Position {

    @XmlElement(name = "Long")
    protected Float _long;
    @XmlElement(name = "Lat")
    protected Float lat;
    @XmlElement(name = "PosText")
    protected String posText;
    @XmlElement(name = "Course", required = true, nillable = true)
    protected String course;
    @XmlElement(name = "Speed", required = true, nillable = true)
    protected String speed;
    @XmlElement(name = "KM", required = true, nillable = true)
    protected String km;
    @XmlElement(name = "GPSStatus")
    protected String gpsStatus;
    @XmlElement(name = "TracePositions")
    protected Position.TracePositions tracePositions;
    @XmlAttribute(name = "timestamp")
    protected String timestamp;

    /**
     * Gets the value of the long property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getLong() {
        return _long;
    }

    /**
     * Sets the value of the long property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setLong(Float value) {
        this._long = value;
    }

    /**
     * Gets the value of the lat property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getLat() {
        return lat;
    }

    /**
     * Sets the value of the lat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setLat(Float value) {
        this.lat = value;
    }

    /**
     * Gets the value of the posText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosText() {
        return posText;
    }

    /**
     * Sets the value of the posText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosText(String value) {
        this.posText = value;
    }

    /**
     * Gets the value of the course property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCourse() {
        return course;
    }

    /**
     * Sets the value of the course property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCourse(String value) {
        this.course = value;
    }

    /**
     * Gets the value of the speed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * Sets the value of the speed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpeed(String value) {
        this.speed = value;
    }

    /**
     * Gets the value of the km property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKM() {
        return km;
    }

    /**
     * Sets the value of the km property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKM(String value) {
        this.km = value;
    }

    /**
     * Gets the value of the gpsStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGPSStatus() {
        return gpsStatus;
    }

    /**
     * Sets the value of the gpsStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGPSStatus(String value) {
        this.gpsStatus = value;
    }

    /**
     * Gets the value of the tracePositions property.
     * 
     * @return
     *     possible object is
     *     {@link Position.TracePositions }
     *     
     */
    public Position.TracePositions getTracePositions() {
        return tracePositions;
    }

    /**
     * Sets the value of the tracePositions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Position.TracePositions }
     *     
     */
    public void setTracePositions(Position.TracePositions value) {
        this.tracePositions = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimestamp(String value) {
        this.timestamp = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TracePosition" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="GpsTime" type="{http://www.fleetboard.com/data}timestampType"/>
     *                   &lt;element name="Long" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *                   &lt;element name="Lat" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tracePosition"
    })
    public static class TracePositions {

        @XmlElement(name = "TracePosition")
        protected List<Position.TracePositions.TracePosition> tracePosition;

        /**
         * Gets the value of the tracePosition property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tracePosition property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTracePosition().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Position.TracePositions.TracePosition }
         * 
         * 
         */
        public List<Position.TracePositions.TracePosition> getTracePosition() {
            if (tracePosition == null) {
                tracePosition = new ArrayList<Position.TracePositions.TracePosition>();
            }
            return this.tracePosition;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="GpsTime" type="{http://www.fleetboard.com/data}timestampType"/>
         *         &lt;element name="Long" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="Lat" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "gpsTime",
            "_long",
            "lat"
        })
        public static class TracePosition {

            @XmlElement(name = "GpsTime", required = true)
            protected String gpsTime;
            @XmlElement(name = "Long")
            protected float _long;
            @XmlElement(name = "Lat")
            protected float lat;

            /**
             * Gets the value of the gpsTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGpsTime() {
                return gpsTime;
            }

            /**
             * Sets the value of the gpsTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGpsTime(String value) {
                this.gpsTime = value;
            }

            /**
             * Gets the value of the long property.
             * 
             */
            public float getLong() {
                return _long;
            }

            /**
             * Sets the value of the long property.
             * 
             */
            public void setLong(float value) {
                this._long = value;
            }

            /**
             * Gets the value of the lat property.
             * 
             */
            public float getLat() {
                return lat;
            }

            /**
             * Sets the value of the lat property.
             * 
             */
            public void setLat(float value) {
                this.lat = value;
            }

        }

    }

}
