
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}TXMessage" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}RXMessage" minOccurs="0"/>
 *         &lt;element name="Request" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "txMessage",
    "rxMessage",
    "request"
})
@XmlRootElement(name = "Action")
public class Action {

    @XmlElement(name = "TXMessage")
    protected TXMessage txMessage;
    @XmlElement(name = "RXMessage")
    protected RXMessage rxMessage;
    @XmlElement(name = "Request")
    protected Action.Request request;

    /**
     * Gets the value of the txMessage property.
     * 
     * @return
     *     possible object is
     *     {@link TXMessage }
     *     
     */
    public TXMessage getTXMessage() {
        return txMessage;
    }

    /**
     * Sets the value of the txMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link TXMessage }
     *     
     */
    public void setTXMessage(TXMessage value) {
        this.txMessage = value;
    }

    /**
     * Gets the value of the rxMessage property.
     * 
     * @return
     *     possible object is
     *     {@link RXMessage }
     *     
     */
    public RXMessage getRXMessage() {
        return rxMessage;
    }

    /**
     * Sets the value of the rxMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link RXMessage }
     *     
     */
    public void setRXMessage(RXMessage value) {
        this.rxMessage = value;
    }

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link Action.Request }
     *     
     */
    public Action.Request getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link Action.Request }
     *     
     */
    public void setRequest(Action.Request value) {
        this.request = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service"
    })
    public static class Request {

        @XmlElement(name = "Service", required = true)
        protected List<String> service;

        /**
         * Gets the value of the service property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the service property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getService().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getService() {
            if (service == null) {
                service = new ArrayList<String>();
            }
            return this.service;
        }

    }

}
