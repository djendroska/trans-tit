
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TspAccountState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TspAccountState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNREGISTERED"/>
 *     &lt;enumeration value="REGISTER_PENDING"/>
 *     &lt;enumeration value="REGISTER_ERROR"/>
 *     &lt;enumeration value="REGISTERED"/>
 *     &lt;enumeration value="UNREGISTER_PENDING"/>
 *     &lt;enumeration value="UNREGISTER_ERROR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TspAccountState")
@XmlEnum
public enum TspAccountState {

    UNREGISTERED,
    REGISTER_PENDING,
    REGISTER_ERROR,
    REGISTERED,
    UNREGISTER_PENDING,
    UNREGISTER_ERROR;

    public String value() {
        return name();
    }

    public static TspAccountState fromValue(String v) {
        return valueOf(v);
    }

}
