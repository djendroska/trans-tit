
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for configurationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="configurationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AREAMONITORING"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "configurationType")
@XmlEnum
public enum ConfigurationType {

    AREAMONITORING;

    public String value() {
        return name();
    }

    public static ConfigurationType fromValue(String v) {
        return valueOf(v);
    }

}
