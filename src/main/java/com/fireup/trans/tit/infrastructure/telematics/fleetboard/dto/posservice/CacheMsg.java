
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DocType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CacheType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Info" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="doc_inoid" type="{http://www.fleetboard.com/data}inoidType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "docType",
    "cacheType",
    "info"
})
@XmlRootElement(name = "CacheMsg")
public class CacheMsg {

    @XmlElement(name = "DocType", required = true)
    protected String docType;
    @XmlElement(name = "CacheType", required = true)
    protected String cacheType;
    @XmlElement(name = "Info", required = true)
    protected String info;
    @XmlAttribute(name = "doc_inoid")
    protected String docInoid;

    /**
     * Gets the value of the docType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Sets the value of the docType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocType(String value) {
        this.docType = value;
    }

    /**
     * Gets the value of the cacheType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCacheType() {
        return cacheType;
    }

    /**
     * Sets the value of the cacheType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCacheType(String value) {
        this.cacheType = value;
    }

    /**
     * Gets the value of the info property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo() {
        return info;
    }

    /**
     * Sets the value of the info property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo(String value) {
        this.info = value;
    }

    /**
     * Gets the value of the docInoid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocInoid() {
        return docInoid;
    }

    /**
     * Sets the value of the docInoid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocInoid(String value) {
        this.docInoid = value;
    }

}
