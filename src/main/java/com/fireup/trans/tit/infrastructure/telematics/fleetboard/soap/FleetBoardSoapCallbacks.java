package com.fireup.trans.tit.infrastructure.telematics.fleetboard.soap;

public class FleetBoardSoapCallbacks {
    static final String LOGIN = "login";
    static final String LOGOUT = "logout";
    static final String GET_VEHICLE = "getVehicle";
    static final String JSESSION_ID = ";jsessionid=";
    static final String GET_LAST_POSITION = "getLastPosition";
}
