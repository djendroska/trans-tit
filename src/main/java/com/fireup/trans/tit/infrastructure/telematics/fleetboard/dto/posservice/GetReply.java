
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetReplyRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getReplyRequest"
})
@XmlRootElement(name = "getReply")
public class GetReply {

    @XmlElement(name = "GetReplyRequest", required = true)
    protected GetReplyRequestType getReplyRequest;

    /**
     * Gets the value of the getReplyRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetReplyRequestType }
     *     
     */
    public GetReplyRequestType getGetReplyRequest() {
        return getReplyRequest;
    }

    /**
     * Sets the value of the getReplyRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetReplyRequestType }
     *     
     */
    public void setGetReplyRequest(GetReplyRequestType value) {
        this.getReplyRequest = value;
    }

}
