
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetStatusDefRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getStatusDefRequest"
})
@XmlRootElement(name = "getStatusDef")
public class GetStatusDef {

    @XmlElement(name = "GetStatusDefRequest", required = true)
    protected GetStatusDefRequestType getStatusDefRequest;

    /**
     * Gets the value of the getStatusDefRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetStatusDefRequestType }
     *     
     */
    public GetStatusDefRequestType getGetStatusDefRequest() {
        return getStatusDefRequest;
    }

    /**
     * Sets the value of the getStatusDefRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetStatusDefRequestType }
     *     
     */
    public void setGetStatusDefRequest(GetStatusDefRequestType value) {
        this.getStatusDefRequest = value;
    }

}
