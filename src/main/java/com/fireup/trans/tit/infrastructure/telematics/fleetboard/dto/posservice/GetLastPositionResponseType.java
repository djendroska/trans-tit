
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetLastPositionResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetLastPositionResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Positions" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FleetID" type="{http://www.fleetboard.com/data}fleetidType"/>
 *                   &lt;element name="VehicleID" type="{http://www.fleetboard.com/data}vehicleidType"/>
 *                   &lt;element name="DriverNameID" type="{http://www.fleetboard.com/data}driverNameIDType"/>
 *                   &lt;element name="MotorStatus" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *                   &lt;element name="IgnitionStatus" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *                   &lt;element ref="{http://www.fleetboard.com/data}Position"/>
 *                   &lt;element name="StateOfVehicle" type="{http://www.fleetboard.com/data}LastPositionState" minOccurs="0"/>
 *                   &lt;element name="StateOfTimeManagement" type="{http://www.fleetboard.com/data}LastPositionState" minOccurs="0"/>
 *                   &lt;element name="StateOfOrderManagement" type="{http://www.fleetboard.com/data}LastPositionState" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="limit" type="{http://www.fleetboard.com/data}limitType" />
 *       &lt;attribute name="offset" type="{http://www.fleetboard.com/data}offsetType" />
 *       &lt;attribute name="resultSize" type="{http://www.fleetboard.com/data}resultSizeType" />
 *       &lt;attribute name="responseSize" type="{http://www.fleetboard.com/data}responseSizeType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetLastPositionResponseType", propOrder = {
    "positions"
})
public class GetLastPositionResponseType {

    @XmlElement(name = "Positions")
    protected List<GetLastPositionResponseType.Positions> positions;
    @XmlAttribute(name = "limit")
    protected BigInteger limit;
    @XmlAttribute(name = "offset")
    protected BigInteger offset;
    @XmlAttribute(name = "resultSize")
    protected BigInteger resultSize;
    @XmlAttribute(name = "responseSize")
    protected BigInteger responseSize;

    /**
     * Gets the value of the positions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the positions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPositions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetLastPositionResponseType.Positions }
     * 
     * 
     */
    public List<GetLastPositionResponseType.Positions> getPositions() {
        if (positions == null) {
            positions = new ArrayList<GetLastPositionResponseType.Positions>();
        }
        return this.positions;
    }

    /**
     * Gets the value of the limit property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLimit() {
        return limit;
    }

    /**
     * Sets the value of the limit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLimit(BigInteger value) {
        this.limit = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOffset(BigInteger value) {
        this.offset = value;
    }

    /**
     * Gets the value of the resultSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getResultSize() {
        return resultSize;
    }

    /**
     * Sets the value of the resultSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setResultSize(BigInteger value) {
        this.resultSize = value;
    }

    /**
     * Gets the value of the responseSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getResponseSize() {
        return responseSize;
    }

    /**
     * Sets the value of the responseSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setResponseSize(BigInteger value) {
        this.responseSize = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FleetID" type="{http://www.fleetboard.com/data}fleetidType"/>
     *         &lt;element name="VehicleID" type="{http://www.fleetboard.com/data}vehicleidType"/>
     *         &lt;element name="DriverNameID" type="{http://www.fleetboard.com/data}driverNameIDType"/>
     *         &lt;element name="MotorStatus" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
     *         &lt;element name="IgnitionStatus" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
     *         &lt;element ref="{http://www.fleetboard.com/data}Position"/>
     *         &lt;element name="StateOfVehicle" type="{http://www.fleetboard.com/data}LastPositionState" minOccurs="0"/>
     *         &lt;element name="StateOfTimeManagement" type="{http://www.fleetboard.com/data}LastPositionState" minOccurs="0"/>
     *         &lt;element name="StateOfOrderManagement" type="{http://www.fleetboard.com/data}LastPositionState" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fleetID",
        "vehicleID",
        "driverNameID",
        "motorStatus",
        "ignitionStatus",
        "position",
        "stateOfVehicle",
        "stateOfTimeManagement",
        "stateOfOrderManagement"
    })
    public static class Positions {

        @XmlElement(name = "FleetID")
        protected long fleetID;
        @XmlElement(name = "VehicleID")
        protected long vehicleID;
        @XmlElement(name = "DriverNameID")
        protected long driverNameID;
        @XmlElement(name = "MotorStatus")
        protected Short motorStatus;
        @XmlElement(name = "IgnitionStatus")
        protected Short ignitionStatus;
        @XmlElement(name = "Position", required = true)
        protected Position position;
        @XmlElement(name = "StateOfVehicle")
        protected LastPositionState stateOfVehicle;
        @XmlElement(name = "StateOfTimeManagement")
        protected LastPositionState stateOfTimeManagement;
        @XmlElement(name = "StateOfOrderManagement")
        protected LastPositionState stateOfOrderManagement;

        /**
         * Gets the value of the fleetID property.
         * 
         */
        public long getFleetID() {
            return fleetID;
        }

        /**
         * Sets the value of the fleetID property.
         * 
         */
        public void setFleetID(long value) {
            this.fleetID = value;
        }

        /**
         * Gets the value of the vehicleID property.
         * 
         */
        public long getVehicleID() {
            return vehicleID;
        }

        /**
         * Sets the value of the vehicleID property.
         * 
         */
        public void setVehicleID(long value) {
            this.vehicleID = value;
        }

        /**
         * Gets the value of the driverNameID property.
         * 
         */
        public long getDriverNameID() {
            return driverNameID;
        }

        /**
         * Sets the value of the driverNameID property.
         * 
         */
        public void setDriverNameID(long value) {
            this.driverNameID = value;
        }

        /**
         * Gets the value of the motorStatus property.
         * 
         * @return
         *     possible object is
         *     {@link Short }
         *     
         */
        public Short getMotorStatus() {
            return motorStatus;
        }

        /**
         * Sets the value of the motorStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link Short }
         *     
         */
        public void setMotorStatus(Short value) {
            this.motorStatus = value;
        }

        /**
         * Gets the value of the ignitionStatus property.
         * 
         * @return
         *     possible object is
         *     {@link Short }
         *     
         */
        public Short getIgnitionStatus() {
            return ignitionStatus;
        }

        /**
         * Sets the value of the ignitionStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link Short }
         *     
         */
        public void setIgnitionStatus(Short value) {
            this.ignitionStatus = value;
        }

        /**
         * Gets the value of the position property.
         * 
         * @return
         *     possible object is
         *     {@link Position }
         *     
         */
        public Position getPosition() {
            return position;
        }

        /**
         * Sets the value of the position property.
         * 
         * @param value
         *     allowed object is
         *     {@link Position }
         *     
         */
        public void setPosition(Position value) {
            this.position = value;
        }

        /**
         * Gets the value of the stateOfVehicle property.
         * 
         * @return
         *     possible object is
         *     {@link LastPositionState }
         *     
         */
        public LastPositionState getStateOfVehicle() {
            return stateOfVehicle;
        }

        /**
         * Sets the value of the stateOfVehicle property.
         * 
         * @param value
         *     allowed object is
         *     {@link LastPositionState }
         *     
         */
        public void setStateOfVehicle(LastPositionState value) {
            this.stateOfVehicle = value;
        }

        /**
         * Gets the value of the stateOfTimeManagement property.
         * 
         * @return
         *     possible object is
         *     {@link LastPositionState }
         *     
         */
        public LastPositionState getStateOfTimeManagement() {
            return stateOfTimeManagement;
        }

        /**
         * Sets the value of the stateOfTimeManagement property.
         * 
         * @param value
         *     allowed object is
         *     {@link LastPositionState }
         *     
         */
        public void setStateOfTimeManagement(LastPositionState value) {
            this.stateOfTimeManagement = value;
        }

        /**
         * Gets the value of the stateOfOrderManagement property.
         * 
         * @return
         *     possible object is
         *     {@link LastPositionState }
         *     
         */
        public LastPositionState getStateOfOrderManagement() {
            return stateOfOrderManagement;
        }

        /**
         * Sets the value of the stateOfOrderManagement property.
         * 
         * @param value
         *     allowed object is
         *     {@link LastPositionState }
         *     
         */
        public void setStateOfOrderManagement(LastPositionState value) {
            this.stateOfOrderManagement = value;
        }

    }

}
