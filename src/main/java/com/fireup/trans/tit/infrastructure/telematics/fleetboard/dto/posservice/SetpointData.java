
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="temperature" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="returnAirTemperature" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="supplyTemperature" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="evaporatorCoilTemperature" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="compartmentOperationMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "temperature",
    "returnAirTemperature",
    "supplyTemperature",
    "evaporatorCoilTemperature",
    "compartmentOperationMode"
})
@XmlRootElement(name = "SetpointData")
public class SetpointData {

    protected Double temperature;
    protected Double returnAirTemperature;
    protected Double supplyTemperature;
    protected Double evaporatorCoilTemperature;
    protected String compartmentOperationMode;

    /**
     * Gets the value of the temperature property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTemperature() {
        return temperature;
    }

    /**
     * Sets the value of the temperature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTemperature(Double value) {
        this.temperature = value;
    }

    /**
     * Gets the value of the returnAirTemperature property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getReturnAirTemperature() {
        return returnAirTemperature;
    }

    /**
     * Sets the value of the returnAirTemperature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setReturnAirTemperature(Double value) {
        this.returnAirTemperature = value;
    }

    /**
     * Gets the value of the supplyTemperature property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSupplyTemperature() {
        return supplyTemperature;
    }

    /**
     * Sets the value of the supplyTemperature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSupplyTemperature(Double value) {
        this.supplyTemperature = value;
    }

    /**
     * Gets the value of the evaporatorCoilTemperature property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getEvaporatorCoilTemperature() {
        return evaporatorCoilTemperature;
    }

    /**
     * Sets the value of the evaporatorCoilTemperature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setEvaporatorCoilTemperature(Double value) {
        this.evaporatorCoilTemperature = value;
    }

    /**
     * Gets the value of the compartmentOperationMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompartmentOperationMode() {
        return compartmentOperationMode;
    }

    /**
     * Sets the value of the compartmentOperationMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompartmentOperationMode(String value) {
        this.compartmentOperationMode = value;
    }

}
