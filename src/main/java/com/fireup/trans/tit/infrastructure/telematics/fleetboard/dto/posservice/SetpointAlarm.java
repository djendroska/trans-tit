
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setpointSensor" type="{http://www.fleetboard.com/data}SetpointSensorDTO" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}SetpointData" minOccurs="0"/>
 *         &lt;element name="threshold" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="isAllClear" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setpointSensor",
    "setpointData",
    "threshold",
    "isAllClear"
})
@XmlRootElement(name = "SetpointAlarm")
public class SetpointAlarm {

    @XmlSchemaType(name = "string")
    protected SetpointSensorDTO setpointSensor;
    @XmlElement(name = "SetpointData")
    protected SetpointData setpointData;
    protected Long threshold;
    protected Boolean isAllClear;

    /**
     * Gets the value of the setpointSensor property.
     * 
     * @return
     *     possible object is
     *     {@link SetpointSensorDTO }
     *     
     */
    public SetpointSensorDTO getSetpointSensor() {
        return setpointSensor;
    }

    /**
     * Sets the value of the setpointSensor property.
     * 
     * @param value
     *     allowed object is
     *     {@link SetpointSensorDTO }
     *     
     */
    public void setSetpointSensor(SetpointSensorDTO value) {
        this.setpointSensor = value;
    }

    /**
     * Gets the value of the setpointData property.
     * 
     * @return
     *     possible object is
     *     {@link SetpointData }
     *     
     */
    public SetpointData getSetpointData() {
        return setpointData;
    }

    /**
     * Sets the value of the setpointData property.
     * 
     * @param value
     *     allowed object is
     *     {@link SetpointData }
     *     
     */
    public void setSetpointData(SetpointData value) {
        this.setpointData = value;
    }

    /**
     * Gets the value of the threshold property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getThreshold() {
        return threshold;
    }

    /**
     * Sets the value of the threshold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setThreshold(Long value) {
        this.threshold = value;
    }

    /**
     * Gets the value of the isAllClear property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAllClear() {
        return isAllClear;
    }

    /**
     * Sets the value of the isAllClear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAllClear(Boolean value) {
        this.isAllClear = value;
    }

}
