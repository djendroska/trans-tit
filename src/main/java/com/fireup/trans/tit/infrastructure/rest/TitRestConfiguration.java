package com.fireup.trans.tit.infrastructure.rest;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class TitRestConfiguration extends WebMvcConfigurerAdapter {

    public void addInterceptors(final InterceptorRegistry registry) {

        registry.addInterceptor(new TitRestInterceptor())
                .addPathPatterns("/**");
    }
}
