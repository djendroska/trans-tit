
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tmObjectId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="documentInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tmObjectId",
    "documentInfo"
})
@XmlRootElement(name = "TmInfoDTO")
public class TmInfoDTO {

    protected long tmObjectId;
    protected String documentInfo;

    /**
     * Gets the value of the tmObjectId property.
     * 
     */
    public long getTmObjectId() {
        return tmObjectId;
    }

    /**
     * Sets the value of the tmObjectId property.
     * 
     */
    public void setTmObjectId(long value) {
        this.tmObjectId = value;
    }

    /**
     * Gets the value of the documentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentInfo() {
        return documentInfo;
    }

    /**
     * Sets the value of the documentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentInfo(String value) {
        this.documentInfo = value;
    }

}
