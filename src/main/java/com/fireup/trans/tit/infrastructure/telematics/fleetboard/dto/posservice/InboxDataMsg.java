
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TrailerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MessageType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AreaMonitoring" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AreaId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="AreaName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "trailerName",
    "messageType",
    "message",
    "areaMonitoring"
})
@XmlRootElement(name = "InboxDataMsg")
public class InboxDataMsg {

    @XmlElement(name = "ID", required = true)
    protected String id;
    @XmlElement(name = "TrailerName")
    protected String trailerName;
    @XmlElement(name = "MessageType", required = true)
    protected String messageType;
    @XmlElement(name = "Message", required = true)
    protected String message;
    @XmlElement(name = "AreaMonitoring")
    protected InboxDataMsg.AreaMonitoring areaMonitoring;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the trailerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrailerName() {
        return trailerName;
    }

    /**
     * Sets the value of the trailerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrailerName(String value) {
        this.trailerName = value;
    }

    /**
     * Gets the value of the messageType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * Sets the value of the messageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageType(String value) {
        this.messageType = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the areaMonitoring property.
     * 
     * @return
     *     possible object is
     *     {@link InboxDataMsg.AreaMonitoring }
     *     
     */
    public InboxDataMsg.AreaMonitoring getAreaMonitoring() {
        return areaMonitoring;
    }

    /**
     * Sets the value of the areaMonitoring property.
     * 
     * @param value
     *     allowed object is
     *     {@link InboxDataMsg.AreaMonitoring }
     *     
     */
    public void setAreaMonitoring(InboxDataMsg.AreaMonitoring value) {
        this.areaMonitoring = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AreaId" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="AreaName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "areaId",
        "areaName"
    })
    public static class AreaMonitoring {

        @XmlElement(name = "AreaId")
        protected long areaId;
        @XmlElement(name = "AreaName", required = true)
        protected String areaName;

        /**
         * Gets the value of the areaId property.
         * 
         */
        public long getAreaId() {
            return areaId;
        }

        /**
         * Sets the value of the areaId property.
         * 
         */
        public void setAreaId(long value) {
            this.areaId = value;
        }

        /**
         * Gets the value of the areaName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAreaName() {
            return areaName;
        }

        /**
         * Sets the value of the areaName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAreaName(String value) {
            this.areaName = value;
        }

    }

}
