
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="compartmentNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="compartmentOperationMode" type="{http://www.fleetboard.com/data}CompartmentOperationModeDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "compartmentNumber",
    "compartmentOperationMode"
})
@XmlRootElement(name = "CurrentCompartmentDataList")
public class CurrentCompartmentDataList {

    protected BigInteger compartmentNumber;
    @XmlSchemaType(name = "string")
    protected CompartmentOperationModeDTO compartmentOperationMode;

    /**
     * Gets the value of the compartmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCompartmentNumber() {
        return compartmentNumber;
    }

    /**
     * Sets the value of the compartmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCompartmentNumber(BigInteger value) {
        this.compartmentNumber = value;
    }

    /**
     * Gets the value of the compartmentOperationMode property.
     * 
     * @return
     *     possible object is
     *     {@link CompartmentOperationModeDTO }
     *     
     */
    public CompartmentOperationModeDTO getCompartmentOperationMode() {
        return compartmentOperationMode;
    }

    /**
     * Sets the value of the compartmentOperationMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompartmentOperationModeDTO }
     *     
     */
    public void setCompartmentOperationMode(CompartmentOperationModeDTO value) {
        this.compartmentOperationMode = value;
    }

}
