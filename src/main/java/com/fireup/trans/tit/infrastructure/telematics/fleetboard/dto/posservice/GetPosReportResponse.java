
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetPosReportResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPosReportResponse"
})
@XmlRootElement(name = "getPosReportResponse")
public class GetPosReportResponse {

    @XmlElement(name = "GetPosReportResponse", required = true)
    protected GetPosReportResponseType getPosReportResponse;

    /**
     * Gets the value of the getPosReportResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetPosReportResponseType }
     *     
     */
    public GetPosReportResponseType getGetPosReportResponse() {
        return getPosReportResponse;
    }

    /**
     * Sets the value of the getPosReportResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPosReportResponseType }
     *     
     */
    public void setGetPosReportResponse(GetPosReportResponseType value) {
        this.getPosReportResponse = value;
    }

}
