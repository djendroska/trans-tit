
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CurrentReeferOperationModeDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CurrentReeferOperationModeDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="StartStop"/>
 *     &lt;enumeration value="Continuous"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CurrentReeferOperationModeDTO")
@XmlEnum
public enum CurrentReeferOperationModeDTO {

    @XmlEnumValue("StartStop")
    START_STOP("StartStop"),
    @XmlEnumValue("Continuous")
    CONTINUOUS("Continuous");
    private final String value;

    CurrentReeferOperationModeDTO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CurrentReeferOperationModeDTO fromValue(String v) {
        for (CurrentReeferOperationModeDTO c: CurrentReeferOperationModeDTO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
