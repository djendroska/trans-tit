
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="temperatureSensor" type="{http://www.fleetboard.com/data}TemperatureSensorDTO" minOccurs="0"/>
 *         &lt;element name="currentTemperature" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="minThreshold" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="maxThreshold" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="isAllClear" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "temperatureSensor",
    "currentTemperature",
    "minThreshold",
    "maxThreshold",
    "isAllClear"
})
@XmlRootElement(name = "TemperatureAlarm")
public class TemperatureAlarm {

    @XmlSchemaType(name = "string")
    protected TemperatureSensorDTO temperatureSensor;
    protected Double currentTemperature;
    protected Long minThreshold;
    protected Long maxThreshold;
    protected Boolean isAllClear;

    /**
     * Gets the value of the temperatureSensor property.
     * 
     * @return
     *     possible object is
     *     {@link TemperatureSensorDTO }
     *     
     */
    public TemperatureSensorDTO getTemperatureSensor() {
        return temperatureSensor;
    }

    /**
     * Sets the value of the temperatureSensor property.
     * 
     * @param value
     *     allowed object is
     *     {@link TemperatureSensorDTO }
     *     
     */
    public void setTemperatureSensor(TemperatureSensorDTO value) {
        this.temperatureSensor = value;
    }

    /**
     * Gets the value of the currentTemperature property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCurrentTemperature() {
        return currentTemperature;
    }

    /**
     * Sets the value of the currentTemperature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCurrentTemperature(Double value) {
        this.currentTemperature = value;
    }

    /**
     * Gets the value of the minThreshold property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMinThreshold() {
        return minThreshold;
    }

    /**
     * Sets the value of the minThreshold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMinThreshold(Long value) {
        this.minThreshold = value;
    }

    /**
     * Gets the value of the maxThreshold property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMaxThreshold() {
        return maxThreshold;
    }

    /**
     * Sets the value of the maxThreshold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMaxThreshold(Long value) {
        this.maxThreshold = value;
    }

    /**
     * Gets the value of the isAllClear property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAllClear() {
        return isAllClear;
    }

    /**
     * Sets the value of the isAllClear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAllClear(Boolean value) {
        this.isAllClear = value;
    }

}
