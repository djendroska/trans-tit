
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="positionDataTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gpsTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="gpsHeading" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="gpsSpeed" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="gpsMileage" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="positionText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "positionDataTime",
    "gpsTime",
    "longitude",
    "latitude",
    "gpsHeading",
    "gpsSpeed",
    "gpsMileage",
    "positionText"
})
@XmlRootElement(name = "TrailerPositionDTO")
public class TrailerPositionDTO {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar positionDataTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar gpsTime;
    protected Double longitude;
    protected Double latitude;
    protected BigInteger gpsHeading;
    protected Long gpsSpeed;
    protected Double gpsMileage;
    protected String positionText;

    /**
     * Gets the value of the positionDataTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPositionDataTime() {
        return positionDataTime;
    }

    /**
     * Sets the value of the positionDataTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPositionDataTime(XMLGregorianCalendar value) {
        this.positionDataTime = value;
    }

    /**
     * Gets the value of the gpsTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGpsTime() {
        return gpsTime;
    }

    /**
     * Sets the value of the gpsTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGpsTime(XMLGregorianCalendar value) {
        this.gpsTime = value;
    }

    /**
     * Gets the value of the longitude property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * Sets the value of the longitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLongitude(Double value) {
        this.longitude = value;
    }

    /**
     * Gets the value of the latitude property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Sets the value of the latitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLatitude(Double value) {
        this.latitude = value;
    }

    /**
     * Gets the value of the gpsHeading property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getGpsHeading() {
        return gpsHeading;
    }

    /**
     * Sets the value of the gpsHeading property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setGpsHeading(BigInteger value) {
        this.gpsHeading = value;
    }

    /**
     * Gets the value of the gpsSpeed property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGpsSpeed() {
        return gpsSpeed;
    }

    /**
     * Sets the value of the gpsSpeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGpsSpeed(Long value) {
        this.gpsSpeed = value;
    }

    /**
     * Gets the value of the gpsMileage property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getGpsMileage() {
        return gpsMileage;
    }

    /**
     * Sets the value of the gpsMileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setGpsMileage(Double value) {
        this.gpsMileage = value;
    }

    /**
     * Gets the value of the positionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPositionText() {
        return positionText;
    }

    /**
     * Sets the value of the positionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPositionText(String value) {
        this.positionText = value;
    }

}
