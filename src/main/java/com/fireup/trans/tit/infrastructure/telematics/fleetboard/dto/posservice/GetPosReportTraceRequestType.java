
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPosReportTraceRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPosReportTraceRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VehicleId" type="{http://www.fleetboard.com/data}vehicleidType"/>
 *         &lt;element name="VehicleTimestamp" type="{http://www.fleetboard.com/data}TPDate" minOccurs="0"/>
 *         &lt;element name="ServerTimestamp" type="{http://www.fleetboard.com/data}TPDate" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="sessionid" type="{http://www.fleetboard.com/data}sessionidType" />
 *       &lt;attribute name="version" type="{http://www.fleetboard.com/data}versionType" />
 *       &lt;attribute name="limit" type="{http://www.fleetboard.com/data}limitType" />
 *       &lt;attribute name="offset" type="{http://www.fleetboard.com/data}offsetType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPosReportTraceRequestType", propOrder = {
    "vehicleId",
    "vehicleTimestamp",
    "serverTimestamp"
})
public class GetPosReportTraceRequestType {

    @XmlElement(name = "VehicleId")
    protected long vehicleId;
    @XmlElement(name = "VehicleTimestamp")
    protected TPDate vehicleTimestamp;
    @XmlElement(name = "ServerTimestamp")
    protected TPDate serverTimestamp;
    @XmlAttribute(name = "sessionid")
    protected String sessionid;
    @XmlAttribute(name = "version")
    protected Long version;
    @XmlAttribute(name = "limit")
    protected BigInteger limit;
    @XmlAttribute(name = "offset")
    protected BigInteger offset;

    /**
     * Gets the value of the vehicleId property.
     * 
     */
    public long getVehicleId() {
        return vehicleId;
    }

    /**
     * Sets the value of the vehicleId property.
     * 
     */
    public void setVehicleId(long value) {
        this.vehicleId = value;
    }

    /**
     * Gets the value of the vehicleTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link TPDate }
     *     
     */
    public TPDate getVehicleTimestamp() {
        return vehicleTimestamp;
    }

    /**
     * Sets the value of the vehicleTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPDate }
     *     
     */
    public void setVehicleTimestamp(TPDate value) {
        this.vehicleTimestamp = value;
    }

    /**
     * Gets the value of the serverTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link TPDate }
     *     
     */
    public TPDate getServerTimestamp() {
        return serverTimestamp;
    }

    /**
     * Sets the value of the serverTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPDate }
     *     
     */
    public void setServerTimestamp(TPDate value) {
        this.serverTimestamp = value;
    }

    /**
     * Gets the value of the sessionid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionid() {
        return sessionid;
    }

    /**
     * Sets the value of the sessionid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionid(String value) {
        this.sessionid = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVersion(Long value) {
        this.version = value;
    }

    /**
     * Gets the value of the limit property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLimit() {
        return limit;
    }

    /**
     * Sets the value of the limit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLimit(BigInteger value) {
        this.limit = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOffset(BigInteger value) {
        this.offset = value;
    }

}
