
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WatchboxAlarmReasonDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WatchboxAlarmReasonDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="WatchboxEntry"/>
 *     &lt;enumeration value="WatchboxExit"/>
 *     &lt;enumeration value="WatchboxEntryTimeDifference"/>
 *     &lt;enumeration value="WatchboxExitTimeDifference"/>
 *     &lt;enumeration value="WatchboxMonitoringFinished"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "WatchboxAlarmReasonDTO")
@XmlEnum
public enum WatchboxAlarmReasonDTO {

    @XmlEnumValue("WatchboxEntry")
    WATCHBOX_ENTRY("WatchboxEntry"),
    @XmlEnumValue("WatchboxExit")
    WATCHBOX_EXIT("WatchboxExit"),
    @XmlEnumValue("WatchboxEntryTimeDifference")
    WATCHBOX_ENTRY_TIME_DIFFERENCE("WatchboxEntryTimeDifference"),
    @XmlEnumValue("WatchboxExitTimeDifference")
    WATCHBOX_EXIT_TIME_DIFFERENCE("WatchboxExitTimeDifference"),
    @XmlEnumValue("WatchboxMonitoringFinished")
    WATCHBOX_MONITORING_FINISHED("WatchboxMonitoringFinished");
    private final String value;

    WatchboxAlarmReasonDTO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WatchboxAlarmReasonDTO fromValue(String v) {
        for (WatchboxAlarmReasonDTO c: WatchboxAlarmReasonDTO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
