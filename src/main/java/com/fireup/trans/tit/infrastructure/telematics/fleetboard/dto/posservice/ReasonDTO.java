
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="standard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="statusRequest" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="tagChangedAlarm" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ebsNotConnected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="brakeLiningAlarm" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="couplingAlarm" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="doorAlarm" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ignitionAlarm" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="reeferAlarm" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="reeferAlarmQueueChanged" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="reeferPresetListChanged" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="reeferCurrentPresetChanged" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="trailerStartStopAlarm" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TemperatureAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}SetpointAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}Ebs24NAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}MilageAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}ReeferStartStopContinousChanged" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}ReeferDieselElectricChanged" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}ReeferOperationModeChanged" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}FuelLevelAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TempLoggerMonitoringAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TirePressureAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}UndervoltageAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}VelocityAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}WatchboxAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}BogieLoadAlarm" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}HumidityAlarm" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "standard",
    "statusRequest",
    "tagChangedAlarm",
    "ebsNotConnected",
    "brakeLiningAlarm",
    "couplingAlarm",
    "doorAlarm",
    "ignitionAlarm",
    "reeferAlarm",
    "reeferAlarmQueueChanged",
    "reeferPresetListChanged",
    "reeferCurrentPresetChanged",
    "trailerStartStopAlarm",
    "temperatureAlarm",
    "setpointAlarm",
    "ebs24NAlarm",
    "milageAlarm",
    "reeferStartStopContinousChanged",
    "reeferDieselElectricChanged",
    "reeferOperationModeChanged",
    "fuelLevelAlarm",
    "tempLoggerMonitoringAlarm",
    "tirePressureAlarm",
    "undervoltageAlarm",
    "velocityAlarm",
    "watchboxAlarm",
    "bogieLoadAlarm",
    "humidityAlarm"
})
@XmlRootElement(name = "ReasonDTO")
public class ReasonDTO {

    protected Boolean standard;
    protected Boolean statusRequest;
    protected Boolean tagChangedAlarm;
    protected Boolean ebsNotConnected;
    protected Boolean brakeLiningAlarm;
    protected Boolean couplingAlarm;
    protected Boolean doorAlarm;
    protected Boolean ignitionAlarm;
    protected Boolean reeferAlarm;
    protected Boolean reeferAlarmQueueChanged;
    protected Boolean reeferPresetListChanged;
    protected Boolean reeferCurrentPresetChanged;
    protected Boolean trailerStartStopAlarm;
    @XmlElement(name = "TemperatureAlarm")
    protected TemperatureAlarm temperatureAlarm;
    @XmlElement(name = "SetpointAlarm")
    protected SetpointAlarm setpointAlarm;
    @XmlElement(name = "Ebs24NAlarm")
    protected Ebs24NAlarm ebs24NAlarm;
    @XmlElement(name = "MilageAlarm")
    protected MilageAlarm milageAlarm;
    @XmlElement(name = "ReeferStartStopContinousChanged")
    protected ReeferStartStopContinousChanged reeferStartStopContinousChanged;
    @XmlElement(name = "ReeferDieselElectricChanged")
    protected ReeferDieselElectricChanged reeferDieselElectricChanged;
    @XmlElement(name = "ReeferOperationModeChanged")
    protected ReeferOperationModeChanged reeferOperationModeChanged;
    @XmlElement(name = "FuelLevelAlarm")
    protected FuelLevelAlarm fuelLevelAlarm;
    @XmlElement(name = "TempLoggerMonitoringAlarm")
    protected TempLoggerMonitoringAlarm tempLoggerMonitoringAlarm;
    @XmlElement(name = "TirePressureAlarm")
    protected TirePressureAlarm tirePressureAlarm;
    @XmlElement(name = "UndervoltageAlarm")
    protected UndervoltageAlarm undervoltageAlarm;
    @XmlElement(name = "VelocityAlarm")
    protected VelocityAlarm velocityAlarm;
    @XmlElement(name = "WatchboxAlarm")
    protected WatchboxAlarm watchboxAlarm;
    @XmlElement(name = "BogieLoadAlarm")
    protected BogieLoadAlarm bogieLoadAlarm;
    @XmlElement(name = "HumidityAlarm")
    protected HumidityAlarm humidityAlarm;

    /**
     * Gets the value of the standard property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStandard() {
        return standard;
    }

    /**
     * Sets the value of the standard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStandard(Boolean value) {
        this.standard = value;
    }

    /**
     * Gets the value of the statusRequest property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStatusRequest() {
        return statusRequest;
    }

    /**
     * Sets the value of the statusRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStatusRequest(Boolean value) {
        this.statusRequest = value;
    }

    /**
     * Gets the value of the tagChangedAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTagChangedAlarm() {
        return tagChangedAlarm;
    }

    /**
     * Sets the value of the tagChangedAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTagChangedAlarm(Boolean value) {
        this.tagChangedAlarm = value;
    }

    /**
     * Gets the value of the ebsNotConnected property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEbsNotConnected() {
        return ebsNotConnected;
    }

    /**
     * Sets the value of the ebsNotConnected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEbsNotConnected(Boolean value) {
        this.ebsNotConnected = value;
    }

    /**
     * Gets the value of the brakeLiningAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBrakeLiningAlarm() {
        return brakeLiningAlarm;
    }

    /**
     * Sets the value of the brakeLiningAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBrakeLiningAlarm(Boolean value) {
        this.brakeLiningAlarm = value;
    }

    /**
     * Gets the value of the couplingAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCouplingAlarm() {
        return couplingAlarm;
    }

    /**
     * Sets the value of the couplingAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCouplingAlarm(Boolean value) {
        this.couplingAlarm = value;
    }

    /**
     * Gets the value of the doorAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoorAlarm() {
        return doorAlarm;
    }

    /**
     * Sets the value of the doorAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoorAlarm(Boolean value) {
        this.doorAlarm = value;
    }

    /**
     * Gets the value of the ignitionAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnitionAlarm() {
        return ignitionAlarm;
    }

    /**
     * Sets the value of the ignitionAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnitionAlarm(Boolean value) {
        this.ignitionAlarm = value;
    }

    /**
     * Gets the value of the reeferAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReeferAlarm() {
        return reeferAlarm;
    }

    /**
     * Sets the value of the reeferAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReeferAlarm(Boolean value) {
        this.reeferAlarm = value;
    }

    /**
     * Gets the value of the reeferAlarmQueueChanged property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReeferAlarmQueueChanged() {
        return reeferAlarmQueueChanged;
    }

    /**
     * Sets the value of the reeferAlarmQueueChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReeferAlarmQueueChanged(Boolean value) {
        this.reeferAlarmQueueChanged = value;
    }

    /**
     * Gets the value of the reeferPresetListChanged property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReeferPresetListChanged() {
        return reeferPresetListChanged;
    }

    /**
     * Sets the value of the reeferPresetListChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReeferPresetListChanged(Boolean value) {
        this.reeferPresetListChanged = value;
    }

    /**
     * Gets the value of the reeferCurrentPresetChanged property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReeferCurrentPresetChanged() {
        return reeferCurrentPresetChanged;
    }

    /**
     * Sets the value of the reeferCurrentPresetChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReeferCurrentPresetChanged(Boolean value) {
        this.reeferCurrentPresetChanged = value;
    }

    /**
     * Gets the value of the trailerStartStopAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTrailerStartStopAlarm() {
        return trailerStartStopAlarm;
    }

    /**
     * Sets the value of the trailerStartStopAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTrailerStartStopAlarm(Boolean value) {
        this.trailerStartStopAlarm = value;
    }

    /**
     * Gets the value of the temperatureAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link TemperatureAlarm }
     *     
     */
    public TemperatureAlarm getTemperatureAlarm() {
        return temperatureAlarm;
    }

    /**
     * Sets the value of the temperatureAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link TemperatureAlarm }
     *     
     */
    public void setTemperatureAlarm(TemperatureAlarm value) {
        this.temperatureAlarm = value;
    }

    /**
     * Gets the value of the setpointAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link SetpointAlarm }
     *     
     */
    public SetpointAlarm getSetpointAlarm() {
        return setpointAlarm;
    }

    /**
     * Sets the value of the setpointAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link SetpointAlarm }
     *     
     */
    public void setSetpointAlarm(SetpointAlarm value) {
        this.setpointAlarm = value;
    }

    /**
     * Gets the value of the ebs24NAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link Ebs24NAlarm }
     *     
     */
    public Ebs24NAlarm getEbs24NAlarm() {
        return ebs24NAlarm;
    }

    /**
     * Sets the value of the ebs24NAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ebs24NAlarm }
     *     
     */
    public void setEbs24NAlarm(Ebs24NAlarm value) {
        this.ebs24NAlarm = value;
    }

    /**
     * Gets the value of the milageAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link MilageAlarm }
     *     
     */
    public MilageAlarm getMilageAlarm() {
        return milageAlarm;
    }

    /**
     * Sets the value of the milageAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link MilageAlarm }
     *     
     */
    public void setMilageAlarm(MilageAlarm value) {
        this.milageAlarm = value;
    }

    /**
     * Gets the value of the reeferStartStopContinousChanged property.
     * 
     * @return
     *     possible object is
     *     {@link ReeferStartStopContinousChanged }
     *     
     */
    public ReeferStartStopContinousChanged getReeferStartStopContinousChanged() {
        return reeferStartStopContinousChanged;
    }

    /**
     * Sets the value of the reeferStartStopContinousChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReeferStartStopContinousChanged }
     *     
     */
    public void setReeferStartStopContinousChanged(ReeferStartStopContinousChanged value) {
        this.reeferStartStopContinousChanged = value;
    }

    /**
     * Gets the value of the reeferDieselElectricChanged property.
     * 
     * @return
     *     possible object is
     *     {@link ReeferDieselElectricChanged }
     *     
     */
    public ReeferDieselElectricChanged getReeferDieselElectricChanged() {
        return reeferDieselElectricChanged;
    }

    /**
     * Sets the value of the reeferDieselElectricChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReeferDieselElectricChanged }
     *     
     */
    public void setReeferDieselElectricChanged(ReeferDieselElectricChanged value) {
        this.reeferDieselElectricChanged = value;
    }

    /**
     * Gets the value of the reeferOperationModeChanged property.
     * 
     * @return
     *     possible object is
     *     {@link ReeferOperationModeChanged }
     *     
     */
    public ReeferOperationModeChanged getReeferOperationModeChanged() {
        return reeferOperationModeChanged;
    }

    /**
     * Sets the value of the reeferOperationModeChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReeferOperationModeChanged }
     *     
     */
    public void setReeferOperationModeChanged(ReeferOperationModeChanged value) {
        this.reeferOperationModeChanged = value;
    }

    /**
     * Gets the value of the fuelLevelAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link FuelLevelAlarm }
     *     
     */
    public FuelLevelAlarm getFuelLevelAlarm() {
        return fuelLevelAlarm;
    }

    /**
     * Sets the value of the fuelLevelAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link FuelLevelAlarm }
     *     
     */
    public void setFuelLevelAlarm(FuelLevelAlarm value) {
        this.fuelLevelAlarm = value;
    }

    /**
     * Gets the value of the tempLoggerMonitoringAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link TempLoggerMonitoringAlarm }
     *     
     */
    public TempLoggerMonitoringAlarm getTempLoggerMonitoringAlarm() {
        return tempLoggerMonitoringAlarm;
    }

    /**
     * Sets the value of the tempLoggerMonitoringAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link TempLoggerMonitoringAlarm }
     *     
     */
    public void setTempLoggerMonitoringAlarm(TempLoggerMonitoringAlarm value) {
        this.tempLoggerMonitoringAlarm = value;
    }

    /**
     * Gets the value of the tirePressureAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link TirePressureAlarm }
     *     
     */
    public TirePressureAlarm getTirePressureAlarm() {
        return tirePressureAlarm;
    }

    /**
     * Sets the value of the tirePressureAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link TirePressureAlarm }
     *     
     */
    public void setTirePressureAlarm(TirePressureAlarm value) {
        this.tirePressureAlarm = value;
    }

    /**
     * Gets the value of the undervoltageAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link UndervoltageAlarm }
     *     
     */
    public UndervoltageAlarm getUndervoltageAlarm() {
        return undervoltageAlarm;
    }

    /**
     * Sets the value of the undervoltageAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link UndervoltageAlarm }
     *     
     */
    public void setUndervoltageAlarm(UndervoltageAlarm value) {
        this.undervoltageAlarm = value;
    }

    /**
     * Gets the value of the velocityAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link VelocityAlarm }
     *     
     */
    public VelocityAlarm getVelocityAlarm() {
        return velocityAlarm;
    }

    /**
     * Sets the value of the velocityAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link VelocityAlarm }
     *     
     */
    public void setVelocityAlarm(VelocityAlarm value) {
        this.velocityAlarm = value;
    }

    /**
     * Gets the value of the watchboxAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link WatchboxAlarm }
     *     
     */
    public WatchboxAlarm getWatchboxAlarm() {
        return watchboxAlarm;
    }

    /**
     * Sets the value of the watchboxAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link WatchboxAlarm }
     *     
     */
    public void setWatchboxAlarm(WatchboxAlarm value) {
        this.watchboxAlarm = value;
    }

    /**
     * Gets the value of the bogieLoadAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link BogieLoadAlarm }
     *     
     */
    public BogieLoadAlarm getBogieLoadAlarm() {
        return bogieLoadAlarm;
    }

    /**
     * Sets the value of the bogieLoadAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link BogieLoadAlarm }
     *     
     */
    public void setBogieLoadAlarm(BogieLoadAlarm value) {
        this.bogieLoadAlarm = value;
    }

    /**
     * Gets the value of the humidityAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link HumidityAlarm }
     *     
     */
    public HumidityAlarm getHumidityAlarm() {
        return humidityAlarm;
    }

    /**
     * Sets the value of the humidityAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link HumidityAlarm }
     *     
     */
    public void setHumidityAlarm(HumidityAlarm value) {
        this.humidityAlarm = value;
    }

}
