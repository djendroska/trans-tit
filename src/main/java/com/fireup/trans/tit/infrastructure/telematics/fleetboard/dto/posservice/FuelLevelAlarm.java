
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currentFuelLevelColor" type="{http://www.fleetboard.com/data}CurrentFuelLevelColorDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "currentFuelLevelColor"
})
@XmlRootElement(name = "FuelLevelAlarm")
public class FuelLevelAlarm {

    @XmlSchemaType(name = "string")
    protected CurrentFuelLevelColorDTO currentFuelLevelColor;

    /**
     * Gets the value of the currentFuelLevelColor property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentFuelLevelColorDTO }
     *     
     */
    public CurrentFuelLevelColorDTO getCurrentFuelLevelColor() {
        return currentFuelLevelColor;
    }

    /**
     * Sets the value of the currentFuelLevelColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentFuelLevelColorDTO }
     *     
     */
    public void setCurrentFuelLevelColor(CurrentFuelLevelColorDTO value) {
        this.currentFuelLevelColor = value;
    }

}
