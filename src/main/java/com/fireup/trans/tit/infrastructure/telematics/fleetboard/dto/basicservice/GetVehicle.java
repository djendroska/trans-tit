
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetVehicleRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getVehicleRequest"
})
@XmlRootElement(name = "getVehicle")
public class GetVehicle {

    @XmlElement(name = "GetVehicleRequest", required = true)
    protected GetVehicleRequest getVehicleRequest;

    /**
     * Gets the value of the getVehicleRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetVehicleRequest }
     *     
     */
    public GetVehicleRequest getGetVehicleRequest() {
        return getVehicleRequest;
    }

    /**
     * Sets the value of the getVehicleRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetVehicleRequest }
     *     
     */
    public void setGetVehicleRequest(GetVehicleRequest value) {
        this.getVehicleRequest = value;
    }

}
