
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VehicleColumn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VehicleRow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}Hidden" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleColumn",
    "vehicleRow",
    "hidden"
})
public class VhcLayout {

    @XmlElement(name = "VehicleColumn")
    protected String vehicleColumn;
    @XmlElement(name = "VehicleRow")
    protected String vehicleRow;
    @XmlElement(name = "Hidden", nillable = true)
    protected Hidden hidden;

    /**
     * Gets the value of the vehicleColumn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleColumn() {
        return vehicleColumn;
    }

    /**
     * Sets the value of the vehicleColumn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleColumn(String value) {
        this.vehicleColumn = value;
    }

    /**
     * Gets the value of the vehicleRow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleRow() {
        return vehicleRow;
    }

    /**
     * Sets the value of the vehicleRow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleRow(String value) {
        this.vehicleRow = value;
    }

    /**
     * Gets the value of the hidden property.
     * 
     * @return
     *     possible object is
     *     {@link Hidden }
     *     
     */
    public Hidden getHidden() {
        return hidden;
    }

    /**
     * Sets the value of the hidden property.
     * 
     * @param value
     *     allowed object is
     *     {@link Hidden }
     *     
     */
    public void setHidden(Hidden value) {
        this.hidden = value;
    }

}
