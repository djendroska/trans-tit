
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReceiveRQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReadRQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "receiveRQ",
    "readRQ"
})
@XmlRootElement(name = "TXMessage")
public class TXMessage {

    @XmlElement(name = "ReceiveRQ")
    protected String receiveRQ;
    @XmlElement(name = "ReadRQ")
    protected String readRQ;

    /**
     * Gets the value of the receiveRQ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveRQ() {
        return receiveRQ;
    }

    /**
     * Sets the value of the receiveRQ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveRQ(String value) {
        this.receiveRQ = value;
    }

    /**
     * Gets the value of the readRQ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReadRQ() {
        return readRQ;
    }

    /**
     * Sets the value of the readRQ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReadRQ(String value) {
        this.readRQ = value;
    }

}
