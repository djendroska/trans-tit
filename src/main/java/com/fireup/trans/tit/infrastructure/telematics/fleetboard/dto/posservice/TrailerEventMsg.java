
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="trailerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vehicleId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="driverNameId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="coupled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ignitionOn" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="inMotion" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}ReasonDTO" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TrailerPositionDTO" minOccurs="0"/>
 *         &lt;element name="deviceTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="receiveTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="isDoor1Open" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isDoor2Open" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isDoor3Open" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isDoor4Open" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TmInfoDTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "trailerId",
    "vehicleId",
    "driverNameId",
    "coupled",
    "ignitionOn",
    "inMotion",
    "reasonDTO",
    "trailerPositionDTO",
    "deviceTime",
    "receiveTime",
    "isDoor1Open",
    "isDoor2Open",
    "isDoor3Open",
    "isDoor4Open",
    "tmInfoDTO"
})
@XmlRootElement(name = "TrailerEventMsg")
public class TrailerEventMsg {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected String trailerId;
    protected Long vehicleId;
    protected Long driverNameId;
    protected Boolean coupled;
    protected Boolean ignitionOn;
    protected Boolean inMotion;
    @XmlElement(name = "ReasonDTO")
    protected ReasonDTO reasonDTO;
    @XmlElement(name = "TrailerPositionDTO")
    protected TrailerPositionDTO trailerPositionDTO;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar deviceTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar receiveTime;
    protected Boolean isDoor1Open;
    protected Boolean isDoor2Open;
    protected Boolean isDoor3Open;
    protected Boolean isDoor4Open;
    @XmlElement(name = "TmInfoDTO")
    protected List<TmInfoDTO> tmInfoDTO;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the trailerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrailerId() {
        return trailerId;
    }

    /**
     * Sets the value of the trailerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrailerId(String value) {
        this.trailerId = value;
    }

    /**
     * Gets the value of the vehicleId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVehicleId() {
        return vehicleId;
    }

    /**
     * Sets the value of the vehicleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVehicleId(Long value) {
        this.vehicleId = value;
    }

    /**
     * Gets the value of the driverNameId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDriverNameId() {
        return driverNameId;
    }

    /**
     * Sets the value of the driverNameId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDriverNameId(Long value) {
        this.driverNameId = value;
    }

    /**
     * Gets the value of the coupled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCoupled() {
        return coupled;
    }

    /**
     * Sets the value of the coupled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCoupled(Boolean value) {
        this.coupled = value;
    }

    /**
     * Gets the value of the ignitionOn property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnitionOn() {
        return ignitionOn;
    }

    /**
     * Sets the value of the ignitionOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnitionOn(Boolean value) {
        this.ignitionOn = value;
    }

    /**
     * Gets the value of the inMotion property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInMotion() {
        return inMotion;
    }

    /**
     * Sets the value of the inMotion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInMotion(Boolean value) {
        this.inMotion = value;
    }

    /**
     * Gets the value of the reasonDTO property.
     * 
     * @return
     *     possible object is
     *     {@link ReasonDTO }
     *     
     */
    public ReasonDTO getReasonDTO() {
        return reasonDTO;
    }

    /**
     * Sets the value of the reasonDTO property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReasonDTO }
     *     
     */
    public void setReasonDTO(ReasonDTO value) {
        this.reasonDTO = value;
    }

    /**
     * Gets the value of the trailerPositionDTO property.
     * 
     * @return
     *     possible object is
     *     {@link TrailerPositionDTO }
     *     
     */
    public TrailerPositionDTO getTrailerPositionDTO() {
        return trailerPositionDTO;
    }

    /**
     * Sets the value of the trailerPositionDTO property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrailerPositionDTO }
     *     
     */
    public void setTrailerPositionDTO(TrailerPositionDTO value) {
        this.trailerPositionDTO = value;
    }

    /**
     * Gets the value of the deviceTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeviceTime() {
        return deviceTime;
    }

    /**
     * Sets the value of the deviceTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeviceTime(XMLGregorianCalendar value) {
        this.deviceTime = value;
    }

    /**
     * Gets the value of the receiveTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReceiveTime() {
        return receiveTime;
    }

    /**
     * Sets the value of the receiveTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReceiveTime(XMLGregorianCalendar value) {
        this.receiveTime = value;
    }

    /**
     * Gets the value of the isDoor1Open property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsDoor1Open() {
        return isDoor1Open;
    }

    /**
     * Sets the value of the isDoor1Open property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDoor1Open(Boolean value) {
        this.isDoor1Open = value;
    }

    /**
     * Gets the value of the isDoor2Open property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsDoor2Open() {
        return isDoor2Open;
    }

    /**
     * Sets the value of the isDoor2Open property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDoor2Open(Boolean value) {
        this.isDoor2Open = value;
    }

    /**
     * Gets the value of the isDoor3Open property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsDoor3Open() {
        return isDoor3Open;
    }

    /**
     * Sets the value of the isDoor3Open property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDoor3Open(Boolean value) {
        this.isDoor3Open = value;
    }

    /**
     * Gets the value of the isDoor4Open property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsDoor4Open() {
        return isDoor4Open;
    }

    /**
     * Sets the value of the isDoor4Open property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDoor4Open(Boolean value) {
        this.isDoor4Open = value;
    }

    /**
     * Gets the value of the tmInfoDTO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tmInfoDTO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTmInfoDTO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TmInfoDTO }
     * 
     * 
     */
    public List<TmInfoDTO> getTmInfoDTO() {
        if (tmInfoDTO == null) {
            tmInfoDTO = new ArrayList<TmInfoDTO>();
        }
        return this.tmInfoDTO;
    }

}
