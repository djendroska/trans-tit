
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FormDefID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}FieldsDef"/>
 *         &lt;element name="FormDefInoID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "name",
    "formDefID",
    "fieldsDef",
    "formDefInoID"
})
@XmlRootElement(name = "FormDefMsg")
public class FormDefMsg {

    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "FormDefID", required = true)
    protected BigInteger formDefID;
    @XmlElement(name = "FieldsDef", required = true, nillable = true)
    protected FieldsDef fieldsDef;
    @XmlElement(name = "FormDefInoID")
    protected String formDefInoID;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the formDefID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFormDefID() {
        return formDefID;
    }

    /**
     * Sets the value of the formDefID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFormDefID(BigInteger value) {
        this.formDefID = value;
    }

    /**
     * Gets the value of the fieldsDef property.
     * 
     * @return
     *     possible object is
     *     {@link FieldsDef }
     *     
     */
    public FieldsDef getFieldsDef() {
        return fieldsDef;
    }

    /**
     * Sets the value of the fieldsDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldsDef }
     *     
     */
    public void setFieldsDef(FieldsDef value) {
        this.fieldsDef = value;
    }

    /**
     * Gets the value of the formDefInoID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormDefInoID() {
        return formDefInoID;
    }

    /**
     * Sets the value of the formDefInoID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormDefInoID(String value) {
        this.formDefInoID = value;
    }

}
