package com.fireup.trans.tit.infrastructure.telematics.fleetboard.soap;

import java.util.Collections;
import java.util.List;

import org.apache.http.auth.InvalidCredentialsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice.LoginResponse2;
import com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice.GetLastPosition;
import com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice.GetLastPositionRequestType;
import com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice.GetLastPositionResponse;
import com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice.GetLastPositionResponseType;

public class PosServiceClient extends WebServiceGatewaySupport {

    @Autowired
    private BasicServiceClient fleetBoardBasicServiceClient;

    public List<GetLastPositionResponseType.Positions> getPositions(String username, String fleetName, String password)
            throws InvalidCredentialsException {
        LoginResponse2 session = fleetBoardBasicServiceClient.authenticate(username, fleetName, password);
        List<GetLastPositionResponseType.Positions> positions = getLastPosition(session);
        fleetBoardBasicServiceClient.logout(session);

        return positions;
    }

    private List<GetLastPositionResponseType.Positions> getLastPosition(final LoginResponse2 session) {
        final GetLastPositionRequestType request = new GetLastPositionRequestType();
        request.setSessionid(session.getLoginResponse().getSessionid());
        request.setQueryType((short) 0); //0 - positions of the vehicle, 1 - positions of the driver

        final GetLastPosition lastPosition = new GetLastPosition();
        lastPosition.setGetLastPositionRequest(request);

        final GetLastPositionResponse response = (GetLastPositionResponse) getWebServiceTemplate().marshalSendAndReceive(
                lastPosition,
                new SoapActionCallback(FleetBoardSoapCallbacks.GET_LAST_POSITION + FleetBoardSoapCallbacks.JSESSION_ID + session.getLoginResponse().getSessionid())
        );

        final List positions = response.getGetLastPositionResponse().getPositions();
        if (positions == null) {
            return Collections.EMPTY_LIST;
        }

        return positions;
    }

}
