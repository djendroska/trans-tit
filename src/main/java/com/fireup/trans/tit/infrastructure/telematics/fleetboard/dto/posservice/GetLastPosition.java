
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetLastPositionRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLastPositionRequest"
})
@XmlRootElement(name = "getLastPosition")
public class GetLastPosition {

    @XmlElement(name = "GetLastPositionRequest", required = true)
    protected GetLastPositionRequestType getLastPositionRequest;

    /**
     * Gets the value of the getLastPositionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetLastPositionRequestType }
     *     
     */
    public GetLastPositionRequestType getGetLastPositionRequest() {
        return getLastPositionRequest;
    }

    /**
     * Sets the value of the getLastPositionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetLastPositionRequestType }
     *     
     */
    public void setGetLastPositionRequest(GetLastPositionRequestType value) {
        this.getLastPositionRequest = value;
    }

}
