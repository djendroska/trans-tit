
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetLocationDescriptionResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLocationDescriptionResponse"
})
@XmlRootElement(name = "getLocationDescriptionResponse")
public class GetLocationDescriptionResponse {

    @XmlElement(name = "GetLocationDescriptionResponse", required = true)
    protected GetLocationDescriptionResponseType getLocationDescriptionResponse;

    /**
     * Gets the value of the getLocationDescriptionResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetLocationDescriptionResponseType }
     *     
     */
    public GetLocationDescriptionResponseType getGetLocationDescriptionResponse() {
        return getLocationDescriptionResponse;
    }

    /**
     * Sets the value of the getLocationDescriptionResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetLocationDescriptionResponseType }
     *     
     */
    public void setGetLocationDescriptionResponse(GetLocationDescriptionResponseType value) {
        this.getLocationDescriptionResponse = value;
    }

}
