
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ebs24NDelta" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ebs24NDelta"
})
@XmlRootElement(name = "Ebs24NAlarm")
public class Ebs24NAlarm {

    protected Long ebs24NDelta;

    /**
     * Gets the value of the ebs24NDelta property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEbs24NDelta() {
        return ebs24NDelta;
    }

    /**
     * Sets the value of the ebs24NDelta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEbs24NDelta(Long value) {
        this.ebs24NDelta = value;
    }

}
