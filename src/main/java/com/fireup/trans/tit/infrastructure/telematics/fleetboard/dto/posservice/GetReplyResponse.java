
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetReplyResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getReplyResponse"
})
@XmlRootElement(name = "getReplyResponse")
public class GetReplyResponse {

    @XmlElement(name = "GetReplyResponse", required = true)
    protected GetReplyResponseType getReplyResponse;

    /**
     * Gets the value of the getReplyResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetReplyResponseType }
     *     
     */
    public GetReplyResponseType getGetReplyResponse() {
        return getReplyResponse;
    }

    /**
     * Sets the value of the getReplyResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetReplyResponseType }
     *     
     */
    public void setGetReplyResponse(GetReplyResponseType value) {
        this.getReplyResponse = value;
    }

}
