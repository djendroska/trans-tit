
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FLEETID" type="{http://www.fleetboard.com/data}fleetidType"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}History" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}Header"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}Body" minOccurs="0"/>
 *         &lt;element name="ForeignRef" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="inoid" type="{http://www.fleetboard.com/data}inoidType" />
 *       &lt;attribute name="fbid" type="{http://www.fleetboard.com/data}fbidType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fleetid",
    "history",
    "header",
    "body",
    "foreignRef"
})
@XmlRootElement(name = "Msg")
public class Msg {

    @XmlElement(name = "FLEETID")
    protected long fleetid;
    @XmlElement(name = "History")
    protected History history;
    @XmlElement(name = "Header", required = true)
    protected Header header;
    @XmlElement(name = "Body", nillable = true)
    protected Body body;
    @XmlElement(name = "ForeignRef")
    protected List<Msg.ForeignRef> foreignRef;
    @XmlAttribute(name = "inoid")
    protected String inoid;
    @XmlAttribute(name = "fbid")
    protected String fbid;

    /**
     * Gets the value of the fleetid property.
     * 
     */
    public long getFLEETID() {
        return fleetid;
    }

    /**
     * Sets the value of the fleetid property.
     * 
     */
    public void setFLEETID(long value) {
        this.fleetid = value;
    }

    /**
     * Gets the value of the history property.
     * 
     * @return
     *     possible object is
     *     {@link History }
     *     
     */
    public History getHistory() {
        return history;
    }

    /**
     * Sets the value of the history property.
     * 
     * @param value
     *     allowed object is
     *     {@link History }
     *     
     */
    public void setHistory(History value) {
        this.history = value;
    }

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link Body }
     *     
     */
    public Body getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link Body }
     *     
     */
    public void setBody(Body value) {
        this.body = value;
    }

    /**
     * Gets the value of the foreignRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the foreignRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForeignRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Msg.ForeignRef }
     * 
     * 
     */
    public List<Msg.ForeignRef> getForeignRef() {
        if (foreignRef == null) {
            foreignRef = new ArrayList<Msg.ForeignRef>();
        }
        return this.foreignRef;
    }

    /**
     * Gets the value of the inoid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInoid() {
        return inoid;
    }

    /**
     * Sets the value of the inoid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInoid(String value) {
        this.inoid = value;
    }

    /**
     * Gets the value of the fbid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFbid() {
        return fbid;
    }

    /**
     * Sets the value of the fbid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFbid(String value) {
        this.fbid = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ForeignRef {

        @XmlAttribute(name = "name")
        protected String name;
        @XmlAttribute(name = "value")
        protected String value;

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }

}
