
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for telematicGroupIDType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="telematicGroupIDType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MB"/>
 *     &lt;enumeration value="AM"/>
 *     &lt;enumeration value="DISPOSITION"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "telematicGroupIDType")
@XmlEnum
public enum TelematicGroupIDType {

    MB,
    AM,
    DISPOSITION;

    public String value() {
        return name();
    }

    public static TelematicGroupIDType fromValue(String v) {
        return valueOf(v);
    }

}
