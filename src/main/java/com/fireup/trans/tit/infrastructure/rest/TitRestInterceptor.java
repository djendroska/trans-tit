package com.fireup.trans.tit.infrastructure.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TitRestInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        log.info("Received '{}' request", request.getRequestURI());
        return true;
    }

    @Override
    public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        switch (response.getStatus()) {
            case 200:
                log.info("Returned {} for request '{}'", response.getStatus(), request.getRequestURI());
                break;
            default:
                log.error("Returned {} for request '{}'", response.getStatus(), request.getRequestURI());
        }
    }
}
