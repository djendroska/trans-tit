
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetLocationDescriptionResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetLocationDescriptionResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}LocationDescription" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="resultSize" type="{http://www.fleetboard.com/data}resultSizeType" />
 *       &lt;attribute name="responseSize" type="{http://www.fleetboard.com/data}responseSizeType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetLocationDescriptionResponseType", propOrder = {
    "locationDescription"
})
public class GetLocationDescriptionResponseType {

    @XmlElement(name = "LocationDescription")
    protected List<LocationDescription> locationDescription;
    @XmlAttribute(name = "resultSize")
    protected BigInteger resultSize;
    @XmlAttribute(name = "responseSize")
    protected BigInteger responseSize;

    /**
     * Gets the value of the locationDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationDescription }
     * 
     * 
     */
    public List<LocationDescription> getLocationDescription() {
        if (locationDescription == null) {
            locationDescription = new ArrayList<LocationDescription>();
        }
        return this.locationDescription;
    }

    /**
     * Gets the value of the resultSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getResultSize() {
        return resultSize;
    }

    /**
     * Sets the value of the resultSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setResultSize(BigInteger value) {
        this.resultSize = value;
    }

    /**
     * Gets the value of the responseSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getResponseSize() {
        return responseSize;
    }

    /**
     * Sets the value of the responseSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setResponseSize(BigInteger value) {
        this.responseSize = value;
    }

}
