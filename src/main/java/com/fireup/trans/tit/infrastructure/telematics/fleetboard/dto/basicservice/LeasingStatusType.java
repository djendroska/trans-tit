
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for leasingStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="leasingStatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="LENT"/>
 *     &lt;enumeration value="RENTED"/>
 *     &lt;enumeration value="OWNED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "leasingStatusType")
@XmlEnum
public enum LeasingStatusType {

    LENT,
    RENTED,
    OWNED;

    public String value() {
        return name();
    }

    public static LeasingStatusType fromValue(String v) {
        return valueOf(v);
    }

}
