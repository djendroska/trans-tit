
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Maxlength" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Taborder" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Addressmapping" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "maxlength",
    "taborder",
    "addressmapping",
    "priority"
})
@XmlRootElement(name = "Behavior")
public class Behavior {

    @XmlElement(name = "Maxlength", required = true)
    protected String maxlength;
    @XmlElement(name = "Taborder", required = true)
    protected String taborder;
    @XmlElement(name = "Addressmapping", required = true)
    protected String addressmapping;
    @XmlElement(name = "Priority", required = true)
    protected String priority;

    /**
     * Gets the value of the maxlength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxlength() {
        return maxlength;
    }

    /**
     * Sets the value of the maxlength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxlength(String value) {
        this.maxlength = value;
    }

    /**
     * Gets the value of the taborder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaborder() {
        return taborder;
    }

    /**
     * Sets the value of the taborder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaborder(String value) {
        this.taborder = value;
    }

    /**
     * Gets the value of the addressmapping property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressmapping() {
        return addressmapping;
    }

    /**
     * Sets the value of the addressmapping property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressmapping(String value) {
        this.addressmapping = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

}
