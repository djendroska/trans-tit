
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="trailerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vehicleId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="trailerCoupled" type="{http://www.fleetboard.com/data}TrailerCoupled"/>
 *         &lt;element name="updateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "trailerId",
    "vehicleId",
    "trailerCoupled",
    "updateTime"
})
@XmlRootElement(name = "TrailerCouplingMsg")
public class TrailerCouplingMsg {

    @XmlElement(required = true)
    protected String trailerId;
    protected Long vehicleId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TrailerCoupled trailerCoupled;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updateTime;

    /**
     * Gets the value of the trailerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrailerId() {
        return trailerId;
    }

    /**
     * Sets the value of the trailerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrailerId(String value) {
        this.trailerId = value;
    }

    /**
     * Gets the value of the vehicleId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVehicleId() {
        return vehicleId;
    }

    /**
     * Sets the value of the vehicleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVehicleId(Long value) {
        this.vehicleId = value;
    }

    /**
     * Gets the value of the trailerCoupled property.
     * 
     * @return
     *     possible object is
     *     {@link TrailerCoupled }
     *     
     */
    public TrailerCoupled getTrailerCoupled() {
        return trailerCoupled;
    }

    /**
     * Sets the value of the trailerCoupled property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrailerCoupled }
     *     
     */
    public void setTrailerCoupled(TrailerCoupled value) {
        this.trailerCoupled = value;
    }

    /**
     * Gets the value of the updateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUpdateTime() {
        return updateTime;
    }

    /**
     * Sets the value of the updateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUpdateTime(XMLGregorianCalendar value) {
        this.updateTime = value;
    }

}
