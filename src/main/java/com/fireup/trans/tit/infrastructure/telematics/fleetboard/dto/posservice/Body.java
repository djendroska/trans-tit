
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FreetextMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}OrderMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}StatusDefMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}FormDefMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}AutoPosDef" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}VehiclestatusMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}DriverStatusMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}OrderstatusMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TourstatusMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}ReplystatusMsg" minOccurs="0"/>
 *         &lt;element name="SimcardChgMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HexMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}CacheMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TextConfMsg" minOccurs="0"/>
 *         &lt;element name="VehiclereplyMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}InboxDataMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}EventStatusMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}PairingMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TrailerEventMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TrailerRegisterMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TrailerSyncMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TourEventUptimeMsg" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}TrailerCouplingMsg" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "freetextMsg",
    "orderMsg",
    "statusDefMsg",
    "formDefMsg",
    "autoPosDef",
    "vehiclestatusMsg",
    "driverStatusMsg",
    "orderstatusMsg",
    "tourstatusMsg",
    "replystatusMsg",
    "simcardChgMsg",
    "hexMsg",
    "cacheMsg",
    "textConfMsg",
    "vehiclereplyMsg",
    "inboxDataMsg",
    "eventStatusMsg",
    "pairingMsg",
    "trailerEventMsg",
    "trailerRegisterMsg",
    "trailerSyncMsg",
    "tourEventUptimeMsg",
    "trailerCouplingMsg"
})
public class Body {

    @XmlElement(name = "FreetextMsg")
    protected String freetextMsg;
    @XmlElement(name = "OrderMsg")
    protected OrderMsg orderMsg;
    @XmlElement(name = "StatusDefMsg")
    protected StatusDefMsg statusDefMsg;
    @XmlElement(name = "FormDefMsg")
    protected FormDefMsg formDefMsg;
    @XmlElement(name = "AutoPosDef")
    protected AutoPosDef autoPosDef;
    @XmlElement(name = "VehiclestatusMsg")
    protected VehiclestatusMsg vehiclestatusMsg;
    @XmlElement(name = "DriverStatusMsg")
    protected DriverStatusMsg driverStatusMsg;
    @XmlElement(name = "OrderstatusMsg")
    protected OrderstatusMsg orderstatusMsg;
    @XmlElement(name = "TourstatusMsg")
    protected TourstatusMsg tourstatusMsg;
    @XmlElement(name = "ReplystatusMsg")
    protected ReplystatusMsg replystatusMsg;
    @XmlElement(name = "SimcardChgMsg")
    protected String simcardChgMsg;
    @XmlElement(name = "HexMsg")
    protected String hexMsg;
    @XmlElement(name = "CacheMsg")
    protected CacheMsg cacheMsg;
    @XmlElement(name = "TextConfMsg")
    protected TextConfMsg textConfMsg;
    @XmlElement(name = "VehiclereplyMsg")
    protected String vehiclereplyMsg;
    @XmlElement(name = "InboxDataMsg")
    protected InboxDataMsg inboxDataMsg;
    @XmlElement(name = "EventStatusMsg")
    protected EventStatusMsg eventStatusMsg;
    @XmlElement(name = "PairingMsg")
    protected PairingMsg pairingMsg;
    @XmlElement(name = "TrailerEventMsg")
    protected TrailerEventMsg trailerEventMsg;
    @XmlElement(name = "TrailerRegisterMsg")
    protected TrailerRegisterMsg trailerRegisterMsg;
    @XmlElement(name = "TrailerSyncMsg")
    protected TrailerSyncMsg trailerSyncMsg;
    @XmlElement(name = "TourEventUptimeMsg")
    protected TourEventUptimeMsg tourEventUptimeMsg;
    @XmlElement(name = "TrailerCouplingMsg")
    protected TrailerCouplingMsg trailerCouplingMsg;

    /**
     * Gets the value of the freetextMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreetextMsg() {
        return freetextMsg;
    }

    /**
     * Sets the value of the freetextMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreetextMsg(String value) {
        this.freetextMsg = value;
    }

    /**
     * Gets the value of the orderMsg property.
     * 
     * @return
     *     possible object is
     *     {@link OrderMsg }
     *     
     */
    public OrderMsg getOrderMsg() {
        return orderMsg;
    }

    /**
     * Sets the value of the orderMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderMsg }
     *     
     */
    public void setOrderMsg(OrderMsg value) {
        this.orderMsg = value;
    }

    /**
     * Gets the value of the statusDefMsg property.
     * 
     * @return
     *     possible object is
     *     {@link StatusDefMsg }
     *     
     */
    public StatusDefMsg getStatusDefMsg() {
        return statusDefMsg;
    }

    /**
     * Sets the value of the statusDefMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusDefMsg }
     *     
     */
    public void setStatusDefMsg(StatusDefMsg value) {
        this.statusDefMsg = value;
    }

    /**
     * Gets the value of the formDefMsg property.
     * 
     * @return
     *     possible object is
     *     {@link FormDefMsg }
     *     
     */
    public FormDefMsg getFormDefMsg() {
        return formDefMsg;
    }

    /**
     * Sets the value of the formDefMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormDefMsg }
     *     
     */
    public void setFormDefMsg(FormDefMsg value) {
        this.formDefMsg = value;
    }

    /**
     * Gets the value of the autoPosDef property.
     * 
     * @return
     *     possible object is
     *     {@link AutoPosDef }
     *     
     */
    public AutoPosDef getAutoPosDef() {
        return autoPosDef;
    }

    /**
     * Sets the value of the autoPosDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoPosDef }
     *     
     */
    public void setAutoPosDef(AutoPosDef value) {
        this.autoPosDef = value;
    }

    /**
     * Gets the value of the vehiclestatusMsg property.
     * 
     * @return
     *     possible object is
     *     {@link VehiclestatusMsg }
     *     
     */
    public VehiclestatusMsg getVehiclestatusMsg() {
        return vehiclestatusMsg;
    }

    /**
     * Sets the value of the vehiclestatusMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehiclestatusMsg }
     *     
     */
    public void setVehiclestatusMsg(VehiclestatusMsg value) {
        this.vehiclestatusMsg = value;
    }

    /**
     * Gets the value of the driverStatusMsg property.
     * 
     * @return
     *     possible object is
     *     {@link DriverStatusMsg }
     *     
     */
    public DriverStatusMsg getDriverStatusMsg() {
        return driverStatusMsg;
    }

    /**
     * Sets the value of the driverStatusMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverStatusMsg }
     *     
     */
    public void setDriverStatusMsg(DriverStatusMsg value) {
        this.driverStatusMsg = value;
    }

    /**
     * Gets the value of the orderstatusMsg property.
     * 
     * @return
     *     possible object is
     *     {@link OrderstatusMsg }
     *     
     */
    public OrderstatusMsg getOrderstatusMsg() {
        return orderstatusMsg;
    }

    /**
     * Sets the value of the orderstatusMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderstatusMsg }
     *     
     */
    public void setOrderstatusMsg(OrderstatusMsg value) {
        this.orderstatusMsg = value;
    }

    /**
     * Gets the value of the tourstatusMsg property.
     * 
     * @return
     *     possible object is
     *     {@link TourstatusMsg }
     *     
     */
    public TourstatusMsg getTourstatusMsg() {
        return tourstatusMsg;
    }

    /**
     * Sets the value of the tourstatusMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link TourstatusMsg }
     *     
     */
    public void setTourstatusMsg(TourstatusMsg value) {
        this.tourstatusMsg = value;
    }

    /**
     * Gets the value of the replystatusMsg property.
     * 
     * @return
     *     possible object is
     *     {@link ReplystatusMsg }
     *     
     */
    public ReplystatusMsg getReplystatusMsg() {
        return replystatusMsg;
    }

    /**
     * Sets the value of the replystatusMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplystatusMsg }
     *     
     */
    public void setReplystatusMsg(ReplystatusMsg value) {
        this.replystatusMsg = value;
    }

    /**
     * Gets the value of the simcardChgMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSimcardChgMsg() {
        return simcardChgMsg;
    }

    /**
     * Sets the value of the simcardChgMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSimcardChgMsg(String value) {
        this.simcardChgMsg = value;
    }

    /**
     * Gets the value of the hexMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHexMsg() {
        return hexMsg;
    }

    /**
     * Sets the value of the hexMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHexMsg(String value) {
        this.hexMsg = value;
    }

    /**
     * Gets the value of the cacheMsg property.
     * 
     * @return
     *     possible object is
     *     {@link CacheMsg }
     *     
     */
    public CacheMsg getCacheMsg() {
        return cacheMsg;
    }

    /**
     * Sets the value of the cacheMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link CacheMsg }
     *     
     */
    public void setCacheMsg(CacheMsg value) {
        this.cacheMsg = value;
    }

    /**
     * Gets the value of the textConfMsg property.
     * 
     * @return
     *     possible object is
     *     {@link TextConfMsg }
     *     
     */
    public TextConfMsg getTextConfMsg() {
        return textConfMsg;
    }

    /**
     * Sets the value of the textConfMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextConfMsg }
     *     
     */
    public void setTextConfMsg(TextConfMsg value) {
        this.textConfMsg = value;
    }

    /**
     * Gets the value of the vehiclereplyMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehiclereplyMsg() {
        return vehiclereplyMsg;
    }

    /**
     * Sets the value of the vehiclereplyMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehiclereplyMsg(String value) {
        this.vehiclereplyMsg = value;
    }

    /**
     * Gets the value of the inboxDataMsg property.
     * 
     * @return
     *     possible object is
     *     {@link InboxDataMsg }
     *     
     */
    public InboxDataMsg getInboxDataMsg() {
        return inboxDataMsg;
    }

    /**
     * Sets the value of the inboxDataMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link InboxDataMsg }
     *     
     */
    public void setInboxDataMsg(InboxDataMsg value) {
        this.inboxDataMsg = value;
    }

    /**
     * Gets the value of the eventStatusMsg property.
     * 
     * @return
     *     possible object is
     *     {@link EventStatusMsg }
     *     
     */
    public EventStatusMsg getEventStatusMsg() {
        return eventStatusMsg;
    }

    /**
     * Sets the value of the eventStatusMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventStatusMsg }
     *     
     */
    public void setEventStatusMsg(EventStatusMsg value) {
        this.eventStatusMsg = value;
    }

    /**
     * Gets the value of the pairingMsg property.
     * 
     * @return
     *     possible object is
     *     {@link PairingMsg }
     *     
     */
    public PairingMsg getPairingMsg() {
        return pairingMsg;
    }

    /**
     * Sets the value of the pairingMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link PairingMsg }
     *     
     */
    public void setPairingMsg(PairingMsg value) {
        this.pairingMsg = value;
    }

    /**
     * Gets the value of the trailerEventMsg property.
     * 
     * @return
     *     possible object is
     *     {@link TrailerEventMsg }
     *     
     */
    public TrailerEventMsg getTrailerEventMsg() {
        return trailerEventMsg;
    }

    /**
     * Sets the value of the trailerEventMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrailerEventMsg }
     *     
     */
    public void setTrailerEventMsg(TrailerEventMsg value) {
        this.trailerEventMsg = value;
    }

    /**
     * Gets the value of the trailerRegisterMsg property.
     * 
     * @return
     *     possible object is
     *     {@link TrailerRegisterMsg }
     *     
     */
    public TrailerRegisterMsg getTrailerRegisterMsg() {
        return trailerRegisterMsg;
    }

    /**
     * Sets the value of the trailerRegisterMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrailerRegisterMsg }
     *     
     */
    public void setTrailerRegisterMsg(TrailerRegisterMsg value) {
        this.trailerRegisterMsg = value;
    }

    /**
     * Gets the value of the trailerSyncMsg property.
     * 
     * @return
     *     possible object is
     *     {@link TrailerSyncMsg }
     *     
     */
    public TrailerSyncMsg getTrailerSyncMsg() {
        return trailerSyncMsg;
    }

    /**
     * Sets the value of the trailerSyncMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrailerSyncMsg }
     *     
     */
    public void setTrailerSyncMsg(TrailerSyncMsg value) {
        this.trailerSyncMsg = value;
    }

    /**
     * Gets the value of the tourEventUptimeMsg property.
     * 
     * @return
     *     possible object is
     *     {@link TourEventUptimeMsg }
     *     
     */
    public TourEventUptimeMsg getTourEventUptimeMsg() {
        return tourEventUptimeMsg;
    }

    /**
     * Sets the value of the tourEventUptimeMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link TourEventUptimeMsg }
     *     
     */
    public void setTourEventUptimeMsg(TourEventUptimeMsg value) {
        this.tourEventUptimeMsg = value;
    }

    /**
     * Gets the value of the trailerCouplingMsg property.
     * 
     * @return
     *     possible object is
     *     {@link TrailerCouplingMsg }
     *     
     */
    public TrailerCouplingMsg getTrailerCouplingMsg() {
        return trailerCouplingMsg;
    }

    /**
     * Sets the value of the trailerCouplingMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrailerCouplingMsg }
     *     
     */
    public void setTrailerCouplingMsg(TrailerCouplingMsg value) {
        this.trailerCouplingMsg = value;
    }

}
