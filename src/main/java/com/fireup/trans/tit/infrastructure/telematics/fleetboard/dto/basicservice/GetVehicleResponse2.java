
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetVehicleResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getVehicleResponse"
})
@XmlRootElement(name = "getVehicleResponse")
public class GetVehicleResponse2 {

    @XmlElement(name = "GetVehicleResponse", required = true)
    protected GetVehicleResponse getVehicleResponse;

    /**
     * Gets the value of the getVehicleResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetVehicleResponse }
     *     
     */
    public GetVehicleResponse getGetVehicleResponse() {
        return getVehicleResponse;
    }

    /**
     * Sets the value of the getVehicleResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetVehicleResponse }
     *     
     */
    public void setGetVehicleResponse(GetVehicleResponse value) {
        this.getVehicleResponse = value;
    }

}
