
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetPosReportTraceResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPosReportTraceResponse"
})
@XmlRootElement(name = "getPosReportTraceResponse")
public class GetPosReportTraceResponse {

    @XmlElement(name = "GetPosReportTraceResponse", required = true)
    protected GetPosReportTraceResponseType getPosReportTraceResponse;

    /**
     * Gets the value of the getPosReportTraceResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetPosReportTraceResponseType }
     *     
     */
    public GetPosReportTraceResponseType getGetPosReportTraceResponse() {
        return getPosReportTraceResponse;
    }

    /**
     * Sets the value of the getPosReportTraceResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPosReportTraceResponseType }
     *     
     */
    public void setGetPosReportTraceResponse(GetPosReportTraceResponseType value) {
        this.getPosReportTraceResponse = value;
    }

}
