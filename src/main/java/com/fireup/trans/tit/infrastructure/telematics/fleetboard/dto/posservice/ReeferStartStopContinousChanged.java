
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currentReeferOperationMode" type="{http://www.fleetboard.com/data}CurrentReeferOperationModeDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "currentReeferOperationMode"
})
@XmlRootElement(name = "ReeferStartStopContinousChanged")
public class ReeferStartStopContinousChanged {

    @XmlSchemaType(name = "string")
    protected CurrentReeferOperationModeDTO currentReeferOperationMode;

    /**
     * Gets the value of the currentReeferOperationMode property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentReeferOperationModeDTO }
     *     
     */
    public CurrentReeferOperationModeDTO getCurrentReeferOperationMode() {
        return currentReeferOperationMode;
    }

    /**
     * Sets the value of the currentReeferOperationMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentReeferOperationModeDTO }
     *     
     */
    public void setCurrentReeferOperationMode(CurrentReeferOperationModeDTO value) {
        this.currentReeferOperationMode = value;
    }

}
