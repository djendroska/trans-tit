
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tirePosition" type="{http://www.fleetboard.com/data}TirePositionDTO" minOccurs="0"/>
 *         &lt;element name="currentPressure" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="threshold" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tirePosition",
    "currentPressure",
    "threshold"
})
@XmlRootElement(name = "TirePressureAlarm")
public class TirePressureAlarm {

    @XmlSchemaType(name = "string")
    protected TirePositionDTO tirePosition;
    protected Double currentPressure;
    protected Double threshold;

    /**
     * Gets the value of the tirePosition property.
     * 
     * @return
     *     possible object is
     *     {@link TirePositionDTO }
     *     
     */
    public TirePositionDTO getTirePosition() {
        return tirePosition;
    }

    /**
     * Sets the value of the tirePosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link TirePositionDTO }
     *     
     */
    public void setTirePosition(TirePositionDTO value) {
        this.tirePosition = value;
    }

    /**
     * Gets the value of the currentPressure property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCurrentPressure() {
        return currentPressure;
    }

    /**
     * Sets the value of the currentPressure property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCurrentPressure(Double value) {
        this.currentPressure = value;
    }

    /**
     * Gets the value of the threshold property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getThreshold() {
        return threshold;
    }

    /**
     * Sets the value of the threshold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setThreshold(Double value) {
        this.threshold = value;
    }

}
