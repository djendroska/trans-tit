
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetPosReportRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPosReportRequest"
})
@XmlRootElement(name = "getPosReport")
public class GetPosReport {

    @XmlElement(name = "GetPosReportRequest", required = true)
    protected GetPosReportRequestType getPosReportRequest;

    /**
     * Gets the value of the getPosReportRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetPosReportRequestType }
     *     
     */
    public GetPosReportRequestType getGetPosReportRequest() {
        return getPosReportRequest;
    }

    /**
     * Sets the value of the getPosReportRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPosReportRequestType }
     *     
     */
    public void setGetPosReportRequest(GetPosReportRequestType value) {
        this.getPosReportRequest = value;
    }

}
