
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CurrentFuelLevelColorDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CurrentFuelLevelColorDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="red"/>
 *     &lt;enumeration value="yellow"/>
 *     &lt;enumeration value="green"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CurrentFuelLevelColorDTO")
@XmlEnum
public enum CurrentFuelLevelColorDTO {

    @XmlEnumValue("red")
    RED("red"),
    @XmlEnumValue("yellow")
    YELLOW("yellow"),
    @XmlEnumValue("green")
    GREEN("green");
    private final String value;

    CurrentFuelLevelColorDTO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CurrentFuelLevelColorDTO fromValue(String v) {
        for (CurrentFuelLevelColorDTO c: CurrentFuelLevelColorDTO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
