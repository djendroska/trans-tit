
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currentPowerMode" type="{http://www.fleetboard.com/data}CurrentPowerModeDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "currentPowerMode"
})
@XmlRootElement(name = "ReeferDieselElectricChanged")
public class ReeferDieselElectricChanged {

    @XmlSchemaType(name = "string")
    protected CurrentPowerModeDTO currentPowerMode;

    /**
     * Gets the value of the currentPowerMode property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentPowerModeDTO }
     *     
     */
    public CurrentPowerModeDTO getCurrentPowerMode() {
        return currentPowerMode;
    }

    /**
     * Sets the value of the currentPowerMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentPowerModeDTO }
     *     
     */
    public void setCurrentPowerMode(CurrentPowerModeDTO value) {
        this.currentPowerMode = value;
    }

}
