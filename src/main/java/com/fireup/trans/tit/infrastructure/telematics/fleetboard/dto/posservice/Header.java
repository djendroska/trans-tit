
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Messagetype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Sentstatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Inbound" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientTimestamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServerTimestamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VehicleTimestamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreationBeginTimestamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreationEndTimestamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}Position" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}ReadInfo" minOccurs="0"/>
 *         &lt;element name="Confirmation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Scheduled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VehicleID" type="{http://www.fleetboard.com/data}vehicleidType" minOccurs="0"/>
 *         &lt;element name="DriverID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeliveryPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="States" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="State" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="timestamp" type="{http://www.fleetboard.com/data}timestampType" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "messagetype",
    "sentstatus",
    "inbound",
    "clientTimestamp",
    "serverTimestamp",
    "vehicleTimestamp",
    "creationBeginTimestamp",
    "creationEndTimestamp",
    "position",
    "readInfo",
    "confirmation",
    "scheduled",
    "vehicleID",
    "driverID",
    "deliveryPriority",
    "states"
})
@XmlRootElement(name = "Header")
public class Header {

    @XmlElement(name = "Messagetype", required = true)
    protected String messagetype;
    @XmlElement(name = "Sentstatus")
    protected String sentstatus;
    @XmlElement(name = "Inbound")
    protected String inbound;
    @XmlElement(name = "ClientTimestamp")
    protected String clientTimestamp;
    @XmlElement(name = "ServerTimestamp")
    protected String serverTimestamp;
    @XmlElement(name = "VehicleTimestamp")
    protected String vehicleTimestamp;
    @XmlElement(name = "CreationBeginTimestamp")
    protected String creationBeginTimestamp;
    @XmlElement(name = "CreationEndTimestamp")
    protected String creationEndTimestamp;
    @XmlElement(name = "Position")
    protected Position position;
    @XmlElement(name = "ReadInfo")
    protected ReadInfo readInfo;
    @XmlElement(name = "Confirmation")
    protected String confirmation;
    @XmlElement(name = "Scheduled")
    protected String scheduled;
    @XmlElement(name = "VehicleID")
    protected Long vehicleID;
    @XmlElement(name = "DriverID")
    protected String driverID;
    @XmlElement(name = "DeliveryPriority")
    protected String deliveryPriority;
    @XmlElement(name = "States")
    protected Header.States states;

    /**
     * Gets the value of the messagetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessagetype() {
        return messagetype;
    }

    /**
     * Sets the value of the messagetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessagetype(String value) {
        this.messagetype = value;
    }

    /**
     * Gets the value of the sentstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSentstatus() {
        return sentstatus;
    }

    /**
     * Sets the value of the sentstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSentstatus(String value) {
        this.sentstatus = value;
    }

    /**
     * Gets the value of the inbound property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInbound() {
        return inbound;
    }

    /**
     * Sets the value of the inbound property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInbound(String value) {
        this.inbound = value;
    }

    /**
     * Gets the value of the clientTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientTimestamp() {
        return clientTimestamp;
    }

    /**
     * Sets the value of the clientTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientTimestamp(String value) {
        this.clientTimestamp = value;
    }

    /**
     * Gets the value of the serverTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerTimestamp() {
        return serverTimestamp;
    }

    /**
     * Sets the value of the serverTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerTimestamp(String value) {
        this.serverTimestamp = value;
    }

    /**
     * Gets the value of the vehicleTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleTimestamp() {
        return vehicleTimestamp;
    }

    /**
     * Sets the value of the vehicleTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleTimestamp(String value) {
        this.vehicleTimestamp = value;
    }

    /**
     * Gets the value of the creationBeginTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationBeginTimestamp() {
        return creationBeginTimestamp;
    }

    /**
     * Sets the value of the creationBeginTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationBeginTimestamp(String value) {
        this.creationBeginTimestamp = value;
    }

    /**
     * Gets the value of the creationEndTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationEndTimestamp() {
        return creationEndTimestamp;
    }

    /**
     * Sets the value of the creationEndTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationEndTimestamp(String value) {
        this.creationEndTimestamp = value;
    }

    /**
     * Gets the value of the position property.
     * 
     * @return
     *     possible object is
     *     {@link Position }
     *     
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Sets the value of the position property.
     * 
     * @param value
     *     allowed object is
     *     {@link Position }
     *     
     */
    public void setPosition(Position value) {
        this.position = value;
    }

    /**
     * Gets the value of the readInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ReadInfo }
     *     
     */
    public ReadInfo getReadInfo() {
        return readInfo;
    }

    /**
     * Sets the value of the readInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReadInfo }
     *     
     */
    public void setReadInfo(ReadInfo value) {
        this.readInfo = value;
    }

    /**
     * Gets the value of the confirmation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmation() {
        return confirmation;
    }

    /**
     * Sets the value of the confirmation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmation(String value) {
        this.confirmation = value;
    }

    /**
     * Gets the value of the scheduled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduled() {
        return scheduled;
    }

    /**
     * Sets the value of the scheduled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduled(String value) {
        this.scheduled = value;
    }

    /**
     * Gets the value of the vehicleID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVehicleID() {
        return vehicleID;
    }

    /**
     * Sets the value of the vehicleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVehicleID(Long value) {
        this.vehicleID = value;
    }

    /**
     * Gets the value of the driverID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverID() {
        return driverID;
    }

    /**
     * Sets the value of the driverID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverID(String value) {
        this.driverID = value;
    }

    /**
     * Gets the value of the deliveryPriority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryPriority() {
        return deliveryPriority;
    }

    /**
     * Sets the value of the deliveryPriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryPriority(String value) {
        this.deliveryPriority = value;
    }

    /**
     * Gets the value of the states property.
     * 
     * @return
     *     possible object is
     *     {@link Header.States }
     *     
     */
    public Header.States getStates() {
        return states;
    }

    /**
     * Sets the value of the states property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header.States }
     *     
     */
    public void setStates(Header.States value) {
        this.states = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="State" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="timestamp" type="{http://www.fleetboard.com/data}timestampType" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "state"
    })
    public static class States {

        @XmlElement(name = "State")
        protected List<Header.States.State> state;

        /**
         * Gets the value of the state property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the state property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getState().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Header.States.State }
         * 
         * 
         */
        public List<Header.States.State> getState() {
            if (state == null) {
                state = new ArrayList<Header.States.State>();
            }
            return this.state;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="timestamp" type="{http://www.fleetboard.com/data}timestampType" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class State {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "name", required = true)
            protected String name;
            @XmlAttribute(name = "timestamp")
            protected String timestamp;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the timestamp property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTimestamp() {
                return timestamp;
            }

            /**
             * Sets the value of the timestamp property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTimestamp(String value) {
                this.timestamp = value;
            }

        }

    }

}
