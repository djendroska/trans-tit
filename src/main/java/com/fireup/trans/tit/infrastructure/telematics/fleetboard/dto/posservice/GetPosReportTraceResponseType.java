
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPosReportTraceResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPosReportTraceResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Position" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VehicleId" type="{http://www.fleetboard.com/data}vehicleidType"/>
 *                   &lt;element name="FleetId" type="{http://www.fleetboard.com/data}fleetidType"/>
 *                   &lt;element name="GpsTime" type="{http://www.fleetboard.com/data}timestampType"/>
 *                   &lt;element name="Long" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Lat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="VehicleTimestamp" type="{http://www.fleetboard.com/data}timestampType" minOccurs="0"/>
 *                   &lt;element name="ServerTimestamp" type="{http://www.fleetboard.com/data}timestampType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="limit" type="{http://www.fleetboard.com/data}limitType" />
 *       &lt;attribute name="offset" type="{http://www.fleetboard.com/data}offsetType" />
 *       &lt;attribute name="resultSize" type="{http://www.fleetboard.com/data}resultSizeType" />
 *       &lt;attribute name="responseSize" type="{http://www.fleetboard.com/data}responseSizeType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPosReportTraceResponseType", propOrder = {
    "position"
})
public class GetPosReportTraceResponseType {

    @XmlElement(name = "Position")
    protected List<GetPosReportTraceResponseType.Position> position;
    @XmlAttribute(name = "limit")
    protected BigInteger limit;
    @XmlAttribute(name = "offset")
    protected BigInteger offset;
    @XmlAttribute(name = "resultSize")
    protected BigInteger resultSize;
    @XmlAttribute(name = "responseSize")
    protected BigInteger responseSize;

    /**
     * Gets the value of the position property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the position property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPosition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetPosReportTraceResponseType.Position }
     * 
     * 
     */
    public List<GetPosReportTraceResponseType.Position> getPosition() {
        if (position == null) {
            position = new ArrayList<GetPosReportTraceResponseType.Position>();
        }
        return this.position;
    }

    /**
     * Gets the value of the limit property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLimit() {
        return limit;
    }

    /**
     * Sets the value of the limit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLimit(BigInteger value) {
        this.limit = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOffset(BigInteger value) {
        this.offset = value;
    }

    /**
     * Gets the value of the resultSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getResultSize() {
        return resultSize;
    }

    /**
     * Sets the value of the resultSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setResultSize(BigInteger value) {
        this.resultSize = value;
    }

    /**
     * Gets the value of the responseSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getResponseSize() {
        return responseSize;
    }

    /**
     * Sets the value of the responseSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setResponseSize(BigInteger value) {
        this.responseSize = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VehicleId" type="{http://www.fleetboard.com/data}vehicleidType"/>
     *         &lt;element name="FleetId" type="{http://www.fleetboard.com/data}fleetidType"/>
     *         &lt;element name="GpsTime" type="{http://www.fleetboard.com/data}timestampType"/>
     *         &lt;element name="Long" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Lat" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="VehicleTimestamp" type="{http://www.fleetboard.com/data}timestampType" minOccurs="0"/>
     *         &lt;element name="ServerTimestamp" type="{http://www.fleetboard.com/data}timestampType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehicleId",
        "fleetId",
        "gpsTime",
        "_long",
        "lat",
        "vehicleTimestamp",
        "serverTimestamp"
    })
    public static class Position {

        @XmlElement(name = "VehicleId")
        protected long vehicleId;
        @XmlElement(name = "FleetId")
        protected long fleetId;
        @XmlElement(name = "GpsTime", required = true)
        protected String gpsTime;
        @XmlElement(name = "Long", required = true)
        protected String _long;
        @XmlElement(name = "Lat", required = true)
        protected String lat;
        @XmlElement(name = "VehicleTimestamp")
        protected String vehicleTimestamp;
        @XmlElement(name = "ServerTimestamp")
        protected String serverTimestamp;

        /**
         * Gets the value of the vehicleId property.
         * 
         */
        public long getVehicleId() {
            return vehicleId;
        }

        /**
         * Sets the value of the vehicleId property.
         * 
         */
        public void setVehicleId(long value) {
            this.vehicleId = value;
        }

        /**
         * Gets the value of the fleetId property.
         * 
         */
        public long getFleetId() {
            return fleetId;
        }

        /**
         * Sets the value of the fleetId property.
         * 
         */
        public void setFleetId(long value) {
            this.fleetId = value;
        }

        /**
         * Gets the value of the gpsTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGpsTime() {
            return gpsTime;
        }

        /**
         * Sets the value of the gpsTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGpsTime(String value) {
            this.gpsTime = value;
        }

        /**
         * Gets the value of the long property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLong() {
            return _long;
        }

        /**
         * Sets the value of the long property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLong(String value) {
            this._long = value;
        }

        /**
         * Gets the value of the lat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLat() {
            return lat;
        }

        /**
         * Sets the value of the lat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLat(String value) {
            this.lat = value;
        }

        /**
         * Gets the value of the vehicleTimestamp property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleTimestamp() {
            return vehicleTimestamp;
        }

        /**
         * Sets the value of the vehicleTimestamp property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleTimestamp(String value) {
            this.vehicleTimestamp = value;
        }

        /**
         * Gets the value of the serverTimestamp property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServerTimestamp() {
            return serverTimestamp;
        }

        /**
         * Sets the value of the serverTimestamp property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServerTimestamp(String value) {
            this.serverTimestamp = value;
        }

    }

}
