
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FieldDefID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}VhcLayout"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}FormLayout"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}Behavior"/>
 *         &lt;element name="Searchable" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="SuppressInVehicle" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="CopyFrom" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}SemanticsDef" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fieldDefID",
    "name",
    "type",
    "vhcLayout",
    "formLayout",
    "behavior",
    "searchable",
    "suppressInVehicle",
    "copyFrom",
    "semanticsDef"
})
@XmlRootElement(name = "FieldDef")
public class FieldDef {

    @XmlElement(name = "FieldDefID", required = true)
    protected BigInteger fieldDefID;
    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "Type", required = true)
    protected String type;
    @XmlElement(name = "VhcLayout", required = true, nillable = true)
    protected VhcLayout vhcLayout;
    @XmlElement(name = "FormLayout", required = true)
    protected FormLayout formLayout;
    @XmlElement(name = "Behavior", required = true)
    protected Behavior behavior;
    @XmlElement(name = "Searchable")
    protected BigInteger searchable;
    @XmlElement(name = "SuppressInVehicle")
    protected BigInteger suppressInVehicle;
    @XmlElement(name = "CopyFrom")
    protected BigInteger copyFrom;
    @XmlElement(name = "SemanticsDef")
    protected SemanticsDef semanticsDef;

    /**
     * Gets the value of the fieldDefID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFieldDefID() {
        return fieldDefID;
    }

    /**
     * Sets the value of the fieldDefID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFieldDefID(BigInteger value) {
        this.fieldDefID = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the vhcLayout property.
     * 
     * @return
     *     possible object is
     *     {@link VhcLayout }
     *     
     */
    public VhcLayout getVhcLayout() {
        return vhcLayout;
    }

    /**
     * Sets the value of the vhcLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link VhcLayout }
     *     
     */
    public void setVhcLayout(VhcLayout value) {
        this.vhcLayout = value;
    }

    /**
     * Gets the value of the formLayout property.
     * 
     * @return
     *     possible object is
     *     {@link FormLayout }
     *     
     */
    public FormLayout getFormLayout() {
        return formLayout;
    }

    /**
     * Sets the value of the formLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormLayout }
     *     
     */
    public void setFormLayout(FormLayout value) {
        this.formLayout = value;
    }

    /**
     * Gets the value of the behavior property.
     * 
     * @return
     *     possible object is
     *     {@link Behavior }
     *     
     */
    public Behavior getBehavior() {
        return behavior;
    }

    /**
     * Sets the value of the behavior property.
     * 
     * @param value
     *     allowed object is
     *     {@link Behavior }
     *     
     */
    public void setBehavior(Behavior value) {
        this.behavior = value;
    }

    /**
     * Gets the value of the searchable property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSearchable() {
        return searchable;
    }

    /**
     * Sets the value of the searchable property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSearchable(BigInteger value) {
        this.searchable = value;
    }

    /**
     * Gets the value of the suppressInVehicle property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSuppressInVehicle() {
        return suppressInVehicle;
    }

    /**
     * Sets the value of the suppressInVehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSuppressInVehicle(BigInteger value) {
        this.suppressInVehicle = value;
    }

    /**
     * Gets the value of the copyFrom property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCopyFrom() {
        return copyFrom;
    }

    /**
     * Sets the value of the copyFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCopyFrom(BigInteger value) {
        this.copyFrom = value;
    }

    /**
     * Gets the value of the semanticsDef property.
     * 
     * @return
     *     possible object is
     *     {@link SemanticsDef }
     *     
     */
    public SemanticsDef getSemanticsDef() {
        return semanticsDef;
    }

    /**
     * Sets the value of the semanticsDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link SemanticsDef }
     *     
     */
    public void setSemanticsDef(SemanticsDef value) {
        this.semanticsDef = value;
    }

}
