
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TrailerCoupled.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TrailerCoupled">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Coupled"/>
 *     &lt;enumeration value="Uncoupled"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TrailerCoupled")
@XmlEnum
public enum TrailerCoupled {

    @XmlEnumValue("Coupled")
    COUPLED("Coupled"),
    @XmlEnumValue("Uncoupled")
    UNCOUPLED("Uncoupled"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    TrailerCoupled(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrailerCoupled fromValue(String v) {
        for (TrailerCoupled c: TrailerCoupled.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
