
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tmmodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tmmodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MASTERFLEETONLY"/>
 *     &lt;enumeration value="PARTNERFLEETONLY"/>
 *     &lt;enumeration value="MASTERFLEETISOWNER"/>
 *     &lt;enumeration value="PARTNERFLEETISOWNER"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tmmodeType")
@XmlEnum
public enum TmmodeType {

    MASTERFLEETONLY,
    PARTNERFLEETONLY,
    MASTERFLEETISOWNER,
    PARTNERFLEETISOWNER;

    public String value() {
        return name();
    }

    public static TmmodeType fromValue(String v) {
        return valueOf(v);
    }

}
