
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetUserResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserResponse"
})
@XmlRootElement(name = "getUserResponse")
public class GetUserResponse {

    @XmlElement(name = "GetUserResponse", required = true)
    protected GetUserResponseType getUserResponse;

    /**
     * Gets the value of the getUserResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetUserResponseType }
     *     
     */
    public GetUserResponseType getGetUserResponse() {
        return getUserResponse;
    }

    /**
     * Sets the value of the getUserResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetUserResponseType }
     *     
     */
    public void setGetUserResponse(GetUserResponseType value) {
        this.getUserResponse = value;
    }

}
