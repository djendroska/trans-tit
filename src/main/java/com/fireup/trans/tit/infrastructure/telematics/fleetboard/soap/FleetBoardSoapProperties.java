package com.fireup.trans.tit.infrastructure.telematics.fleetboard.soap;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "fleetBoard.soap")
@Data
public class FleetBoardSoapProperties {
    private int connectionTimeoutInMiliSec;
    private int readTimeoutInMiliSec;
    private String basicServiceUrl;
    private String posServiceUrl;
}