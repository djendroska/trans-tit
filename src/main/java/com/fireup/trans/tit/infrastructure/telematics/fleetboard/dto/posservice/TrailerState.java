
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TrailerState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TrailerState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACTIVE"/>
 *     &lt;enumeration value="INACTIVE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TrailerState")
@XmlEnum
public enum TrailerState {

    ACTIVE,
    INACTIVE;

    public String value() {
        return name();
    }

    public static TrailerState fromValue(String v) {
        return valueOf(v);
    }

}
