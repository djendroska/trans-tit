
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TemperatureSensorDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TemperatureSensorDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TemperaturSensor1"/>
 *     &lt;enumeration value="TemperaturSensor2"/>
 *     &lt;enumeration value="TemperaturSensor3"/>
 *     &lt;enumeration value="TemperaturSensor4"/>
 *     &lt;enumeration value="TemperaturSensor5"/>
 *     &lt;enumeration value="TemperaturSensor6"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TemperatureSensorDTO")
@XmlEnum
public enum TemperatureSensorDTO {

    @XmlEnumValue("TemperaturSensor1")
    TEMPERATUR_SENSOR_1("TemperaturSensor1"),
    @XmlEnumValue("TemperaturSensor2")
    TEMPERATUR_SENSOR_2("TemperaturSensor2"),
    @XmlEnumValue("TemperaturSensor3")
    TEMPERATUR_SENSOR_3("TemperaturSensor3"),
    @XmlEnumValue("TemperaturSensor4")
    TEMPERATUR_SENSOR_4("TemperaturSensor4"),
    @XmlEnumValue("TemperaturSensor5")
    TEMPERATUR_SENSOR_5("TemperaturSensor5"),
    @XmlEnumValue("TemperaturSensor6")
    TEMPERATUR_SENSOR_6("TemperaturSensor6");
    private final String value;

    TemperatureSensorDTO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TemperatureSensorDTO fromValue(String v) {
        for (TemperatureSensorDTO c: TemperatureSensorDTO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
