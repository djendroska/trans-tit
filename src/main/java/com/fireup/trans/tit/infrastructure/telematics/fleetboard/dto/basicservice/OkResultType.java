
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for okResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="okResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OKResult" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="vehicleID" use="required" type="{http://www.fleetboard.com/data}vehicleidType" />
 *                 &lt;attribute name="inoid" use="required" type="{http://www.fleetboard.com/data}inoidType" />
 *                 &lt;attribute name="fbid" use="required" type="{http://www.fleetboard.com/data}fbidType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "okResultType", propOrder = {
    "okResult"
})
public class OkResultType {

    @XmlElement(name = "OKResult", required = true)
    protected List<OkResultType.OKResult> okResult;

    /**
     * Gets the value of the okResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the okResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOKResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OkResultType.OKResult }
     * 
     * 
     */
    public List<OkResultType.OKResult> getOKResult() {
        if (okResult == null) {
            okResult = new ArrayList<OkResultType.OKResult>();
        }
        return this.okResult;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="vehicleID" use="required" type="{http://www.fleetboard.com/data}vehicleidType" />
     *       &lt;attribute name="inoid" use="required" type="{http://www.fleetboard.com/data}inoidType" />
     *       &lt;attribute name="fbid" use="required" type="{http://www.fleetboard.com/data}fbidType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class OKResult {

        @XmlAttribute(name = "vehicleID", required = true)
        protected long vehicleID;
        @XmlAttribute(name = "inoid", required = true)
        protected String inoid;
        @XmlAttribute(name = "fbid", required = true)
        protected String fbid;

        /**
         * Gets the value of the vehicleID property.
         * 
         */
        public long getVehicleID() {
            return vehicleID;
        }

        /**
         * Sets the value of the vehicleID property.
         * 
         */
        public void setVehicleID(long value) {
            this.vehicleID = value;
        }

        /**
         * Gets the value of the inoid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInoid() {
            return inoid;
        }

        /**
         * Sets the value of the inoid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInoid(String value) {
            this.inoid = value;
        }

        /**
         * Gets the value of the fbid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFbid() {
            return fbid;
        }

        /**
         * Sets the value of the fbid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFbid(String value) {
            this.fbid = value;
        }

    }

}
