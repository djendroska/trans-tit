
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="watchboxName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="watchboxAlarmReason" type="{http://www.fleetboard.com/data}WatchboxAlarmReasonDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "watchboxName",
    "watchboxAlarmReason"
})
@XmlRootElement(name = "WatchboxAlarm")
public class WatchboxAlarm {

    protected String watchboxName;
    @XmlSchemaType(name = "string")
    protected WatchboxAlarmReasonDTO watchboxAlarmReason;

    /**
     * Gets the value of the watchboxName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWatchboxName() {
        return watchboxName;
    }

    /**
     * Sets the value of the watchboxName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWatchboxName(String value) {
        this.watchboxName = value;
    }

    /**
     * Gets the value of the watchboxAlarmReason property.
     * 
     * @return
     *     possible object is
     *     {@link WatchboxAlarmReasonDTO }
     *     
     */
    public WatchboxAlarmReasonDTO getWatchboxAlarmReason() {
        return watchboxAlarmReason;
    }

    /**
     * Sets the value of the watchboxAlarmReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link WatchboxAlarmReasonDTO }
     *     
     */
    public void setWatchboxAlarmReason(WatchboxAlarmReasonDTO value) {
        this.watchboxAlarmReason = value;
    }

}
