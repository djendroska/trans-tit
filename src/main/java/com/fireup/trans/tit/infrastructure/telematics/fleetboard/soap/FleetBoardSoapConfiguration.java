package com.fireup.trans.tit.infrastructure.telematics.fleetboard.soap;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

@Configuration
public class FleetBoardSoapConfiguration {

    @Bean
    public FleetBoardSoapProperties fleetBoardSoapProperties() {
        return new FleetBoardSoapProperties();
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice");
        return marshaller;
    }

    @Bean
    public Jaxb2Marshaller posMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice");
        return marshaller;
    }

    @Bean
    public HttpComponentsMessageSender httpComponentsMessageSender(FleetBoardSoapProperties fleetBoardSoapProperties) {
        HttpComponentsMessageSender sender = new HttpComponentsMessageSender();
        sender.setConnectionTimeout(fleetBoardSoapProperties.getConnectionTimeoutInMiliSec());
        sender.setReadTimeout(fleetBoardSoapProperties.getReadTimeoutInMiliSec());
        return sender;
    }

    @Bean
    public BasicServiceClient basicServiceClient(Jaxb2Marshaller marshaller, HttpComponentsMessageSender httpComponentsMessageSender) {
        BasicServiceClient client = new BasicServiceClient();
        client.setDefaultUri(fleetBoardSoapProperties().getBasicServiceUrl());
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        client.setMessageSender(httpComponentsMessageSender);
        return client;
    }

    @Bean
    public PosServiceClient posServiceClient(Jaxb2Marshaller posMarshaller, HttpComponentsMessageSender httpComponentsMessageSender) {
        PosServiceClient client = new PosServiceClient();
        client.setDefaultUri(fleetBoardSoapProperties().getPosServiceUrl());
        client.setMarshaller(posMarshaller);
        client.setUnmarshaller(posMarshaller);
        client.setMessageSender(httpComponentsMessageSender);
        return client;
    }
}
