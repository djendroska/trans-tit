
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetCurrentUserResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCurrentUserResponse"
})
@XmlRootElement(name = "getCurrentUserResponse")
public class GetCurrentUserResponse {

    @XmlElement(name = "GetCurrentUserResponse", required = true)
    protected GetCurrentUserResponseType getCurrentUserResponse;

    /**
     * Gets the value of the getCurrentUserResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetCurrentUserResponseType }
     *     
     */
    public GetCurrentUserResponseType getGetCurrentUserResponse() {
        return getCurrentUserResponse;
    }

    /**
     * Sets the value of the getCurrentUserResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCurrentUserResponseType }
     *     
     */
    public void setGetCurrentUserResponse(GetCurrentUserResponseType value) {
        this.getCurrentUserResponse = value;
    }

}
