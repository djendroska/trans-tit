
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}AutoPos"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "autoPos"
})
@XmlRootElement(name = "AutoPosDef")
public class AutoPosDef {

    @XmlElement(name = "AutoPos", required = true, nillable = true)
    protected AutoPos autoPos;

    /**
     * Gets the value of the autoPos property.
     * 
     * @return
     *     possible object is
     *     {@link AutoPos }
     *     
     */
    public AutoPos getAutoPos() {
        return autoPos;
    }

    /**
     * Sets the value of the autoPos property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoPos }
     *     
     */
    public void setAutoPos(AutoPos value) {
        this.autoPos = value;
    }

}
