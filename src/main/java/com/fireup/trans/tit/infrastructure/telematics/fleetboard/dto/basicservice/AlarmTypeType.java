
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for alarmTypeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="alarmTypeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SERVICE-CALL"/>
 *     &lt;enumeration value="HOME-CALL"/>
 *     &lt;enumeration value="EVENT-CALL"/>
 *     &lt;enumeration value="EDW-ALARM"/>
 *     &lt;enumeration value="ECU-ALARM"/>
 *     &lt;enumeration value="DISPOPILOT-OVERFLOW"/>
 *     &lt;enumeration value="AREA-ENTRY"/>
 *     &lt;enumeration value="AREA-EXIT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "alarmTypeType")
@XmlEnum
public enum AlarmTypeType {

    @XmlEnumValue("SERVICE-CALL")
    SERVICE_CALL("SERVICE-CALL"),
    @XmlEnumValue("HOME-CALL")
    HOME_CALL("HOME-CALL"),
    @XmlEnumValue("EVENT-CALL")
    EVENT_CALL("EVENT-CALL"),
    @XmlEnumValue("EDW-ALARM")
    EDW_ALARM("EDW-ALARM"),
    @XmlEnumValue("ECU-ALARM")
    ECU_ALARM("ECU-ALARM"),
    @XmlEnumValue("DISPOPILOT-OVERFLOW")
    DISPOPILOT_OVERFLOW("DISPOPILOT-OVERFLOW"),
    @XmlEnumValue("AREA-ENTRY")
    AREA_ENTRY("AREA-ENTRY"),
    @XmlEnumValue("AREA-EXIT")
    AREA_EXIT("AREA-EXIT");
    private final String value;

    AlarmTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AlarmTypeType fromValue(String v) {
        for (AlarmTypeType c: AlarmTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
