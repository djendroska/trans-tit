
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Device2VehicleConnectionStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Device2VehicleConnectionStatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FREE"/>
 *     &lt;enumeration value="REQUESTED"/>
 *     &lt;enumeration value="CONNECTED"/>
 *     &lt;enumeration value="REJECTED"/>
 *     &lt;enumeration value="UNAVAILABLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Device2VehicleConnectionStatusType")
@XmlEnum
public enum Device2VehicleConnectionStatusType {

    FREE,
    REQUESTED,
    CONNECTED,
    REJECTED,
    UNAVAILABLE;

    public String value() {
        return name();
    }

    public static Device2VehicleConnectionStatusType fromValue(String v) {
        return valueOf(v);
    }

}
