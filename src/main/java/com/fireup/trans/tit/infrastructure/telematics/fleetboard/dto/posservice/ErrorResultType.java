
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for errorResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="errorResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ErrorResult" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="vehicleID" use="required" type="{http://www.fleetboard.com/data}vehicleidType" />
 *                 &lt;attribute name="faultcode" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                 &lt;attribute name="faultstring" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "errorResultType", propOrder = {
    "errorResult"
})
public class ErrorResultType {

    @XmlElement(name = "ErrorResult", required = true)
    protected List<ErrorResultType.ErrorResult> errorResult;

    /**
     * Gets the value of the errorResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorResultType.ErrorResult }
     * 
     * 
     */
    public List<ErrorResultType.ErrorResult> getErrorResult() {
        if (errorResult == null) {
            errorResult = new ArrayList<ErrorResultType.ErrorResult>();
        }
        return this.errorResult;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="vehicleID" use="required" type="{http://www.fleetboard.com/data}vehicleidType" />
     *       &lt;attribute name="faultcode" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *       &lt;attribute name="faultstring" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ErrorResult {

        @XmlAttribute(name = "vehicleID", required = true)
        protected long vehicleID;
        @XmlAttribute(name = "faultcode", required = true)
        protected BigInteger faultcode;
        @XmlAttribute(name = "faultstring", required = true)
        protected String faultstring;

        /**
         * Gets the value of the vehicleID property.
         * 
         */
        public long getVehicleID() {
            return vehicleID;
        }

        /**
         * Sets the value of the vehicleID property.
         * 
         */
        public void setVehicleID(long value) {
            this.vehicleID = value;
        }

        /**
         * Gets the value of the faultcode property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getFaultcode() {
            return faultcode;
        }

        /**
         * Sets the value of the faultcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setFaultcode(BigInteger value) {
            this.faultcode = value;
        }

        /**
         * Gets the value of the faultstring property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFaultstring() {
            return faultstring;
        }

        /**
         * Sets the value of the faultstring property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFaultstring(String value) {
            this.faultstring = value;
        }

    }

}
