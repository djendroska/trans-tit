
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetCurrentStateRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCurrentStateRequest"
})
@XmlRootElement(name = "getCurrentState")
public class GetCurrentState {

    @XmlElement(name = "GetCurrentStateRequest", required = true)
    protected GetCurrentStateRequestType getCurrentStateRequest;

    /**
     * Gets the value of the getCurrentStateRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetCurrentStateRequestType }
     *     
     */
    public GetCurrentStateRequestType getGetCurrentStateRequest() {
        return getCurrentStateRequest;
    }

    /**
     * Sets the value of the getCurrentStateRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCurrentStateRequestType }
     *     
     */
    public void setGetCurrentStateRequest(GetCurrentStateRequestType value) {
        this.getCurrentStateRequest = value;
    }

}
