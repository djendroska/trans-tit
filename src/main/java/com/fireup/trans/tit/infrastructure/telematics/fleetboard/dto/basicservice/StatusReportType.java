
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for statusReportType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="statusReportType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="VEHICLE"/>
 *     &lt;enumeration value="TOUR"/>
 *     &lt;enumeration value="ORDER"/>
 *     &lt;enumeration value="EVENT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "statusReportType")
@XmlEnum
public enum StatusReportType {

    VEHICLE,
    TOUR,
    ORDER,
    EVENT;

    public String value() {
        return name();
    }

    public static StatusReportType fromValue(String v) {
        return valueOf(v);
    }

}
