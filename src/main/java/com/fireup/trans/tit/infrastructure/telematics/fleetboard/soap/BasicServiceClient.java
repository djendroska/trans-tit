package com.fireup.trans.tit.infrastructure.telematics.fleetboard.soap;

import java.util.Collections;
import java.util.List;

import org.apache.http.auth.InvalidCredentialsException;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice.*;

public class BasicServiceClient extends WebServiceGatewaySupport {

    public LoginResponse2 authenticate(String username, String fleetName, String password)
            throws InvalidCredentialsException {
        return authenticate(username, fleetName, password, false);
    }

    private LoginResponse2 authenticate(String username, String fleetName, String password, boolean isRetry)
            throws InvalidCredentialsException {

        final LoginRequest request = new LoginRequest();
        request.setFleetname(fleetName);
        request.setUser(username);
        request.setPassword(password);

        final Login login = new Login();
        login.setLoginRequest(request);
        try {
            return (LoginResponse2) getWebServiceTemplate().marshalSendAndReceive(login, new SoapActionCallback(
                    FleetBoardSoapCallbacks.LOGIN));
        } catch (Exception e) {
            if (!isRetry) {
                try {
                    Thread.sleep((long) 1000);
                } catch (InterruptedException ie) {
                    throw new InvalidCredentialsException(e.getMessage());
                }
                return authenticate(username, fleetName, password, true);
            } else {
                throw new InvalidCredentialsException(e.getMessage());
            }
        }
    }

    public List<VEHICLE> getVehicles(String username, String fleetName, String password)
            throws InvalidCredentialsException {
        LoginResponse2 session = this.authenticate(username, fleetName, password);
        List<VEHICLE> vehicleList = this.getVehicles(session);
        this.logout(session);

        return vehicleList;
    }

    public void logout(final LoginResponse2 authentication) {
        final LogoutRequest logoutRequest = new LogoutRequest();

        final Logout logout = new Logout();
        logout.setLogoutRequest(logoutRequest);

        getWebServiceTemplate().marshalSendAndReceive(
                logout,
                new SoapActionCallback(FleetBoardSoapCallbacks.LOGOUT + FleetBoardSoapCallbacks.JSESSION_ID + authentication.getLoginResponse().getSessionid())
        );
    }

    private List<VEHICLE> getVehicles(LoginResponse2 session) {
        final GetVehicleRequest vehicleRequest = new GetVehicleRequest();
        final GetVehicle getVehicle = new GetVehicle();
        getVehicle.setGetVehicleRequest(vehicleRequest);

        final GetVehicleResponse2 response = (GetVehicleResponse2) getWebServiceTemplate().marshalSendAndReceive(
                getVehicle,
                new SoapActionCallback(FleetBoardSoapCallbacks.GET_VEHICLE + FleetBoardSoapCallbacks.JSESSION_ID + session.getLoginResponse().getSessionid())
        );

        List<VEHICLE> vehicles = response.getGetVehicleResponse().getVEHICLE();
        if (vehicles == null) {
            return Collections.EMPTY_LIST;
        }
        return vehicles;
    }
}
