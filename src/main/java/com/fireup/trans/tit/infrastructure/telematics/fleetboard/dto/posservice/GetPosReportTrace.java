
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetPosReportTraceRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPosReportTraceRequest"
})
@XmlRootElement(name = "getPosReportTrace")
public class GetPosReportTrace {

    @XmlElement(name = "GetPosReportTraceRequest", required = true)
    protected GetPosReportTraceRequestType getPosReportTraceRequest;

    /**
     * Gets the value of the getPosReportTraceRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetPosReportTraceRequestType }
     *     
     */
    public GetPosReportTraceRequestType getGetPosReportTraceRequest() {
        return getPosReportTraceRequest;
    }

    /**
     * Sets the value of the getPosReportTraceRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPosReportTraceRequestType }
     *     
     */
    public void setGetPosReportTraceRequest(GetPosReportTraceRequestType value) {
        this.getPosReportTraceRequest = value;
    }

}
