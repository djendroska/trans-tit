
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetDriverGroupRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDriverGroupRequest"
})
@XmlRootElement(name = "getDriverGroup")
public class GetDriverGroup {

    @XmlElement(name = "GetDriverGroupRequest", required = true)
    protected GetDriverGroupRequest getDriverGroupRequest;

    /**
     * Gets the value of the getDriverGroupRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetDriverGroupRequest }
     *     
     */
    public GetDriverGroupRequest getGetDriverGroupRequest() {
        return getDriverGroupRequest;
    }

    /**
     * Sets the value of the getDriverGroupRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDriverGroupRequest }
     *     
     */
    public void setGetDriverGroupRequest(GetDriverGroupRequest value) {
        this.getDriverGroupRequest = value;
    }

}
