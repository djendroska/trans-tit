
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TourEventType" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="TourEventStatus" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tourEventType",
    "tourEventStatus"
})
@XmlRootElement(name = "TourEventUptimeMsg")
public class TourEventUptimeMsg {

    @XmlElement(name = "TourEventType")
    protected short tourEventType;
    @XmlElement(name = "TourEventStatus")
    protected short tourEventStatus;

    /**
     * Gets the value of the tourEventType property.
     * 
     */
    public short getTourEventType() {
        return tourEventType;
    }

    /**
     * Sets the value of the tourEventType property.
     * 
     */
    public void setTourEventType(short value) {
        this.tourEventType = value;
    }

    /**
     * Gets the value of the tourEventStatus property.
     * 
     */
    public short getTourEventStatus() {
        return tourEventStatus;
    }

    /**
     * Sets the value of the tourEventStatus property.
     * 
     */
    public void setTourEventStatus(short value) {
        this.tourEventStatus = value;
    }

}
