
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetCurrentStateResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCurrentStateResponse"
})
@XmlRootElement(name = "getCurrentStateResponse")
public class GetCurrentStateResponse {

    @XmlElement(name = "GetCurrentStateResponse", required = true)
    protected GetCurrentStateResponseType getCurrentStateResponse;

    /**
     * Gets the value of the getCurrentStateResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetCurrentStateResponseType }
     *     
     */
    public GetCurrentStateResponseType getGetCurrentStateResponse() {
        return getCurrentStateResponse;
    }

    /**
     * Sets the value of the getCurrentStateResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCurrentStateResponseType }
     *     
     */
    public void setGetCurrentStateResponse(GetCurrentStateResponseType value) {
        this.getCurrentStateResponse = value;
    }

}
