
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetUserRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserRequest"
})
@XmlRootElement(name = "getUser")
public class GetUser {

    @XmlElement(name = "GetUserRequest", required = true)
    protected GetUserRequest getUserRequest;

    /**
     * Gets the value of the getUserRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetUserRequest }
     *     
     */
    public GetUserRequest getGetUserRequest() {
        return getUserRequest;
    }

    /**
     * Sets the value of the getUserRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetUserRequest }
     *     
     */
    public void setGetUserRequest(GetUserRequest value) {
        this.getUserRequest = value;
    }

}
