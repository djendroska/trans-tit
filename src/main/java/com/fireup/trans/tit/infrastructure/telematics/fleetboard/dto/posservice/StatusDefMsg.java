
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ATSRegister" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StatusDefID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}VhcStatusLayout" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}SemanticsDef" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "atsRegister",
    "text",
    "statusDefID",
    "vhcStatusLayout",
    "semanticsDef"
})
@XmlRootElement(name = "StatusDefMsg")
public class StatusDefMsg {

    @XmlElement(name = "ATSRegister", required = true)
    protected String atsRegister;
    @XmlElement(name = "Text", required = true)
    protected String text;
    @XmlElement(name = "StatusDefID")
    protected String statusDefID;
    @XmlElement(name = "VhcStatusLayout", nillable = true)
    protected VhcStatusLayout vhcStatusLayout;
    @XmlElement(name = "SemanticsDef")
    protected SemanticsDef semanticsDef;

    /**
     * Gets the value of the atsRegister property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getATSRegister() {
        return atsRegister;
    }

    /**
     * Sets the value of the atsRegister property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setATSRegister(String value) {
        this.atsRegister = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the statusDefID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusDefID() {
        return statusDefID;
    }

    /**
     * Sets the value of the statusDefID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusDefID(String value) {
        this.statusDefID = value;
    }

    /**
     * Gets the value of the vhcStatusLayout property.
     * 
     * @return
     *     possible object is
     *     {@link VhcStatusLayout }
     *     
     */
    public VhcStatusLayout getVhcStatusLayout() {
        return vhcStatusLayout;
    }

    /**
     * Sets the value of the vhcStatusLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link VhcStatusLayout }
     *     
     */
    public void setVhcStatusLayout(VhcStatusLayout value) {
        this.vhcStatusLayout = value;
    }

    /**
     * Gets the value of the semanticsDef property.
     * 
     * @return
     *     possible object is
     *     {@link SemanticsDef }
     *     
     */
    public SemanticsDef getSemanticsDef() {
        return semanticsDef;
    }

    /**
     * Sets the value of the semanticsDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link SemanticsDef }
     *     
     */
    public void setSemanticsDef(SemanticsDef value) {
        this.semanticsDef = value;
    }

}
