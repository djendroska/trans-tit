
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TourID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TourRank" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="OrderCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element ref="{http://www.fleetboard.com/data}Form"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderID",
    "tourID",
    "tourRank",
    "orderCount",
    "form"
})
@XmlRootElement(name = "OrderMsg")
public class OrderMsg {

    @XmlElement(name = "OrderID", required = true)
    protected String orderID;
    @XmlElement(name = "TourID", required = true)
    protected String tourID;
    @XmlElement(name = "TourRank")
    protected BigInteger tourRank;
    @XmlElement(name = "OrderCount")
    protected BigInteger orderCount;
    @XmlElement(name = "Form", required = true)
    protected Form form;

    /**
     * Gets the value of the orderID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderID() {
        return orderID;
    }

    /**
     * Sets the value of the orderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderID(String value) {
        this.orderID = value;
    }

    /**
     * Gets the value of the tourID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourID() {
        return tourID;
    }

    /**
     * Sets the value of the tourID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourID(String value) {
        this.tourID = value;
    }

    /**
     * Gets the value of the tourRank property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTourRank() {
        return tourRank;
    }

    /**
     * Sets the value of the tourRank property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTourRank(BigInteger value) {
        this.tourRank = value;
    }

    /**
     * Gets the value of the orderCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOrderCount() {
        return orderCount;
    }

    /**
     * Sets the value of the orderCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOrderCount(BigInteger value) {
        this.orderCount = value;
    }

    /**
     * Gets the value of the form property.
     * 
     * @return
     *     possible object is
     *     {@link Form }
     *     
     */
    public Form getForm() {
        return form;
    }

    /**
     * Sets the value of the form property.
     * 
     * @param value
     *     allowed object is
     *     {@link Form }
     *     
     */
    public void setForm(Form value) {
        this.form = value;
    }

}
