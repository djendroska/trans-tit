
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetLastPositionResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLastPositionResponse"
})
@XmlRootElement(name = "getLastPositionResponse")
public class GetLastPositionResponse {

    @XmlElement(name = "GetLastPositionResponse", required = true)
    protected GetLastPositionResponseType getLastPositionResponse;

    /**
     * Gets the value of the getLastPositionResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetLastPositionResponseType }
     *     
     */
    public GetLastPositionResponseType getGetLastPositionResponse() {
        return getLastPositionResponse;
    }

    /**
     * Sets the value of the getLastPositionResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetLastPositionResponseType }
     *     
     */
    public void setGetLastPositionResponse(GetLastPositionResponseType value) {
        this.getLastPositionResponse = value;
    }

}
