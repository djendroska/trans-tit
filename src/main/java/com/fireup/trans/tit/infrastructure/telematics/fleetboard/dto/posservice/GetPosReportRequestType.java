
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPosReportRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPosReportRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VehicleID" type="{http://www.fleetboard.com/data}vehicleidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="VehicleGroupID" type="{http://www.fleetboard.com/data}vehicleGroupIdType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TelematicGroupID" type="{http://www.fleetboard.com/data}telematicGroupIDType" minOccurs="0"/>
 *         &lt;element name="DriverNameID" type="{http://www.fleetboard.com/data}driverNameIDType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DriverGroupID" type="{http://www.fleetboard.com/data}driverGroupIdType" minOccurs="0"/>
 *         &lt;element name="VehicleTimestamp" type="{http://www.fleetboard.com/data}TPDate" minOccurs="0"/>
 *         &lt;element name="RealVehicleTimestamp" type="{http://www.fleetboard.com/data}TPDate" minOccurs="0"/>
 *         &lt;element name="Inoid" type="{http://www.fleetboard.com/data}inoidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Fbid" type="{http://www.fleetboard.com/data}fbidType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="sessionid" type="{http://www.fleetboard.com/data}sessionidType" />
 *       &lt;attribute name="limit" type="{http://www.fleetboard.com/data}limitType" />
 *       &lt;attribute name="offset" type="{http://www.fleetboard.com/data}offsetType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPosReportRequestType", propOrder = {
    "vehicleID",
    "vehicleGroupID",
    "telematicGroupID",
    "driverNameID",
    "driverGroupID",
    "vehicleTimestamp",
    "realVehicleTimestamp",
    "inoid",
    "fbid"
})
public class GetPosReportRequestType {

    @XmlElement(name = "VehicleID", type = Long.class)
    protected List<Long> vehicleID;
    @XmlElement(name = "VehicleGroupID", type = Long.class)
    protected List<Long> vehicleGroupID;
    @XmlElement(name = "TelematicGroupID")
    @XmlSchemaType(name = "string")
    protected TelematicGroupIDType telematicGroupID;
    @XmlElement(name = "DriverNameID", type = Long.class)
    protected List<Long> driverNameID;
    @XmlElement(name = "DriverGroupID")
    protected Long driverGroupID;
    @XmlElement(name = "VehicleTimestamp")
    protected TPDate vehicleTimestamp;
    @XmlElement(name = "RealVehicleTimestamp")
    protected TPDate realVehicleTimestamp;
    @XmlElement(name = "Inoid")
    protected List<String> inoid;
    @XmlElement(name = "Fbid")
    protected List<String> fbid;
    @XmlAttribute(name = "sessionid")
    protected String sessionid;
    @XmlAttribute(name = "limit")
    protected BigInteger limit;
    @XmlAttribute(name = "offset")
    protected BigInteger offset;

    /**
     * Gets the value of the vehicleID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getVehicleID() {
        if (vehicleID == null) {
            vehicleID = new ArrayList<Long>();
        }
        return this.vehicleID;
    }

    /**
     * Gets the value of the vehicleGroupID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleGroupID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleGroupID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getVehicleGroupID() {
        if (vehicleGroupID == null) {
            vehicleGroupID = new ArrayList<Long>();
        }
        return this.vehicleGroupID;
    }

    /**
     * Gets the value of the telematicGroupID property.
     * 
     * @return
     *     possible object is
     *     {@link TelematicGroupIDType }
     *     
     */
    public TelematicGroupIDType getTelematicGroupID() {
        return telematicGroupID;
    }

    /**
     * Sets the value of the telematicGroupID property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelematicGroupIDType }
     *     
     */
    public void setTelematicGroupID(TelematicGroupIDType value) {
        this.telematicGroupID = value;
    }

    /**
     * Gets the value of the driverNameID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the driverNameID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDriverNameID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getDriverNameID() {
        if (driverNameID == null) {
            driverNameID = new ArrayList<Long>();
        }
        return this.driverNameID;
    }

    /**
     * Gets the value of the driverGroupID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDriverGroupID() {
        return driverGroupID;
    }

    /**
     * Sets the value of the driverGroupID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDriverGroupID(Long value) {
        this.driverGroupID = value;
    }

    /**
     * Gets the value of the vehicleTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link TPDate }
     *     
     */
    public TPDate getVehicleTimestamp() {
        return vehicleTimestamp;
    }

    /**
     * Sets the value of the vehicleTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPDate }
     *     
     */
    public void setVehicleTimestamp(TPDate value) {
        this.vehicleTimestamp = value;
    }

    /**
     * Gets the value of the realVehicleTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link TPDate }
     *     
     */
    public TPDate getRealVehicleTimestamp() {
        return realVehicleTimestamp;
    }

    /**
     * Sets the value of the realVehicleTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPDate }
     *     
     */
    public void setRealVehicleTimestamp(TPDate value) {
        this.realVehicleTimestamp = value;
    }

    /**
     * Gets the value of the inoid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inoid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInoid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getInoid() {
        if (inoid == null) {
            inoid = new ArrayList<String>();
        }
        return this.inoid;
    }

    /**
     * Gets the value of the fbid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fbid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFbid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFbid() {
        if (fbid == null) {
            fbid = new ArrayList<String>();
        }
        return this.fbid;
    }

    /**
     * Gets the value of the sessionid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionid() {
        return sessionid;
    }

    /**
     * Sets the value of the sessionid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionid(String value) {
        this.sessionid = value;
    }

    /**
     * Gets the value of the limit property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLimit() {
        return limit;
    }

    /**
     * Sets the value of the limit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLimit(BigInteger value) {
        this.limit = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOffset(BigInteger value) {
        this.offset = value;
    }

}
