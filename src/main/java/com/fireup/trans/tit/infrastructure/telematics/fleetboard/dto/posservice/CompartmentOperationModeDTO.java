
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompartmentOperationModeDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CompartmentOperationModeDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="On"/>
 *     &lt;enumeration value="Off"/>
 *     &lt;enumeration value="Cooling"/>
 *     &lt;enumeration value="Heating"/>
 *     &lt;enumeration value="Defrost"/>
 *     &lt;enumeration value="Pretrip"/>
 *     &lt;enumeration value="Null"/>
 *     &lt;enumeration value="Invalid"/>
 *     &lt;enumeration value="Unknown"/>
 *     &lt;enumeration value="Diagnostic"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CompartmentOperationModeDTO")
@XmlEnum
public enum CompartmentOperationModeDTO {

    @XmlEnumValue("On")
    ON("On"),
    @XmlEnumValue("Off")
    OFF("Off"),
    @XmlEnumValue("Cooling")
    COOLING("Cooling"),
    @XmlEnumValue("Heating")
    HEATING("Heating"),
    @XmlEnumValue("Defrost")
    DEFROST("Defrost"),
    @XmlEnumValue("Pretrip")
    PRETRIP("Pretrip"),
    @XmlEnumValue("Null")
    NULL("Null"),
    @XmlEnumValue("Invalid")
    INVALID("Invalid"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    @XmlEnumValue("Diagnostic")
    DIAGNOSTIC("Diagnostic");
    private final String value;

    CompartmentOperationModeDTO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CompartmentOperationModeDTO fromValue(String v) {
        for (CompartmentOperationModeDTO c: CompartmentOperationModeDTO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
