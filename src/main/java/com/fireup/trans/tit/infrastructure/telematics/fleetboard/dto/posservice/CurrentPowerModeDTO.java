
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CurrentPowerModeDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CurrentPowerModeDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Electric"/>
 *     &lt;enumeration value="Diesel"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CurrentPowerModeDTO")
@XmlEnum
public enum CurrentPowerModeDTO {

    @XmlEnumValue("Electric")
    ELECTRIC("Electric"),
    @XmlEnumValue("Diesel")
    DIESEL("Diesel"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    CurrentPowerModeDTO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CurrentPowerModeDTO fromValue(String v) {
        for (CurrentPowerModeDTO c: CurrentPowerModeDTO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
