
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetDriverGroupResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDriverGroupResponse"
})
@XmlRootElement(name = "getDriverGroupResponse")
public class GetDriverGroupResponse {

    @XmlElement(name = "GetDriverGroupResponse", required = true)
    protected GetDriverGroupResponse2 getDriverGroupResponse;

    /**
     * Gets the value of the getDriverGroupResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetDriverGroupResponse2 }
     *     
     */
    public GetDriverGroupResponse2 getGetDriverGroupResponse() {
        return getDriverGroupResponse;
    }

    /**
     * Sets the value of the getDriverGroupResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDriverGroupResponse2 }
     *     
     */
    public void setGetDriverGroupResponse(GetDriverGroupResponse2 value) {
        this.getDriverGroupResponse = value;
    }

}
