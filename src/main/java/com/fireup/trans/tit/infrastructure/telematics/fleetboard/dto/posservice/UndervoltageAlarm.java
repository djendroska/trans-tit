
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="batteryConcerned" type="{http://www.fleetboard.com/data}BatteryConcernedDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "batteryConcerned"
})
@XmlRootElement(name = "UndervoltageAlarm")
public class UndervoltageAlarm {

    @XmlSchemaType(name = "string")
    protected BatteryConcernedDTO batteryConcerned;

    /**
     * Gets the value of the batteryConcerned property.
     * 
     * @return
     *     possible object is
     *     {@link BatteryConcernedDTO }
     *     
     */
    public BatteryConcernedDTO getBatteryConcerned() {
        return batteryConcerned;
    }

    /**
     * Sets the value of the batteryConcerned property.
     * 
     * @param value
     *     allowed object is
     *     {@link BatteryConcernedDTO }
     *     
     */
    public void setBatteryConcerned(BatteryConcernedDTO value) {
        this.batteryConcerned = value;
    }

}
