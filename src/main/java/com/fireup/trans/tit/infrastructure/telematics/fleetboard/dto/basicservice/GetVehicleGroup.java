
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetVehicleGroupRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getVehicleGroupRequest"
})
@XmlRootElement(name = "getVehicleGroup")
public class GetVehicleGroup {

    @XmlElement(name = "GetVehicleGroupRequest", required = true)
    protected GetVehicleGroupRequest getVehicleGroupRequest;

    /**
     * Gets the value of the getVehicleGroupRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetVehicleGroupRequest }
     *     
     */
    public GetVehicleGroupRequest getGetVehicleGroupRequest() {
        return getVehicleGroupRequest;
    }

    /**
     * Sets the value of the getVehicleGroupRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetVehicleGroupRequest }
     *     
     */
    public void setGetVehicleGroupRequest(GetVehicleGroupRequest value) {
        this.getVehicleGroupRequest = value;
    }

}
