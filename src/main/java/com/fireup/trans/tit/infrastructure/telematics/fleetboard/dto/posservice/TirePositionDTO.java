
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TirePositionDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TirePositionDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FrontLeftWheel"/>
 *     &lt;enumeration value="FrontRightWheel"/>
 *     &lt;enumeration value="MiddleLeftWheel"/>
 *     &lt;enumeration value="MiddleRightWheel"/>
 *     &lt;enumeration value="RearLeftWheel"/>
 *     &lt;enumeration value="RearRightWheel"/>
 *     &lt;enumeration value="SpareWheel"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TirePositionDTO")
@XmlEnum
public enum TirePositionDTO {

    @XmlEnumValue("FrontLeftWheel")
    FRONT_LEFT_WHEEL("FrontLeftWheel"),
    @XmlEnumValue("FrontRightWheel")
    FRONT_RIGHT_WHEEL("FrontRightWheel"),
    @XmlEnumValue("MiddleLeftWheel")
    MIDDLE_LEFT_WHEEL("MiddleLeftWheel"),
    @XmlEnumValue("MiddleRightWheel")
    MIDDLE_RIGHT_WHEEL("MiddleRightWheel"),
    @XmlEnumValue("RearLeftWheel")
    REAR_LEFT_WHEEL("RearLeftWheel"),
    @XmlEnumValue("RearRightWheel")
    REAR_RIGHT_WHEEL("RearRightWheel"),
    @XmlEnumValue("SpareWheel")
    SPARE_WHEEL("SpareWheel");
    private final String value;

    TirePositionDTO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TirePositionDTO fromValue(String v) {
        for (TirePositionDTO c: TirePositionDTO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
