
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fireup.trans.tit.infrastructure.telematics.fleetboard.soap.posservice package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interface
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPosReportRequest_QNAME = new QName("http://www.fleetboard.com/data", "GetPosReportRequest");
    private final static QName _GetPosReportTraceRequest_QNAME = new QName("http://www.fleetboard.com/data", "GetPosReportTraceRequest");
    private final static QName _FieldsDef_QNAME = new QName("http://www.fleetboard.com/data", "FieldsDef");
    private final static QName _GetLastPositionRequest_QNAME = new QName("http://www.fleetboard.com/data", "GetLastPositionRequest");
    private final static QName _GetReplyRequest_QNAME = new QName("http://www.fleetboard.com/data", "GetReplyRequest");
    private final static QName _VhcStatusLayout_QNAME = new QName("http://www.fleetboard.com/data", "VhcStatusLayout");
    private final static QName _GetCurrentStateRequest_QNAME = new QName("http://www.fleetboard.com/data", "GetCurrentStateRequest");
    private final static QName _GetPosReportResponse_QNAME = new QName("http://www.fleetboard.com/data", "GetPosReportResponse");
    private final static QName _GetReplyResponse_QNAME = new QName("http://www.fleetboard.com/data", "GetReplyResponse");
    private final static QName _GetLastPositionResponse_QNAME = new QName("http://www.fleetboard.com/data", "GetLastPositionResponse");
    private final static QName _GetLocationDescriptionResponse_QNAME = new QName("http://www.fleetboard.com/data", "GetLocationDescriptionResponse");
    private final static QName _AutoPos_QNAME = new QName("http://www.fleetboard.com/data", "AutoPos");
    private final static QName _VhcLayout_QNAME = new QName("http://www.fleetboard.com/data", "VhcLayout");
    private final static QName _Hidden_QNAME = new QName("http://www.fleetboard.com/data", "Hidden");
    private final static QName _GetCurrentStateResponse_QNAME = new QName("http://www.fleetboard.com/data", "GetCurrentStateResponse");
    private final static QName _GetPosReportTraceResponse_QNAME = new QName("http://www.fleetboard.com/data", "GetPosReportTraceResponse");
    private final static QName _GetLocationDescriptionRequest_QNAME = new QName("http://www.fleetboard.com/data", "GetLocationDescriptionRequest");
    private final static QName _Body_QNAME = new QName("http://www.fleetboard.com/data", "Body");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fireup.trans.tit.infrastructure.telematics.fleetboard.soap.posservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SemanticsDef }
     * 
     */
    public SemanticsDef createSemanticsDef() {
        return new SemanticsDef();
    }

    /**
     * Create an instance of {@link Position }
     * 
     */
    public Position createPosition() {
        return new Position();
    }

    /**
     * Create an instance of {@link InboxDataMsg }
     * 
     */
    public InboxDataMsg createInboxDataMsg() {
        return new InboxDataMsg();
    }

    /**
     * Create an instance of {@link Header }
     * 
     */
    public Header createHeader() {
        return new Header();
    }

    /**
     * Create an instance of {@link Msg }
     * 
     */
    public Msg createMsg() {
        return new Msg();
    }

    /**
     * Create an instance of {@link ReplystatusMsg }
     * 
     */
    public ReplystatusMsg createReplystatusMsg() {
        return new ReplystatusMsg();
    }

    /**
     * Create an instance of {@link ErrorResultType }
     * 
     */
    public ErrorResultType createErrorResultType() {
        return new ErrorResultType();
    }

    /**
     * Create an instance of {@link ErrorType }
     * 
     */
    public ErrorType createErrorType() {
        return new ErrorType();
    }

    /**
     * Create an instance of {@link OkType }
     * 
     */
    public OkType createOkType() {
        return new OkType();
    }

    /**
     * Create an instance of {@link OkResultType }
     * 
     */
    public OkResultType createOkResultType() {
        return new OkResultType();
    }

    /**
     * Create an instance of {@link AutoPos }
     * 
     */
    public AutoPos createAutoPos() {
        return new AutoPos();
    }

    /**
     * Create an instance of {@link Header.States }
     * 
     */
    public Header.States createHeaderStates() {
        return new Header.States();
    }

    /**
     * Create an instance of {@link GetLastPositionResponseType }
     * 
     */
    public GetLastPositionResponseType createGetLastPositionResponseType() {
        return new GetLastPositionResponseType();
    }

    /**
     * Create an instance of {@link Position.TracePositions }
     * 
     */
    public Position.TracePositions createPositionTracePositions() {
        return new Position.TracePositions();
    }

    /**
     * Create an instance of {@link GetPosReportTraceResponseType }
     * 
     */
    public GetPosReportTraceResponseType createGetPosReportTraceResponseType() {
        return new GetPosReportTraceResponseType();
    }

    /**
     * Create an instance of {@link GetPosReportTraceRequestType }
     * 
     */
    public GetPosReportTraceRequestType createGetPosReportTraceRequestType() {
        return new GetPosReportTraceRequestType();
    }

    /**
     * Create an instance of {@link GetPosReportRequestType }
     * 
     */
    public GetPosReportRequestType createGetPosReportRequestType() {
        return new GetPosReportRequestType();
    }

    /**
     * Create an instance of {@link FieldsDef }
     * 
     */
    public FieldsDef createFieldsDef() {
        return new FieldsDef();
    }

    /**
     * Create an instance of {@link VelocityAlarm }
     * 
     */
    public VelocityAlarm createVelocityAlarm() {
        return new VelocityAlarm();
    }

    /**
     * Create an instance of {@link TourEventUptimeMsg }
     * 
     */
    public TourEventUptimeMsg createTourEventUptimeMsg() {
        return new TourEventUptimeMsg();
    }

    /**
     * Create an instance of {@link HumidityAlarm }
     * 
     */
    public HumidityAlarm createHumidityAlarm() {
        return new HumidityAlarm();
    }

    /**
     * Create an instance of {@link TrailerSyncMsg }
     * 
     */
    public TrailerSyncMsg createTrailerSyncMsg() {
        return new TrailerSyncMsg();
    }

    /**
     * Create an instance of {@link SetpointData }
     * 
     */
    public SetpointData createSetpointData() {
        return new SetpointData();
    }

    /**
     * Create an instance of {@link UndervoltageAlarm }
     * 
     */
    public UndervoltageAlarm createUndervoltageAlarm() {
        return new UndervoltageAlarm();
    }

    /**
     * Create an instance of {@link History }
     * 
     */
    public History createHistory() {
        return new History();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link VhcStatusLayout }
     * 
     */
    public VhcStatusLayout createVhcStatusLayout() {
        return new VhcStatusLayout();
    }

    /**
     * Create an instance of {@link GetCurrentStateRequestType }
     * 
     */
    public GetCurrentStateRequestType createGetCurrentStateRequestType() {
        return new GetCurrentStateRequestType();
    }

    /**
     * Create an instance of {@link EventStatusMsg }
     * 
     */
    public EventStatusMsg createEventStatusMsg() {
        return new EventStatusMsg();
    }

    /**
     * Create an instance of {@link Form }
     * 
     */
    public Form createForm() {
        return new Form();
    }

    /**
     * Create an instance of {@link Fields }
     * 
     */
    public Fields createFields() {
        return new Fields();
    }

    /**
     * Create an instance of {@link Field }
     * 
     */
    public Field createField() {
        return new Field();
    }

    /**
     * Create an instance of {@link VehiclestatusMsg }
     * 
     */
    public VehiclestatusMsg createVehiclestatusMsg() {
        return new VehiclestatusMsg();
    }

    /**
     * Create an instance of {@link LocationDescription }
     * 
     */
    public LocationDescription createLocationDescription() {
        return new LocationDescription();
    }

    /**
     * Create an instance of {@link FormDefMsg }
     * 
     */
    public FormDefMsg createFormDefMsg() {
        return new FormDefMsg();
    }

    /**
     * Create an instance of {@link ReeferDieselElectricChanged }
     * 
     */
    public ReeferDieselElectricChanged createReeferDieselElectricChanged() {
        return new ReeferDieselElectricChanged();
    }

    /**
     * Create an instance of {@link SemanticsDef.Semantics }
     * 
     */
    public SemanticsDef.Semantics createSemanticsDefSemantics() {
        return new SemanticsDef.Semantics();
    }

    /**
     * Create an instance of {@link PairingMsg }
     * 
     */
    public PairingMsg createPairingMsg() {
        return new PairingMsg();
    }

    /**
     * Create an instance of {@link Hidden }
     * 
     */
    public Hidden createHidden() {
        return new Hidden();
    }

    /**
     * Create an instance of {@link MilageAlarm }
     * 
     */
    public MilageAlarm createMilageAlarm() {
        return new MilageAlarm();
    }

    /**
     * Create an instance of {@link ReadInfo }
     * 
     */
    public ReadInfo createReadInfo() {
        return new ReadInfo();
    }

    /**
     * Create an instance of {@link VhcLayout }
     * 
     */
    public VhcLayout createVhcLayout() {
        return new VhcLayout();
    }

    /**
     * Create an instance of {@link Behavior }
     * 
     */
    public Behavior createBehavior() {
        return new Behavior();
    }

    /**
     * Create an instance of {@link TrailerEventMsg }
     * 
     */
    public TrailerEventMsg createTrailerEventMsg() {
        return new TrailerEventMsg();
    }

    /**
     * Create an instance of {@link ReasonDTO }
     * 
     */
    public ReasonDTO createReasonDTO() {
        return new ReasonDTO();
    }

    /**
     * Create an instance of {@link TemperatureAlarm }
     * 
     */
    public TemperatureAlarm createTemperatureAlarm() {
        return new TemperatureAlarm();
    }

    /**
     * Create an instance of {@link SetpointAlarm }
     * 
     */
    public SetpointAlarm createSetpointAlarm() {
        return new SetpointAlarm();
    }

    /**
     * Create an instance of {@link Ebs24NAlarm }
     * 
     */
    public Ebs24NAlarm createEbs24NAlarm() {
        return new Ebs24NAlarm();
    }

    /**
     * Create an instance of {@link ReeferStartStopContinousChanged }
     * 
     */
    public ReeferStartStopContinousChanged createReeferStartStopContinousChanged() {
        return new ReeferStartStopContinousChanged();
    }

    /**
     * Create an instance of {@link ReeferOperationModeChanged }
     * 
     */
    public ReeferOperationModeChanged createReeferOperationModeChanged() {
        return new ReeferOperationModeChanged();
    }

    /**
     * Create an instance of {@link CurrentCompartmentDataList }
     * 
     */
    public CurrentCompartmentDataList createCurrentCompartmentDataList() {
        return new CurrentCompartmentDataList();
    }

    /**
     * Create an instance of {@link FuelLevelAlarm }
     * 
     */
    public FuelLevelAlarm createFuelLevelAlarm() {
        return new FuelLevelAlarm();
    }

    /**
     * Create an instance of {@link TempLoggerMonitoringAlarm }
     * 
     */
    public TempLoggerMonitoringAlarm createTempLoggerMonitoringAlarm() {
        return new TempLoggerMonitoringAlarm();
    }

    /**
     * Create an instance of {@link TirePressureAlarm }
     * 
     */
    public TirePressureAlarm createTirePressureAlarm() {
        return new TirePressureAlarm();
    }

    /**
     * Create an instance of {@link WatchboxAlarm }
     * 
     */
    public WatchboxAlarm createWatchboxAlarm() {
        return new WatchboxAlarm();
    }

    /**
     * Create an instance of {@link BogieLoadAlarm }
     * 
     */
    public BogieLoadAlarm createBogieLoadAlarm() {
        return new BogieLoadAlarm();
    }

    /**
     * Create an instance of {@link TrailerPositionDTO }
     * 
     */
    public TrailerPositionDTO createTrailerPositionDTO() {
        return new TrailerPositionDTO();
    }

    /**
     * Create an instance of {@link TmInfoDTO }
     * 
     */
    public TmInfoDTO createTmInfoDTO() {
        return new TmInfoDTO();
    }

    /**
     * Create an instance of {@link OrderMsg }
     * 
     */
    public OrderMsg createOrderMsg() {
        return new OrderMsg();
    }

    /**
     * Create an instance of {@link TrailerCouplingMsg }
     * 
     */
    public TrailerCouplingMsg createTrailerCouplingMsg() {
        return new TrailerCouplingMsg();
    }

    /**
     * Create an instance of {@link GetPosReportTraceResponse }
     * 
     */
    public GetPosReportTraceResponse createGetPosReportTraceResponse() {
        return new GetPosReportTraceResponse();
    }

    /**
     * Create an instance of {@link GetReply }
     * 
     */
    public GetReply createGetReply() {
        return new GetReply();
    }

    /**
     * Create an instance of {@link GetReplyRequestType }
     * 
     */
    public GetReplyRequestType createGetReplyRequestType() {
        return new GetReplyRequestType();
    }

    /**
     * Create an instance of {@link GetLocationDescriptionResponse }
     * 
     */
    public GetLocationDescriptionResponse createGetLocationDescriptionResponse() {
        return new GetLocationDescriptionResponse();
    }

    /**
     * Create an instance of {@link GetLocationDescriptionResponseType }
     * 
     */
    public GetLocationDescriptionResponseType createGetLocationDescriptionResponseType() {
        return new GetLocationDescriptionResponseType();
    }

    /**
     * Create an instance of {@link GetLastPositionRequestType }
     * 
     */
    public GetLastPositionRequestType createGetLastPositionRequestType() {
        return new GetLastPositionRequestType();
    }

    /**
     * Create an instance of {@link GetPosReportTrace }
     * 
     */
    public GetPosReportTrace createGetPosReportTrace() {
        return new GetPosReportTrace();
    }

    /**
     * Create an instance of {@link GetLastPosition }
     * 
     */
    public GetLastPosition createGetLastPosition() {
        return new GetLastPosition();
    }

    /**
     * Create an instance of {@link TrailerRegisterMsg }
     * 
     */
    public TrailerRegisterMsg createTrailerRegisterMsg() {
        return new TrailerRegisterMsg();
    }

    /**
     * Create an instance of {@link InboxDataMsg.AreaMonitoring }
     * 
     */
    public InboxDataMsg.AreaMonitoring createInboxDataMsgAreaMonitoring() {
        return new InboxDataMsg.AreaMonitoring();
    }

    /**
     * Create an instance of {@link FieldDef }
     * 
     */
    public FieldDef createFieldDef() {
        return new FieldDef();
    }

    /**
     * Create an instance of {@link FormLayout }
     * 
     */
    public FormLayout createFormLayout() {
        return new FormLayout();
    }

    /**
     * Create an instance of {@link GetReplyResponseType }
     * 
     */
    public GetReplyResponseType createGetReplyResponseType() {
        return new GetReplyResponseType();
    }

    /**
     * Create an instance of {@link TextConfMsg }
     * 
     */
    public TextConfMsg createTextConfMsg() {
        return new TextConfMsg();
    }

    /**
     * Create an instance of {@link GetCurrentStateResponseType }
     * 
     */
    public GetCurrentStateResponseType createGetCurrentStateResponseType() {
        return new GetCurrentStateResponseType();
    }

    /**
     * Create an instance of {@link CacheMsg }
     * 
     */
    public CacheMsg createCacheMsg() {
        return new CacheMsg();
    }

    /**
     * Create an instance of {@link GetLocationDescription }
     * 
     */
    public GetLocationDescription createGetLocationDescription() {
        return new GetLocationDescription();
    }

    /**
     * Create an instance of {@link GetLocationDescriptionRequestType }
     * 
     */
    public GetLocationDescriptionRequestType createGetLocationDescriptionRequestType() {
        return new GetLocationDescriptionRequestType();
    }

    /**
     * Create an instance of {@link StatusDefMsg }
     * 
     */
    public StatusDefMsg createStatusDefMsg() {
        return new StatusDefMsg();
    }

    /**
     * Create an instance of {@link GetCurrentStateResponse }
     * 
     */
    public GetCurrentStateResponse createGetCurrentStateResponse() {
        return new GetCurrentStateResponse();
    }

    /**
     * Create an instance of {@link GetReplyResponse }
     * 
     */
    public GetReplyResponse createGetReplyResponse() {
        return new GetReplyResponse();
    }

    /**
     * Create an instance of {@link GetCurrentState }
     * 
     */
    public GetCurrentState createGetCurrentState() {
        return new GetCurrentState();
    }

    /**
     * Create an instance of {@link AutoPosDef }
     * 
     */
    public AutoPosDef createAutoPosDef() {
        return new AutoPosDef();
    }

    /**
     * Create an instance of {@link Body }
     * 
     */
    public Body createBody() {
        return new Body();
    }

    /**
     * Create an instance of {@link DriverStatusMsg }
     * 
     */
    public DriverStatusMsg createDriverStatusMsg() {
        return new DriverStatusMsg();
    }

    /**
     * Create an instance of {@link Msg.ForeignRef }
     * 
     */
    public Msg.ForeignRef createMsgForeignRef() {
        return new Msg.ForeignRef();
    }

    /**
     * Create an instance of {@link GetLastPositionResponse }
     * 
     */
    public GetLastPositionResponse createGetLastPositionResponse() {
        return new GetLastPositionResponse();
    }

    /**
     * Create an instance of {@link ReplystatusMsg.RefID }
     * 
     */
    public ReplystatusMsg.RefID createReplystatusMsgRefID() {
        return new ReplystatusMsg.RefID();
    }

    /**
     * Create an instance of {@link GetPosReportResponse }
     * 
     */
    public GetPosReportResponse createGetPosReportResponse() {
        return new GetPosReportResponse();
    }

    /**
     * Create an instance of {@link GetPosReportResponseType }
     * 
     */
    public GetPosReportResponseType createGetPosReportResponseType() {
        return new GetPosReportResponseType();
    }

    /**
     * Create an instance of {@link GetPosReport }
     * 
     */
    public GetPosReport createGetPosReport() {
        return new GetPosReport();
    }

    /**
     * Create an instance of {@link TourstatusMsg }
     * 
     */
    public TourstatusMsg createTourstatusMsg() {
        return new TourstatusMsg();
    }

    /**
     * Create an instance of {@link OrderstatusMsg }
     * 
     */
    public OrderstatusMsg createOrderstatusMsg() {
        return new OrderstatusMsg();
    }

    /**
     * Create an instance of {@link TPDate }
     * 
     */
    public TPDate createTPDate() {
        return new TPDate();
    }

    /**
     * Create an instance of {@link LastPositionState }
     * 
     */
    public LastPositionState createLastPositionState() {
        return new LastPositionState();
    }

    /**
     * Create an instance of {@link Coordinate }
     * 
     */
    public Coordinate createCoordinate() {
        return new Coordinate();
    }

    /**
     * Create an instance of {@link ErrorResultType.ErrorResult }
     * 
     */
    public ErrorResultType.ErrorResult createErrorResultTypeErrorResult() {
        return new ErrorResultType.ErrorResult();
    }

    /**
     * Create an instance of {@link ErrorType.ErrorResult }
     * 
     */
    public ErrorType.ErrorResult createErrorTypeErrorResult() {
        return new ErrorType.ErrorResult();
    }

    /**
     * Create an instance of {@link OkType.OKResult }
     * 
     */
    public OkType.OKResult createOkTypeOKResult() {
        return new OkType.OKResult();
    }

    /**
     * Create an instance of {@link OkResultType.OKResult }
     * 
     */
    public OkResultType.OKResult createOkResultTypeOKResult() {
        return new OkResultType.OKResult();
    }

    /**
     * Create an instance of {@link AutoPos.Times }
     * 
     */
    public AutoPos.Times createAutoPosTimes() {
        return new AutoPos.Times();
    }

    /**
     * Create an instance of {@link Header.States.State }
     * 
     */
    public Header.States.State createHeaderStatesState() {
        return new Header.States.State();
    }

    /**
     * Create an instance of {@link GetLastPositionResponseType.Positions }
     * 
     */
    public GetLastPositionResponseType.Positions createGetLastPositionResponseTypePositions() {
        return new GetLastPositionResponseType.Positions();
    }

    /**
     * Create an instance of {@link Position.TracePositions.TracePosition }
     * 
     */
    public Position.TracePositions.TracePosition createPositionTracePositionsTracePosition() {
        return new Position.TracePositions.TracePosition();
    }

    /**
     * Create an instance of {@link GetPosReportTraceResponseType.Position }
     * 
     */
    public GetPosReportTraceResponseType.Position createGetPosReportTraceResponseTypePosition() {
        return new GetPosReportTraceResponseType.Position();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPosReportRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetPosReportRequest")
    public JAXBElement<GetPosReportRequestType> createGetPosReportRequest(GetPosReportRequestType value) {
        return new JAXBElement<GetPosReportRequestType>(_GetPosReportRequest_QNAME, GetPosReportRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPosReportTraceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetPosReportTraceRequest")
    public JAXBElement<GetPosReportTraceRequestType> createGetPosReportTraceRequest(GetPosReportTraceRequestType value) {
        return new JAXBElement<GetPosReportTraceRequestType>(_GetPosReportTraceRequest_QNAME, GetPosReportTraceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FieldsDef }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "FieldsDef")
    public JAXBElement<FieldsDef> createFieldsDef(FieldsDef value) {
        return new JAXBElement<FieldsDef>(_FieldsDef_QNAME, FieldsDef.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLastPositionRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetLastPositionRequest")
    public JAXBElement<GetLastPositionRequestType> createGetLastPositionRequest(GetLastPositionRequestType value) {
        return new JAXBElement<GetLastPositionRequestType>(_GetLastPositionRequest_QNAME, GetLastPositionRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReplyRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetReplyRequest")
    public JAXBElement<GetReplyRequestType> createGetReplyRequest(GetReplyRequestType value) {
        return new JAXBElement<GetReplyRequestType>(_GetReplyRequest_QNAME, GetReplyRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VhcStatusLayout }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "VhcStatusLayout")
    public JAXBElement<VhcStatusLayout> createVhcStatusLayout(VhcStatusLayout value) {
        return new JAXBElement<VhcStatusLayout>(_VhcStatusLayout_QNAME, VhcStatusLayout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentStateRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetCurrentStateRequest")
    public JAXBElement<GetCurrentStateRequestType> createGetCurrentStateRequest(GetCurrentStateRequestType value) {
        return new JAXBElement<GetCurrentStateRequestType>(_GetCurrentStateRequest_QNAME, GetCurrentStateRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPosReportResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetPosReportResponse")
    public JAXBElement<GetPosReportResponseType> createGetPosReportResponse(GetPosReportResponseType value) {
        return new JAXBElement<GetPosReportResponseType>(_GetPosReportResponse_QNAME, GetPosReportResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReplyResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetReplyResponse")
    public JAXBElement<GetReplyResponseType> createGetReplyResponse(GetReplyResponseType value) {
        return new JAXBElement<GetReplyResponseType>(_GetReplyResponse_QNAME, GetReplyResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLastPositionResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetLastPositionResponse")
    public JAXBElement<GetLastPositionResponseType> createGetLastPositionResponse(GetLastPositionResponseType value) {
        return new JAXBElement<GetLastPositionResponseType>(_GetLastPositionResponse_QNAME, GetLastPositionResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLocationDescriptionResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetLocationDescriptionResponse")
    public JAXBElement<GetLocationDescriptionResponseType> createGetLocationDescriptionResponse(GetLocationDescriptionResponseType value) {
        return new JAXBElement<GetLocationDescriptionResponseType>(_GetLocationDescriptionResponse_QNAME, GetLocationDescriptionResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AutoPos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "AutoPos")
    public JAXBElement<AutoPos> createAutoPos(AutoPos value) {
        return new JAXBElement<AutoPos>(_AutoPos_QNAME, AutoPos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VhcLayout }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "VhcLayout")
    public JAXBElement<VhcLayout> createVhcLayout(VhcLayout value) {
        return new JAXBElement<VhcLayout>(_VhcLayout_QNAME, VhcLayout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Hidden }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "Hidden")
    public JAXBElement<Hidden> createHidden(Hidden value) {
        return new JAXBElement<Hidden>(_Hidden_QNAME, Hidden.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentStateResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetCurrentStateResponse")
    public JAXBElement<GetCurrentStateResponseType> createGetCurrentStateResponse(GetCurrentStateResponseType value) {
        return new JAXBElement<GetCurrentStateResponseType>(_GetCurrentStateResponse_QNAME, GetCurrentStateResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPosReportTraceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetPosReportTraceResponse")
    public JAXBElement<GetPosReportTraceResponseType> createGetPosReportTraceResponse(GetPosReportTraceResponseType value) {
        return new JAXBElement<GetPosReportTraceResponseType>(_GetPosReportTraceResponse_QNAME, GetPosReportTraceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLocationDescriptionRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "GetLocationDescriptionRequest")
    public JAXBElement<GetLocationDescriptionRequestType> createGetLocationDescriptionRequest(GetLocationDescriptionRequestType value) {
        return new JAXBElement<GetLocationDescriptionRequestType>(_GetLocationDescriptionRequest_QNAME, GetLocationDescriptionRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Body }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fleetboard.com/data", name = "Body")
    public JAXBElement<Body> createBody(Body value) {
        return new JAXBElement<Body>(_Body_QNAME, Body.class, null, value);
    }

}
