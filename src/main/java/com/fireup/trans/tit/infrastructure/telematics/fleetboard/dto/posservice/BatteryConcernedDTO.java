
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BatteryConcernedDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BatteryConcernedDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CTU1_Battery"/>
 *     &lt;enumeration value="CTU2_LIN_Battery"/>
 *     &lt;enumeration value="External_Battery"/>
 *     &lt;enumeration value="Boardnet_12V"/>
 *     &lt;enumeration value="Boardnet_24V"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BatteryConcernedDTO")
@XmlEnum
public enum BatteryConcernedDTO {

    @XmlEnumValue("CTU1_Battery")
    CTU_1_BATTERY("CTU1_Battery"),
    @XmlEnumValue("CTU2_LIN_Battery")
    CTU_2_LIN_BATTERY("CTU2_LIN_Battery"),
    @XmlEnumValue("External_Battery")
    EXTERNAL_BATTERY("External_Battery"),
    @XmlEnumValue("Boardnet_12V")
    BOARDNET_12_V("Boardnet_12V"),
    @XmlEnumValue("Boardnet_24V")
    BOARDNET_24_V("Boardnet_24V");
    private final String value;

    BatteryConcernedDTO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BatteryConcernedDTO fromValue(String v) {
        for (BatteryConcernedDTO c: BatteryConcernedDTO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
