
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.basicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.fleetboard.com/data}GetServerPropertiesResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getServerPropertiesResponse"
})
@XmlRootElement(name = "getServerPropertiesResponse")
public class GetServerPropertiesResponse {

    @XmlElement(name = "GetServerPropertiesResponse", required = true)
    protected GetServerPropertiesResponse2 getServerPropertiesResponse;

    /**
     * Gets the value of the getServerPropertiesResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetServerPropertiesResponse2 }
     *     
     */
    public GetServerPropertiesResponse2 getGetServerPropertiesResponse() {
        return getServerPropertiesResponse;
    }

    /**
     * Sets the value of the getServerPropertiesResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetServerPropertiesResponse2 }
     *     
     */
    public void setGetServerPropertiesResponse(GetServerPropertiesResponse2 value) {
        this.getServerPropertiesResponse = value;
    }

}
