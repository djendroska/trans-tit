
package com.fireup.trans.tit.infrastructure.telematics.fleetboard.dto.posservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCurrentStateRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCurrentStateRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VehicleID" type="{http://www.fleetboard.com/data}vehicleidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="VehicleGroupID" type="{http://www.fleetboard.com/data}vehicleGroupIdType" minOccurs="0"/>
 *         &lt;element name="DriverNameID" type="{http://www.fleetboard.com/data}driverNameIDType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DriverGroupID" type="{http://www.fleetboard.com/data}driverGroupIdType" minOccurs="0"/>
 *         &lt;element name="ServiceType" type="{http://www.fleetboard.com/data}serviceType" minOccurs="0"/>
 *         &lt;element name="QueryType" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="OnlyActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="sessionid" type="{http://www.fleetboard.com/data}sessionidType" />
 *       &lt;attribute name="limit" type="{http://www.fleetboard.com/data}limitType" />
 *       &lt;attribute name="offset" type="{http://www.fleetboard.com/data}offsetType" />
 *       &lt;attribute name="version" type="{http://www.fleetboard.com/data}versionType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCurrentStateRequestType", propOrder = {
    "vehicleID",
    "vehicleGroupID",
    "driverNameID",
    "driverGroupID",
    "serviceType",
    "queryType",
    "onlyActive"
})
public class GetCurrentStateRequestType {

    @XmlElement(name = "VehicleID", type = Long.class)
    protected List<Long> vehicleID;
    @XmlElement(name = "VehicleGroupID")
    protected Long vehicleGroupID;
    @XmlElement(name = "DriverNameID", type = Long.class)
    protected List<Long> driverNameID;
    @XmlElement(name = "DriverGroupID")
    protected Long driverGroupID;
    @XmlElement(name = "ServiceType")
    @XmlSchemaType(name = "string")
    protected ServiceType serviceType;
    @XmlElement(name = "QueryType")
    protected Short queryType;
    @XmlElement(name = "OnlyActive")
    protected Boolean onlyActive;
    @XmlAttribute(name = "sessionid")
    protected String sessionid;
    @XmlAttribute(name = "limit")
    protected BigInteger limit;
    @XmlAttribute(name = "offset")
    protected BigInteger offset;
    @XmlAttribute(name = "version")
    protected Long version;

    /**
     * Gets the value of the vehicleID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getVehicleID() {
        if (vehicleID == null) {
            vehicleID = new ArrayList<Long>();
        }
        return this.vehicleID;
    }

    /**
     * Gets the value of the vehicleGroupID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVehicleGroupID() {
        return vehicleGroupID;
    }

    /**
     * Sets the value of the vehicleGroupID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVehicleGroupID(Long value) {
        this.vehicleGroupID = value;
    }

    /**
     * Gets the value of the driverNameID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the driverNameID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDriverNameID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getDriverNameID() {
        if (driverNameID == null) {
            driverNameID = new ArrayList<Long>();
        }
        return this.driverNameID;
    }

    /**
     * Gets the value of the driverGroupID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDriverGroupID() {
        return driverGroupID;
    }

    /**
     * Sets the value of the driverGroupID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDriverGroupID(Long value) {
        this.driverGroupID = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceType }
     *     
     */
    public ServiceType getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceType }
     *     
     */
    public void setServiceType(ServiceType value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the queryType property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getQueryType() {
        return queryType;
    }

    /**
     * Sets the value of the queryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setQueryType(Short value) {
        this.queryType = value;
    }

    /**
     * Gets the value of the onlyActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOnlyActive() {
        return onlyActive;
    }

    /**
     * Sets the value of the onlyActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOnlyActive(Boolean value) {
        this.onlyActive = value;
    }

    /**
     * Gets the value of the sessionid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionid() {
        return sessionid;
    }

    /**
     * Sets the value of the sessionid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionid(String value) {
        this.sessionid = value;
    }

    /**
     * Gets the value of the limit property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLimit() {
        return limit;
    }

    /**
     * Sets the value of the limit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLimit(BigInteger value) {
        this.limit = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOffset(BigInteger value) {
        this.offset = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVersion(Long value) {
        this.version = value;
    }

}
