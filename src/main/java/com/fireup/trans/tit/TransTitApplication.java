package com.fireup.trans.tit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransTitApplication {

	public static void main(String[] args) {
	    SpringApplication.run(TransTitApplication.class, args);
	}
}
